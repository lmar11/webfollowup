﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PortalProveedor.Models.Entity
{
    /* Descarga Excel */
    public class Download_Entity
    {
        private string downloadname = "";
        public string FileName { get; set; }
        public string DownloadName
        {
            get
            {
                return downloadname;
            }
            set
            {
                if (value == null)
                {
                    downloadname = "";
                }
                else
                {
                    downloadname = value;
                }
            }
        }

    }

    /* Dashboard */
    public class Entry_GraficoOrdenItem
    {
        public string CodigoSapProveedor { get; set; }
        public int CodiFana { get; set; }
        public int Proceso { get; set; }
    }
    public class Response_GraficoOrdenItem
    {
        public List<GraficoOrdenItemModel> Grafico { get; set; }
    }
    public class GraficoOrdenItemModel
    {
        public string Tipo { get; set; }
        public string Valor { get; set; }
        public string NumeroItem { get; set; }
    }
    public class Entry_GraficoOrdenBarra
    {
        public string CodigoSapProveedor { get; set; }
        public int CodiFana { get; set; }
        public int Proceso { get; set; }
    }
    public class Response_GraficoOrdenBarra
    {
        public List<GraficoOrdenBarraModel> Grafico { get; set; }
    }
    public class GraficoOrdenBarraModel
    {
        public string Tipo { get; set; }
        public string ValorNuevas { get; set; }
        public string ValorPorEntregar { get; set; }
        public string ValorObservadas { get; set; }
    }
    public class Entry_GraficoOrdenesVigentes
    {
        public string CodigoSapProveedor { get; set; }
        public int CodiFana { get; set; }
        public int Proceso { get; set; }
    }
    public class Response_GraficoOrdenesVigentes
    {
        public List<GraficoOrdenesVigentesModel> Grafico { get; set; }
    }
    public class GraficoOrdenesVigentesModel
    {
        public string Fecha { get; set; }
        public string Valor { get; set; }
    }
    public class Entry_GraficoOrdenesVencidas
    {
        public string CodigoSapProveedor { get; set; }
        public int CodiFana { get; set; }
        public int Proceso { get; set; }
    }
    public class Response_GraficoOrdenesVencidas
    {
        public List<GraficoOrdenesVencidasModel> Grafico { get; set; }
    }
    public class GraficoOrdenesVencidasModel
    {
        public string Fecha { get; set; }
        public string Valor { get; set; }
    }

    /* Ordenes Nuevas*/
    public class Entry_OrdenCompra
    {
        public decimal CodiProv { get; set; }
        public decimal CodiFana { get; set; }
        public int Proceso { get; set; }
        public string Tipo { get; set; }
        public string Consulta { get; set; }
        public decimal CodiPerf { get; set; }
    }
    public class Response_OrdenCompra
    {
        public decimal CodiOrco { get; set; }
        public string GrupoCompra { get; set; }
        public string DocumentoCompra { get; set; }
        public string Proveedor { get; set; }
        public string Analista { get; set; }
        public decimal ImporteTotal { get; set; }
        public string TipoMoneda { get; set; }
        public string FechaDocumento { get; set; }
        public string Estatus { get; set; }
        public string Url_Pdf { get; set; }
        public string FlagDescargado { get; set; }
        public string FlagNotificado { get; set; }
    }
    public class Entry_OrdenCompraItem
    {
        public decimal CodiOrco { get; set; }
    }
    public class Response_OrdenCompraItem
    {
        public string CodiOrcm { get; set; }
        public int Posicion { get; set; }
        public string Material { get; set; }
        public string Descripcion { get; set; }
    }
    public class Response_OrdenCompraTipoMotivo
    {
        public string CodiMrec { get; set; }
        public string Descripcion { get; set; }
    }
    public class Entry_AcceptOrdenesNuevas
    {
        public string Username { get; set; }
        public string IpAddress { get; set; }
        public decimal CodiOrco { get; set; }
    }
    public class Entry_OrdenCompraObservacionMotivo
    {
        public string Username { get; set; }
        public string IpAddress { get; set; }
        public List<OrdenCompraObservacionMotivo> ObservacionMotivo { get; set; }
    }
    public class OrdenCompraObservacionMotivo
    {
        public decimal CodiOrcm { get; set; }
        public string CodiMrec { get; set; }
        public string Cambio { get; set; }
        public string Comentario { get; set; }
    }
    public class Entry_LogOrdenCompra
    {
        public decimal CodiOrco { get; set; }
        public string Proceso { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
    }
    public class Entry_FiltroAprobacionSolicitudRegistro
    {
        public int CodiFana { get; set; }
        public string Estado { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
    }
    public class Response_FiltroAprobacionSolicitudRegistro
    {
        public Int64 CodiPert { get; set; }
        public string CodigoSapProveedor { get; set; }
        public string RazonSocial { get; set; }
        public string Nombres { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        public string FechaRegistro { get; set; }
        public int CodiRol { get; set; }
        public string Cargo { get; set; }
        public string Estado { get; set; }
    }
    public class Entry_AprobacionSolicitudRegistro
    {
        public string Username { get; set; }
        public string IpAdress { get; set; }
        public List<AprobacionSolicitudRegistro> Registro { get; set; }
    }
    public class AprobacionSolicitudRegistro
    {
        public Int64 CodiPert { get; set; }
        public string Estado { get; set; }
        public string Motivo { get; set; }
        public int CodiRol { get; set; }
    }

    /* Ordenes Por Entregar*/
    public class Entry_OrdenCompraPorEntregar
    {
        public decimal CodiProv { get; set; }
        public decimal CodiFana { get; set; }
        public int Proceso { get; set; }
        public string Tipo { get; set; }
        public string Status { get; set; }
        public string Consulta { get; set; }
        public decimal CodiPerf { get; set; }
    }
    public class Response_OrdenCompraPorEntregar
    {
        public decimal CodiOrcm { get; set; }
        public string DocumentoCompra { get; set; }
        public string Posicion { get; set; }
        public string Proveedor { get; set; }
        public string Analista { get; set; }
        public string Descripcion { get; set; }
        public int CantidadPendiente { get; set; }
        public string UM { get; set; }
        public decimal ImporteTotal { get; set; }
        public string TipoMoneda { get; set; }
        public string FechaDocumento { get; set; }
        public string FechaEntrega { get; set; }
        public string Estatus { get; set; } // Lo que se muestra en la columna Ver
        public string Estado { get; set; } // Lo que se muestran en la columna Fecha E.
        public string FechaConfirmacion { get; set; }
        public int CodiMoti { get; set; }
        public string Motivo { get; set; }
    }
    public class Entry_OrdenCompraPorEntregarConfirmacion
    {
        public List<OrdenCompraPorEntregarConfirmacion> OrdenCompra { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class OrdenCompraPorEntregarConfirmacion
    {
        public decimal CodiOrcm { get; set; }
        public string FechaConfirmacion { get; set; }
        public string CodiMoti { get; set; }
        public string Estado { get; set; }
    }
    public class Response_OrdenCompraPorEntregarMotivo
    {
        public int CodiMoti { get; set; }
        public string Descripcion { get; set; }
    }
    public class Entry_OrdenCompraPorEntregarRemove
    {
        public decimal CodiOrcm { get; set; }
        public string Motivo { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Entry_OrdenCompraPorEntregarDetalle
    {
        public decimal CodiOrcm { get; set; }
    }
    public class Response_OrdenCompraPorEntregarDetalle
    {
        public string FechaConfirmacion { get; set; }
        public int CodiMoti { get; set; }
        public string Motivo { get; set; }
    }

    /* MANTENIMIENTO */
    public class Entry_MotivosMantenimiento
    {
        public int CodiMoti { get; set; }
        public string Descripcion { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Response_MotivosMantenimiento
    {
        public int CodiMoti { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
    }
    public class Entry_ProcesosMantenimiento
    {
        public int CodiProc { get; set; }
        public string Descripcion { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Response_ProcesosMantenimiento
    {
        public int CodiProc { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
    }
    public class Entry_AnalistasMantenimiento
    {
        public int CodiPers { get; set; }
        public string GrupoCompra { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Response_AnalistasMantenimiento
    {
        public int CodiPers { get; set; }
        public string Nombres { get; set; }
        public string GrupoCompra { get; set; }
        public string Estado { get; set; }
    }
    public class Entry_AnalistasAutocompleteMantenimiento
    {
        public string Termino { get; set; }
    }
    public class Response_AnalistasAutocompleteMantenimiento
    {
        public string CodiPers { get; set; }
        public string Nombres { get; set; }
        public string Username { get; set; }
    }
    public class Entry_CategoriasMantenimiento
    {
        public int CodiCate { get; set; }
        public int CodiProc { get; set; }
        public string Clave { get; set; }
        public string Descripcion { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Response_CategoriasMantenimiento
    {
        public int CodiCate { get; set; }
        public int CodiProc { get; set; }
        public string Clave { get; set; }
        public string Descripcion { get; set; }
    }
    public class Entry_ParametrosMantenimiento
    {
        public string Clave { get; set; }
        public string Datos { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Response_ParametrosMantenimiento
    {
        public string Clave { get; set; }
        public string Datos { get; set; }
        public string Descripcion { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Entry_PerfilesMantenimiento
    {
        public int AppId { get; set; }
        public int CodiPerf { get; set; }
        public string Username { get; set; }
        public int Estado { get; set; } //1:Dar acceso 0:Quitar Acceso
    }
    public class Response_PerfilesMantenimiento
    {
        public string Username { get; set; }
        public string Nombres { get; set; }
        public int CodiPerf { get; set; }
        public string Perfil { get; set; }
        public int Estado { get; set; } //1:Activo 0:Inactivo
    }

    /* Ordenes Observadas*/
    public class Entry_OrdenCompraObservadas
    {
        public decimal CodiProv { get; set; }
        public decimal CodiFana { get; set; }
        public int Proceso { get; set; }
        public string Status { get; set; }
        public string Tipo { get; set; }
        public string Consulta { get; set; }
        public decimal CodiPerf { get; set; }
    }
    public class Response_OrdenCompraObservadas
    {
        public decimal CodiOrcm { get; set; }
        public string DocumentoCompra { get; set; }
        public string Descripcion { get; set; }
        public string Posicion { get; set; }
        public string Proveedor { get; set; }
        public string Analista { get; set; }
        public string FechaDocumento { get; set; }
        public string Motivo { get; set; }
    }
    public class Entry_OrdenCompraObservadasDetail
    {
        public decimal CodiOrcm { get; set; }
    }
    public class Response_OrdenCompraObservadasDetail
    {
        public decimal CodiMaob { get; set; }
        public decimal CodiMrec { get; set; }
        public string Motivo { get; set; }
        public string ValorActual { get; set; }
        public string Cambio { get; set; }
        public string Comentario { get; set; }
    }
    public class Entry_AcceptOrdenesObservadas
    {
        public decimal CodiOrcm { get; set; }
        public string Estado { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }

    /* Configuración */
    public class Entry_Persona_Proveedor
    {
        public Int64 CodiPers { get; set; }
        public Int64 CodiProv { get; set; }
    }
    public class Response_Persona_Proveedor
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Cargo { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        public string Celular { get; set; }
        public string Ruc { get; set; }
        public string Proveedor { get; set; }
        public List<Persona> Contactos { get; set; }
    }
    public class Persona
    {
        public string Tipo { get; set; } //GV: G. VENTAS * CF: CONT. FACTURACION * CT: CONT. TECNICO
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Cargo { get; set; }
        public string Correo { get; set; }
        public string Celular { get; set; }
    }
    public class Entry_Configuracion
    {
        public int AppId { get; set; }
        public decimal CodiPers { get; set; }
        public string Ruc { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string IpAddress { get; set; }
        public Contacto Solicitante { get; set; }
        public Contacto ContactoGerencia { get; set; }
        public Contacto ContactoFacturacion { get; set; }
        public Contacto ContactoTecnico { get; set; }
    }
    public class Contacto
    {
        public string Tipo { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Cargo { get; set; }
        public string Correo { get; set; }
        public string Celular { get; set; }
    }
    public class Response_Configuracion
    {
        public int AppId { get; set; }
        public decimal CodiPers { get; set; }
        public string Ruc { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string IpAddress { get; set; }
        public Contacto Solicitante { get; set; }
        public Contacto ContactoGerencia { get; set; }
        public Contacto ContactoFacturacion { get; set; }
        public Contacto ContactoTecnico { get; set; }
    }

    /* Centro de Soporte */
    public class Entry_SupportCentre
    {
        public string DetallePrincipal { get; set; }
        public string DetalleAdicional { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }

    /* Manual */
    public class Entry_ManualMantenimiento
    {
        public string NombreArchivo { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string Username { get; set; }
        public string IpAddress { get; set; }
    }
    public class Response_ManualMantenimiento
    {
        public string NombreArchivo { get; set; }
        public string FechaCreacion { get; set; }
    }
}