﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PortalProveedor.Models.Entity
{
    /* Modelos Login, Validación Login, Session */
    public class ApiResponse
    {
        public object Response { get; set; }
        public int Status { get; set; }
        public string Value { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
    }
    public class Log_Model
    {
        private string controllername = "";
        private string actionname = "";
        public string ControllerName
        {
            get
            {
                return controllername;
            }
            set
            {
                if (value == null)
                {
                    controllername = "";
                }
                else
                {
                    controllername = value;
                }
            }
        }
        public string ActionName
        {
            get
            {
                return actionname;
            }
            set
            {
                if (value == null)
                {
                    actionname = "";
                }
                else
                {
                    actionname = value;
                }
            }
        }
        public decimal SessionId { get; set; }
    }
    public class Login_Model
    {
        public string pcname = "";
        public decimal SessionId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public decimal ProfileId { get; set; }
        public decimal AccountId { get; set; }
        public decimal AppId { get; set; }
        public string IPAddress { get; set; }
        public string Information { get; set; }

        public int PersonaId { get; set; }
        public decimal TrabajadorId { get; set; }
        public string FichaSap { get; set; }
        public string Email { get; set; }

        public string PcName
        {
            get
            {
                return pcname;
            }
            set
            {
                if (value == null)
                {
                    pcname = "";
                }
                else
                {
                    pcname = value;
                }
            }
        }
    }
    public class Entry_Login
    {
        public string Correo { get; set; }
        //public string Username { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
        public int AppId { get; set; }
    }
    public class Response_Login
    {
        public string Username { get; set; }
        public decimal CodiProv { get; set; }
        public string CodiSap { get; set; }
        public decimal CodiPers { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public decimal CodiFana { get; set; }
        public string FichaSap { get; set; }
        public decimal CodiTrab { get; set; }
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public string UrlFoto { get; set; }
        public List<LoginProfileModel> Profile { get; set; }
    }
    public class LoginProfileModel
    {
        public decimal CodiPerf { get; set; }
        public int CodiRol { get; set; }
        public string Descripcion { get; set; }
    }
    public class Entry_ObjectSecurity
    {
        public Int64 AppId { get; set; }
        public int ProfileId { get; set; }
        public string UserId { get; set; }
        public int ParentId { get; set; }
        //public string Username { get; set; }
        //public Int64 CodiObjPadre { get; set; }
    }
    public class Entry_RubroObjectSecurity
    {
        public Int64 AppId { get; set; }
        public int CodiPerf { get; set; }
        public string Username { get; set; }
    }
    public class Response_ObjectSecurity
    {
        //public Int64 CodiObje { get; set; }
        //public int CodiTipo { get; set; }
        //public Int64 CodiObjePadre { get; set; }
        //public string Icono { get; set; }
        //public string Modulo { get; set; }
        //public string Controlador { get; set; }
        //public string Funcion { get; set; }
        //public int FlagFinal { get; set; }
        //public int Orden { get; set; }
        //public string Descripcion { get; set; }
        public decimal Id { get; set; }
        public decimal ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public decimal Type { get; set; }
        public string Color { get; set; }
        public decimal Order { get; set; }
        public bool Completed { get; set; }
        public bool HasChildren { get; set; }
        public int CodiRubr { get; set; }
        public string Rubro { get; set; }
    }
    public class Response_Sesion
    {
        public Int64 SessionId { get; set; }
    }

    /* Modelos Login Portal */
    public class Response_LoginRubro
    {
        public Int64 CodiRubr { get; set; }
        public string Descripcion { get; set; }
    }    
    public class Entry_LoginRuc
    {
        public string Ruc { get; set; }
    }
    public class Response_LoginRuc
    {
        public string RazonSocial { get; set; }
        public List<ContactoLoginModel> Contacto { get; set; }
    }
    public class ContactoLoginModel
    {
        public string Tipo { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Cargo { get; set; }
        public string Correo { get; set; }
        public string Celular { get; set; }
    }    
    public class Entry_LoginValidacionCorreo
    {
        public string Correo { get; set; }
        public int AppId { get; set; }
    }
    public class Response_LoginAnalista
    {
        public List<AnalistaLoginModel> Analista { get; set; }
    }
    public class AnalistaLoginModel
    {
        public decimal CodiFana { get; set; }
        public string Nombre { get; set; }
        public string GrupoCompra { get; set; }
    }
    public class ProcesoModel
    {
        public int CodiProc { get; set; }
        public string Descripcion { get; set; }
    }
    public class Response_LoginSignUp
    {
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public Int64 Analista { get; set; }
        public string Password { get; set; }
        public PersonaLoginSignUpModel Solicitante { get; set; }
        public PersonaLoginSignUpModel ContactoGerencia { get; set; }
        public PersonaLoginSignUpModel ContactoFacturacion { get; set; }
        public PersonaLoginSignUpModel ContactoTecnico { get; set; }
        public int AppId { get; set; }
    }
    public class PersonaLoginSignUpModel
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public string Cargo { get; set; }
        public string Correo { get; set; }
        public string Celular { get; set; }
    }
    public class Entry_Notificacion
    {
        public string Tipo { get; set; }
        public Int64 Codigo { get; set; }
    }
    public class Response_Notificacion
    {
        public Int64 CodiNoti { get; set; }
        public string Proceso { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
    }
}