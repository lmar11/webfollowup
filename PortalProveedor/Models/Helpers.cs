﻿using Newtonsoft.Json;
using PortalProveedor.Models.Dao;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace PortalProveedor.Models
{
    public class Helpers
    {
        public static string APP_VERSION = "2.2";
        public static int ANALISTA_PROFILE_ID = 44;
        public static int ADMINISTRADOR_PROFILE_ID = 1;
        public static int PROVEEDOR_PROFILE_ID = 2;
        public const string DIRECTORY_EMPLOYEE_PHOTO = "capacitacion/fotos_trabajador/{filename}";
        public const string DIRECTORY_RPA_DOCUMENT = "rpa/document/{filename}";
        public const string DIRECTORY_IMAGE_IOT = "sider/imagenes_iot/{filename}";


        public static string ReplaceFirstOccurrence(string p_source, string p_find, string p_replace)
        {
            int l_index = p_source.IndexOf(p_find);
            string result = p_source.Remove(l_index, p_find.Length).Insert(l_index, p_replace);
            return result;
        }
        public static string ReplaceLastOccurrence(string p_source, string p_find, string p_replace)
        {
            int l_index = p_source.LastIndexOf(p_find);
            string result = p_source.Remove(l_index, p_find.Length).Insert(l_index, p_replace);
            return result;
        }
        public static JsonResult GetMessageCrud(DataTable p_dt, object p_others = null)
        {
            string l_message = null, l_code = null, l_estado = null, l_codi_pers = null;
            try
            {
                foreach (DataRow row in p_dt.Rows)
                {
                    l_message = row["Mensaje"].ToString();
                    l_code = row["Value"].ToString();
                    if (p_dt.Columns.Contains("CodiPers"))
                    {
                        l_codi_pers = row["CodiPers"].ToString();
                    }
                    if (p_dt.Columns.Contains("Estado"))
                    {
                        l_estado = row["Estado"].ToString();
                    }
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        status = l_code,
                        response = l_message,
                        exception = l_message,
                        codi = l_codi_pers,
                        estado = l_estado,
                        others = p_others
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                throw;
            }
        }
        public static JsonResult GetException(Exception p_e)
        {
            string l_message = p_e.Message;
            string l_exception = "An exception occurred.\n" +
                "- Type : " + p_e.GetType().Name + "\n" +
                "- Message: " + p_e.Message + "\n" +
                "- Stack Trace: " + p_e.StackTrace;
            Exception l_ie = p_e.InnerException;
            if (l_ie != null)
            {
                l_exception += "\n";
                l_exception += "The Inner Exception:\n" +
                    "- Exception Name: " + l_ie.GetType().Name + "\n" +
                    "- Message: " + l_ie.Message + "\n" +
                    "- Stack Trace: " + l_ie.StackTrace;
            }
            return new JsonResult()
            {
                Data = new
                {
                    status = -1,
                    response = l_message,
                    exception = l_exception
                },
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        public static Stream CreateThumbnail(Stream p_stream, int p_thumbWi, int p_thumbHi)
        {
            Image l_image;
            Bitmap l_thumbnail;
            MemoryStream l_mstream;
            int l_wi, l_hi;
            try
            {
                l_image = Image.FromStream(p_stream);

                if (l_image.Width <= p_thumbWi && l_image.Height <= p_thumbHi)
                {
                    p_stream.Position = 0;
                    return p_stream;
                }

                l_wi = p_thumbWi;
                l_hi = p_thumbHi;

                if (l_image.Width > l_image.Height)
                {
                    l_wi = p_thumbWi;
                    l_hi = (int)(l_image.Height * ((decimal)p_thumbWi / l_image.Width));
                }
                else
                {
                    l_hi = p_thumbHi;
                    l_wi = (int)(l_image.Width * ((decimal)p_thumbHi / l_image.Height));
                }

                l_thumbnail = new Bitmap(l_wi, l_hi);

                using (Graphics g = Graphics.FromImage(l_thumbnail))
                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.FillRectangle(Brushes.Transparent, 0, 0, l_wi, l_hi);
                    g.DrawImage(l_image, 0, 0, l_wi, l_hi);
                }
                l_mstream = new MemoryStream();
                l_thumbnail.Save(l_mstream, l_image.RawFormat);
                l_mstream.Position = 0;
                return l_mstream;
            }
            catch
            {
                return null;
            }
        }
        public static Stream CloneStream(Stream p_stream)
        {
            MemoryStream l_stream = new MemoryStream();
            long l_pos = p_stream.Position;
            p_stream.CopyTo(l_stream);
            p_stream.Position = l_pos;
            l_stream.Position = l_pos;
            return l_stream;
        }
        public static string GetMonthName(int p_month)
        {
            string[] l_names = { "enero", "febrero", "marzo", "abril", "mayo", "junio",
                "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};
            return l_names[p_month - 1];
        }
        public static string Coalesce(object[] p_list)
        {
            foreach (var item in p_list)
            {
                if (!Convert.IsDBNull(item) && item.ToString().Trim() != "")
                {
                    return item.ToString();
                }
            }
            return "";
        }
        public static void ClearDataTable(DataTable p_dt)
        {
            if (p_dt != null)
            {
                p_dt.Clear();
                p_dt.Dispose();
                p_dt = null;
            }
        }
        public static string GetAppSettings(string p_key)
        {
            return System.Configuration.ConfigurationManager.AppSettings.Get(p_key);
        }
        public static string executeAPI(string json_data, string url, string token)
        {
            var postString = json_data;
            byte[] data = UTF8Encoding.UTF8.GetBytes(postString);
            HttpWebRequest request;
            request = WebRequest.Create(url) as HttpWebRequest;

            request.Timeout = 10 * 10000;
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json; charset=utf-8";
            if (token != "")
            {
                request.Headers.Add("Authorization", token);
            }
            Stream postStream = request.GetRequestStream();
            postStream.Write(data, 0, data.Length);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return reader.ReadToEnd();
        }
        public static string executeAPI(object json_data, string url, string token)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (var client = new HttpClient())
            {
                if (token != "")
                {
                    client.DefaultRequestHeaders.Add("authorization", token);
                }
                client.BaseAddress = new Uri(url);
                //HTTP POST
                var postTask = client.PostAsJsonAsync("", json_data);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var ff = result.Content.ReadAsStringAsync().Result;
                    return ff;
                }
                else
                {
                    return "ERROR";
                }
            }
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static ApiResponse ExecuteApiFollowUp(object my_json_data, string url_api)
        {
            try
            {
                string ActualTokenMinutes = DateTime.UtcNow.ToString("yyyyMMddHHmm", CultureInfo.InvariantCulture);
                string Token = SessionHelpers.GetToken();

                /* Inicio Ejecutar API */
                var my_json_login = new
                {
                    Username = WebConfigurationManager.AppSettings["User_ApiFollowUp"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_ApiFollowUp"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["Url_ApiFollowUp"].ToString();
                string l_token = "";

                if (Token == "")
                {
                    l_token = executeAPI(my_json_login, l_url + "/api/login/authenticate", "");
                    l_token = l_token.Replace("\"", "");

                    HttpContext.Current.Session["Session_Token"] = l_token;
                    HttpContext.Current.Session["Session_TokenMinutes"] = ActualTokenMinutes;
                }
                else
                {
                    if (SessionHelpers.IsTokenAvailable())
                    {
                        l_token = Token;
                    }
                    else
                    {
                        l_token = executeAPI(my_json_login, l_url + "/api/login/authenticate", "");
                        l_token = l_token.Replace("\"", "");

                        HttpContext.Current.Session["Session_Token"] = l_token;
                        HttpContext.Current.Session["Session_TokenMinutes"] = ActualTokenMinutes;
                    }
                }                
                
                string l_return_api = Helpers.executeAPI
                    (
                        my_json_data, 
                        l_url + url_api, 
                        l_token
                    );
                var json_data = JsonConvert.DeserializeObject<ApiResponse>(l_return_api);
                /* Fin Ejecutar API */
                return json_data;
            }
            catch (Exception e)
            {
                return new ApiResponse {
                    Status = -1,
                    Message = "Error",
                    Value = "3000",
                    Exception = e.Message
                };
            }
        }

    }
}