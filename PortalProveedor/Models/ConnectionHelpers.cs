﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PortalProveedor.Models
{
    public class ConnectionHelpers
    {
        public static SqlConnection ConnectToSql(string ps_name)
        {
            string ls_connection = System.Configuration.ConfigurationManager.ConnectionStrings[ps_name].ConnectionString;
            return new SqlConnection(ls_connection);
        }
    }
}