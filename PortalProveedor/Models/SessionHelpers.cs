﻿using Newtonsoft.Json;
using PortalProveedor.Models.Dao;
using PortalProveedor.Models.Entity;
using System;
using System.Globalization;
using System.Web;
using System.Web.SessionState;

namespace PortalProveedor.Models
{
    public class SessionHelpers
    {
        public static bool SetAuthenticated()
        {
            try
            {
                HttpContext.Current.Session["Session_Authenticated"] = true;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsAuthenticated()
        {
            try
            {
                return Convert.ToBoolean(HttpContext.Current.Session["Session_Authenticated"]);
            }
            catch
            {
                return false;
            }
        }
        public static decimal GetSessionLog(HttpSessionState p_session = null)
        {
            try
            {
                if (p_session != null)
                {
                    return Convert.ToDecimal(p_session["Session_SessionLog"]);
                }
                return Convert.ToDecimal(HttpContext.Current.Session["Session_SessionLog"]);
            }
            catch
            {
                return 0;
            }
        }
        public static bool InitializeSession(Login_Model p_Model)
        {
            try
            {
                var User = GetVirtualUser();
                var Profile = GetVirtualProfile(p_Model.ProfileId);

                p_Model.Username = User.Username;
                p_Model.ProfileId = Profile.CodiPerf;
                                
                HttpContext.Current.Session["Session_AppId"] = p_Model.AppId;
                HttpContext.Current.Session["Session_IPAddress"] = p_Model.IPAddress;
                HttpContext.Current.Session["Session_PcName"] = p_Model.PcName;
                
                HttpContext.Current.Session["Session_SessionLog"] = StartSession(p_Model);
                HttpContext.Current.Session["Session_SessionID"] = HttpContext.Current.Session.SessionID + "l" + Convert.ToString(GetSessionLog());

                HttpContext.Current.Session["Session_ProfileId"] = Profile.CodiPerf;
                HttpContext.Current.Session["Session_ProfileDescription"] = Profile.Descripcion;
                HttpContext.Current.Session["Session_RolId"] = Profile.CodiRol;

                HttpContext.Current.Session["Session_Username"] = User.Username;
                HttpContext.Current.Session["Session_Email"] = User.Email;
                HttpContext.Current.Session["Session_Nombre"] = User.Nombre;
                HttpContext.Current.Session["Session_PersonaId"] = User.CodiPers;
                HttpContext.Current.Session["Session_ProveedorId"] = User.CodiProv;
                HttpContext.Current.Session["Session_AnalistaId"] = User.CodiFana;
                HttpContext.Current.Session["Session_SapId"] = User.CodiSap;                
                HttpContext.Current.Session["Session_FichaSap"] = User.FichaSap;
                HttpContext.Current.Session["Session_TrabajadorId"] = User.CodiTrab;
                HttpContext.Current.Session["Session_Ruc"] = User.Ruc;
                HttpContext.Current.Session["Session_RazonSocial"] = User.RazonSocial;
                HttpContext.Current.Session["Session_UrlFoto"] = User.UrlFoto;

                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool DestroySession()
        {
            decimal l_sessionid;
            try
            {
                l_sessionid = GetSessionLog();
                if (HttpContext.Current.Session.Count != 0)
                {
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Abandon();
                    //HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                }
                EndSession(new Login_Model
                {
                    SessionId = l_sessionid
                });
                return true;
            }
            catch
            {
                return false;
            }
        }                
        public static int GetAppId()
        {
            try
            {
                return Convert.ToInt32(HttpContext.Current.Session["Session_AppId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static string GetIPAddress()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_IPAddress"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetPcName()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_PcName"]);
            }
            catch
            {
                return null;
            }
        }        
        public static string GetSessionID()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_SessionID"]);
            }
            catch
            {
                return "";
            }
        }

        public static int GetProfileId()
        {
            try
            {
                return Convert.ToInt32(HttpContext.Current.Session["Session_ProfileId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static string GetProfileDescription()
        {
            try
            {
                return HttpContext.Current.Session["Session_ProfileDescription"].ToString();
            }
            catch
            {
                return null;
            }
        }
        public static Int64 GetRolId()
        {
            try
            {
                return Convert.ToInt64(HttpContext.Current.Session["Session_RolId"]);
            }
            catch
            {
                return 0;
            }
        }

        public static string GetUsername()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_Username"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetEmail()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_Email"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetNombre()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_Nombre"]);
            }
            catch
            {
                return null;
            }
        }
        public static Int64 GetPersonaId()
        {
            try
            {
                return Convert.ToInt64(HttpContext.Current.Session["Session_PersonaId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static Int64 GetProveedorId()
        {
            try
            {
                return Convert.ToInt64(HttpContext.Current.Session["Session_ProveedorId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static int GetAnalistaId()
        {
            try
            {
                return Convert.ToInt32(HttpContext.Current.Session["Session_AnalistaId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static string GetSapId()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_SapId"]);
            }
            catch
            {
                return null;
            }
        }        
        public static string GetFichaSap()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_FichaSap"]);
            }
            catch
            {
                return null;
            }
        }
        public static Int64 GetTrabajadorId()
        {
            try
            {
                return Convert.ToInt64(HttpContext.Current.Session["Session_TrabajadorId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static string GetRuc()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_Ruc"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetRazonSocial()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_RazonSocial"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetUrlFoto()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_UrlFoto"]);
            }
            catch
            {
                return null;
            }
        }
        
        public static string GetToken()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_Token"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetTokenMinutes()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Session_TokenMinutes"]);
            }
            catch
            {
                return null;
            }
        }
        public static bool IsTokenAvailable()
        {
            try
            {
                Int64 ActualTokenMinutes = Convert.ToInt64(DateTime.UtcNow.ToString("yyyyMMddHHmm", CultureInfo.InvariantCulture)) - 10;
                Int64 TokenMinutes = Convert.ToInt64(GetTokenMinutes());

                if (ActualTokenMinutes > TokenMinutes)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        #region Azure
        private static decimal StartSession(Login_Model p_Model)
        {
            try
            {
                string url_api = "/api/login/StartSession";

                var json_data = Helpers.ExecuteApiFollowUp(p_Model, url_api);
                if (json_data.Response != null)
                {
                    json_data.Response = JsonConvert.DeserializeObject<Response_Sesion>(json_data.Response.ToString());
                    var l_result = JsonConvert.DeserializeObject<Response_Sesion>(json_data.Response.ToString());

                    return Convert.ToDecimal(l_result.SessionId);
                }
                else
                {
                    return 0;
                }                
            }
            catch
            {
                return 0;
            }
        }
        private static void EndSession(Login_Model entry)
        {
            var my_json_data = entry;
            string url_api = "/api/login/EndSession";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
        }
        public static void RemoveVirtualData()
        {
            try
            {
                HttpContext.Current.Session.Remove("VirtualUser");
            }
            catch
            {
            }
        }
        public static void SetVirtualUser(Response_Login user)
        {
            HttpContext.Current.Session["VirtualUser"] = user;
        }
        public static Response_Login GetVirtualUser()
        {
            try
            {
                return (Response_Login) HttpContext.Current.Session["VirtualUser"];
            }
            catch
            {
                return null;
            }
        }
        public static bool ExistsVirtualProfile(decimal CodiPerf)
        {
            try
            {
                bool existProfile = false;
                var profiles = ((Response_Login) HttpContext.Current.Session["VirtualUser"]).Profile;
                foreach (LoginProfileModel profile in profiles)
                {
                    if (profile.CodiPerf == CodiPerf)
                    {
                        existProfile = true;
                    }
                }
                return existProfile;
            }
            catch
            {
                return false;
            }
        }
        public static LoginProfileModel GetVirtualProfile(decimal CodiPerf)
        {
            try
            {
                LoginProfileModel l_return = null;
                var profiles = ((Response_Login)HttpContext.Current.Session["VirtualUser"]).Profile;
                foreach (LoginProfileModel profile in profiles)
                {
                    if (profile.CodiPerf == CodiPerf)
                    {
                        l_return = profile;
                    }
                }
                return l_return;
            }
            catch
            {
                return null;
            }
        }
        #endregion Azure
    }
}