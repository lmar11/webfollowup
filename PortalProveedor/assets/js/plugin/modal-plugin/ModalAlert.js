﻿(function ($) {
    $.fn.ModalAlert = function (Options) {
        var _Settings = jQuery.extend(true, {
            HeaderColor: null,
            Icon: null,
            Type: "Info",
            Color: "Info",
            Draggable: false,
            Text: "¿Está seguro que desea guardar?",
            Confirm: function () { return true; },
            Save: function () { return true; },
            Delete: function () { return true; },
            Cancel: function () { }
        }, Options);
        var element;
        function _init() {
            InstancesValidation();
            Build();
            _cssStyle();
        }
        function InstancesValidation() {
            if (!window.jQuery) {
                alert("jQuery no está cargado.");
            } else if (!window.jQuery.ui) {
                alert("jQuery UI no está cargado.");
            }
        }
        function _cssStyle() {
            if (_Settings.HeaderColor == null) {
                switch (_Settings.Color) {
                    case "Info":
                        _Settings.HeaderColor = "#3561ff";
                        break;
                    case "Success":
                        _Settings.HeaderColor = "#00d811";
                        break;
                    case "Warning":
                        _Settings.HeaderColor = "#f18807";
                        break;
                    case "Danger":
                        _Settings.HeaderColor = "#f10707";
                        break;
                    default:
                        _Settings.HeaderColor = "#c0c0c0";
                        break;
                }
            }
            $(` <style type='text/css'> 
                    @media (max-width: 576px){
                        .modal-plugin-container{width: 300px !important;}
                    }
                    .modal-plugin{position: fixed; top: 0; left: 0; z-index: 1100; width: 100%; height: 100%; 
                        background-color: #000000ad; display: flex; align-items: center; font-family: 'Lato', sans-serif;
                        justify-content: center; color: #616161;}
                    .modal-plugin button{outline: none;}
                    .modal-plugin-container{background-color: white; max-width: 500px; border-radius: 10px}
                    .modal-plugin-header{padding: 30px; background-color: white; color: white; display: flex; 
                        justify-content: center; border-top-left-radius: inherit;
                        border-top-right-radius: inherit; background-image: linear-gradient(45deg,
                        ` + _Settings.HeaderColor + ` 50%, ` + _Settings.HeaderColor + `a3 50%, ` + _Settings.HeaderColor + `f3);}
                    .modal-plugin-header button{width: 50px; height: 50px; position: absolute; border-radius: 25px;
                        border: 3px solid white; background: ` + _Settings.HeaderColor + `; color: white !important;}
                    .modal-plugin-body {padding: 30px 20px;}
                    .modal-plugin-body label{font-size: 14px !important; text-align: center; display: block; color: #464646;}
                    .modal-plugin-footer{display: flex; align-items: center; justify-content: center; 
                        border-top: 1px solid ` + _Settings.HeaderColor + `5e; padding: 10px;}
                    .modal-plugin-footer button:first-child{border: 2px solid ` + _Settings.HeaderColor + `; 
                        background: ` + _Settings.HeaderColor + `; color: white; padding: 10px 20px; cursor: pointer;}
                    .modal-plugin-footer button:last-child{border: 2px solid ` + _Settings.HeaderColor + `; 
                        background: white; color: black; padding: 10px 20px; margin-left: 10px; cursor: pointer;}
                </style>`).appendTo(element);
        }
        function Build() {
            if (_Settings.Icon == null) {
                switch (_Settings.Color) {
                    case "Info":
                        _Settings.Icon = "fa fa-info";
                        break;
                    case "Success":
                        _Settings.Icon = "fa fa-check";
                        break;
                    case "Warning":
                        _Settings.Icon = "fa fa-exclamation";
                        break;
                    case "Danger":
                        _Settings.Icon = "fa fa-ban" // fa-times;
                        break;
                    default:
                        _Settings.Icon = "fa fa-info";
                        break;
                }
            }
            element =
                $("<div>", { "class": "modal-plugin" }).append(
                    $("<div>", { "class": "modal-plugin-container" }).append(
                        $("<div>", { "class": "modal-plugin-header" }).append(
                            $("<button>").append(
                            $("<i>", { "class": _Settings.Icon })
                            )
                        )
                    ).append(
                        $("<div>", { "class": "modal-plugin-body" }).append(
                            $("<label>", { "text": _Settings.Text })
                        )
                    ).append(
                        $("<div>", { "class": "modal-plugin-footer" })
                    )).appendTo("body");
            switch (_Settings.Type) {
                case "Info":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "Cerrar" }));
                    break;
                case "Confirm":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-confirm-modal-plugin", "text": "SI" }));
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "NO" }));
                    break;
                case "Delete":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-delete-modal-plugin", "text": "ELIMINAR" }));
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "CANCELAR" }));
                    break;
                case "Save":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-save-modal-plugin", "text": "GUARDAR" }));
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "CANCELAR" }));
                    break;
                default:
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "Cerrar" }));
                    break;
            }

            if (_Settings.Draggable) {
                element.find(".modal-plugin-container").draggable({
                    handle: ".modal-plugin-header"
                });
            }

            element.find("button.btn-confirm-modal-plugin").on("click", function () {
                if (_Settings.Confirm())
                    element.remove();
            });
            element.find("button.btn-save-modal-plugin").on("click", function () {
                if (_Settings.Save())
                    element.remove();
            });
            element.find("button.btn-delete-modal-plugin").on("click", function () {
                if (_Settings.Delete())
                    element.remove();
            });
            element.find("button.btn-cancel-modal-plugin").on("click", function () {
                element.remove();
            });
        }
        (function () {
            _init();
        }());
    };
    $.fn.ModalAlertPlugin = function (Options) {
        var _Settings = jQuery.extend(true, {
            HeaderColor: null,
            Color: "Info",
            Type: null,
            Icon: null,
            CssContent: "",
            Draggable: false,
            Content: $("<p>Ingrese aqui el contenido del modal.</p>"),
            Save: function () { return true; },
            Confirm: function () { return true; },
            Delete: function () { return true; },
            Cancel: function () { },
            Element: null
        }, Options);
        var element;
        function _init() {
            Build();
            _cssStyle();
            _Settings.Element = element;
        }
        function _cssStyle() {
            if (_Settings.HeaderColor == null) {
                switch (_Settings.Color) {
                    case "Info":
                        _Settings.HeaderColor = "#3561ff";
                        break;
                    case "Success":
                        _Settings.HeaderColor = "#00d811";
                        break;
                    case "Warning":
                        _Settings.HeaderColor = "#f18807";
                        break;
                    case "Danger":
                        _Settings.HeaderColor = "#f10707";
                        break;
                    default:
                        _Settings.HeaderColor = "#c0c0c0";
                        break;
                }
            }
            $(` <style type='text/css'> 
                    @media (max-width: 576px){
                        .modal-plugin-container{width: 300px !important;}
                    }
                    .modal-plugin{position: fixed; top: 0; left: 0; z-index: 1100; width: 100%; height: 100%;
                        background-color: #000000ad; display: flex; align-items: center; font-family: 'Lato', sans-serif;
                        justify-content: center; color: #616161;}
                    .modal-plugin button{outline: none;}
                    .modal-plugin-container{background-color: white; width: 500px; border-radius: 10px}
                    .modal-plugin-header{padding: 30px; background-color: white; color: white; display: flex;
                        justify-content: center; border-top-left-radius: inherit;
                        border-top-right-radius: inherit; background-image: linear-gradient(45deg,
                        ` + _Settings.HeaderColor + ` 50%, ` + _Settings.HeaderColor + `a3 50%, ` + _Settings.HeaderColor + `f3);}
                    .modal-plugin-header button{width: 50px; height: 50px; position: absolute; border-radius: 25px;
                        border: 3px solid white; background: ` + _Settings.HeaderColor + `; color: white !important;}
                    .modal-plugin-body {font-size: 14px !important; display: block; padding: 30px 20px;}
                    .modal-plugin-footer{display: flex; align-items: center; justify-content: center; 
                        border-top: 1px solid ` + _Settings.HeaderColor + `; padding: 20px;}
                    .modal-plugin-footer button:first-child{border: 2px solid ` + _Settings.HeaderColor + `; 
                        background: ` + _Settings.HeaderColor + `; color: white; padding: 10px 20px; cursor: pointer;}
                    .modal-plugin-footer button:last-child{border: 2px solid ` + _Settings.HeaderColor + `; 
                        background: white; color: black; padding: 10px 20px; margin-left: 10px; cursor: pointer;}
                    ` + _Settings.CssContent + `
                </style>`).appendTo(element);
        }
        function Build() {
            if (_Settings.Icon == null) {
                switch (_Settings.Color) {
                    case "Info":
                        _Settings.Icon = "fa fa-info";
                        break;
                    case "Success":
                        _Settings.Icon = "fa fa-check";
                        break;
                    case "Warning":
                        _Settings.Icon = "fa fa-exclamation";
                        break;
                    case "Danger":
                        _Settings.Icon = "fa fa-ban" // fa-times;
                        break;
                    default:
                        _Settings.Icon = "fa fa-info";
                        break;
                }
            }
            element =
                $("<div>", { "class": "modal-plugin" }).append(
                $("<div>", { "class": "modal-plugin-container" }).append(
                    $("<div>", { "class": "modal-plugin-header" }).append(
                            $("<button>").append(
                            $("<i>", { "class": _Settings.Icon })
                            )
                        )
                    ).append(
                    $("<div>", { "class": "modal-plugin-body" }).append(_Settings.Content)
                    ).append(
                        $("<div>", { "class": "modal-plugin-footer" })
                    )
                ).appendTo("body");

            switch (_Settings.Type) {
                case "Info":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "Cerrar" }));
                    break;
                case "Confirm":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-confirm-modal-plugin", "text": "SI" }));
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "NO" }));
                    break;
                case "Delete":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-delete-modal-plugin", "text": "ELIMINAR" }));
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "CANCELAR" }));
                    break;
                case "Save":
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-save-modal-plugin", "text": "GUARDAR" }));
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "CANCELAR" }));
                    break;
                default:
                    element.find(".modal-plugin-footer").append($("<button>", { "class": "btn-cancel-modal-plugin", "text": "Cerrar" }));
                    break;
            }

            if (_Settings.Draggable) {
                element.find(".modal-plugin-container").draggable({
                    handle: ".modal-plugin-header"
                });
            }

            element.find("button.btn-save-modal-plugin").on("click", function () {
                if (_Settings.Save())
                    element.remove();
            });
            element.find("button.btn-confirm-modal-plugin").on("click", function () {
                if (_Settings.Confirm())
                    element.remove();
            });
            element.find("button.btn-delete-modal-plugin").on("click", function () {
                if (_Settings.Delete())
                    element.remove();
            });
            element.find("button.btn-cancel-modal-plugin").on("click", function () {
                element.remove();
            });
        }
        (function () {
            _init();
        }());
    };
})(jQuery);