﻿(function ($) {
    $.fn.ModalFastAlert = function (Options) {
        var _Settings = jQuery.extend(true, {
            MasterColor: null,
            Icon: null,
            Color: "Info",
            Title: null,
            Text: "Colocar Texto Aqui...",
            Draggable: false
        }, Options);
        var element;
        function _init() {
            $(".modal-fast-alert").remove();
            InstancesValidation();
            Build();
            _cssStyle();
        }
        function InstancesValidation() {
            if (!window.jQuery) {
                alert("jQuery no está cargado.");
            } else if (!window.jQuery.ui) {
                alert("jQuery UI no está cargado.");
            }
        }
        function _cssStyle() {
            if (_Settings.MasterColor == null) {
                switch (_Settings.Color) {
                    case "Info":
                        _Settings.MasterColor = "#3561ff";
                        break;
                    case "Success":
                        _Settings.MasterColor = "#00d811";
                        break;
                    case "Warning":
                        _Settings.MasterColor = "#f18807";
                        break;
                    case "Danger":
                        _Settings.MasterColor = "#f10707";
                        break;
                    default:
                        _Settings.MasterColor = "#c0c0c0";
                        break;
                }
            }
            $(` <style type='text/css'> 
                    .modal-fast-alert{
                        position: fixed; 
                        top: 0; 
                        right: 0; 
                        z-index: 1100; 
                        width: 250px; 
                        height: max-content; 
                        background-color: ` + _Settings.MasterColor + `ad; 
                        margin: 20px;
                        border-radius: 5px;
                    }
                    .modal-fast-alert-container{
                        font-family: sans-serif;
                        display: flex; 
                        align-items: center;
                        color: white;                        
                    }
                    .modal-fast-alert-left{
                        width: 40px;
                        padding-left: 10px;
                        height: 100%;
                        text-align: center;                        
                    }
                    .modal-fast-alert-left i{
                        
                    }
                    .modal-fast-alert-right{
                        padding: 10px
                    }
                    .modal-fast-alert-right label{
                        font-size: 11px !important;
                        color: white !important;
                        font-weight: 600;
                    }
                    .modal-fast-alert-right span{
                        font-size: 12px;
                        display: block;
                    }
                    .close-modal-fast-alert{
                        position: absolute;
                        right: 0;
                        margin-right: 10px;
                        cursor: pointer;
                    }
                </style>`).appendTo(element);
        }
        function Build() {
            if (_Settings.Icon == null) {
                switch (_Settings.Color) {
                    case "Info":
                        _Settings.Icon = "fa fa-info";
                        break;
                    case "Success":
                        _Settings.Icon = "fa fa-check";
                        break;
                    case "Warning":
                        _Settings.Icon = "fa fa-exclamation";
                        break;
                    case "Danger":
                        _Settings.Icon = "fa fa-ban"; // fa-times;
                        break;
                    default:
                        _Settings.Icon = "fa fa-info";
                        break;
                }
            }
            element =
                $("<div>", { "class": "modal-fast-alert" }).append
                (
                    $("<div>", { "class": "modal-fast-alert-container" }).append
                    (
                        $("<div>", { "class": "modal-fast-alert-left" }).append
                        (
                            $("<i>", { "class": _Settings.Icon })
                        )
                    ).append
                    (
                        $("<div>", { "class": "modal-fast-alert-right" }).append
                        (
                            $("<label>", { "text": _Settings.Title }).append
                            (
                                $("<i>", { "class": "fa fa-times close-modal-fast-alert" })
                            )
                        ).append
                        (
                            $("<span>", { "text": _Settings.Text })
                        )
                    )
                ).appendTo("body");

            if (_Settings.Draggable) {
                element.find(".modal-plugin-container").draggable({
                    handle: ".modal-plugin-header"
                });
            }
            var count = 0;
            element.find(".close-modal-fast-alert").on("click", function () {
                element.remove();
            });   
            element.on("mouseover", function () {
                count = 0;
                clearInterval(interval);
            });
            element.on("mouseleave", function () {
                count = 0;
                interval = setInterval(function () {
                    count++;
                    console.log(count);
                    if (count > 3) {
                        clearInterval(interval);
                        element.fadeOut();
                        setTimeout(function () {
                            element.remove();
                        }, 500);
                    }
                }, 1000);
            });
            var interval = setInterval(function () {
                count++;
                console.log(count);
                if (count > 3) {
                    clearInterval(interval);
                    element.fadeOut();
                    setTimeout(function () {
                        element.remove();
                    }, 500);
                }                
            }, 1000);
        }
        (function () {
            _init();
        }());
    };
})(jQuery);