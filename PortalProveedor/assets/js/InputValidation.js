﻿"use strict";

function jQueryNumeric(input) {
    input.on("keydown", function (event) {        
        if ((event.which < 48 || event.which) > 57 && event.which != 8 && event.which != 9
            && event.which != 17 && event.which != 86 && event.which != 37 && event.which != 39) {
            event.preventDefault();
        }
    });
}
function jQueryMoneda(input) {
    input.on("keydown", function (event) {
        if ((event.which < 48 || event.which) > 57 && event.which != 8 && event.which != 9 && event.which != 190
            && event.which != 17 && event.which != 86 && event.which != 37 && event.which != 39) {
            event.preventDefault();
        }
    });
}
function jQueryPhoneNumeric(input) {
    input.on("keydown", function (event) {
        if ((event.which < 48 || event.which > 57) && event.which != 8 && event.which != 9 && event.which != 187
            && event.which != 17 && event.which != 86 && event.which != 37 && event.which != 39) {
            event.preventDefault();
        }
    });
}
function jQueryLetter(input) {
    input.on("keydown", function (event) {
        if ((event.which < 65 || event.which > 90) && event.which != 186 && event.which != 32 && event.which != 8 &&
            event.which != 9 && event.which != 192 && event.which != 17 && event.which != 86
            && event.which != 37 && event.which != 39) {
            event.preventDefault();
        }
    });
}
function jQueryTotalLetter(input) {
    input.on("keydown", function (event) {
        if ((event.which < 65 || event.which > 90) && event.which != 186 && event.which != 32 && event.which != 8 &&
            event.which != 9 && (event.which < 129 || event.which > 165) && event.which != 192
            && event.which != 17 && event.which != 86 && event.which != 37 && event.which != 39) {
            event.preventDefault();
        }
    });
}
function jQueryLetterNumeric(input) {
    input.on("keydown", function (event) {
        if ((event.which < 65 || event.which > 90) && event.which != 186 && event.which != 32 &&
            event.which != 8 && event.which != 9 && (event.which < 48 || event.which > 57) && event.which != 192
            && event.which != 17 && event.which != 86 && event.which != 37 && event.which != 39) {
            event.preventDefault();
        }
    });
}
function jQuerySize(input, size) {
    input.on("keydown", function (event) {
        var len = parseInt(input.val().length) + 1;
        var code = event.which;
        if (len > size && code != 8 && code != 9) {
            event.preventDefault();
        }
    });
}

function DateToFormatDate(date) {
    var Split = date.split('/');
    var FinalDate = Split[2] + "-" + Split[1] + "-" + Split[0];
    return FinalDate;
}
function StringDateToJavaScriptDate(date) {
    var SplitDate1 = date.split("/");
    var SplitDate2 = date.split("-");

    var NewDate = "";
    var Day = "";
    var Month = "";
    var Year = "";

    if (SplitDate1.length == 3) {
        Day = SplitDate1[0];
        Month = SplitDate1[1];
        Year = SplitDate1[2];
    } else if (SplitDate2.length == 3) {
        Day = SplitDate2[2];
        Month = SplitDate2[1];
        Year = SplitDate2[0];
    }
    
    NewDate = Month + "/" + Day + "/" + Year;

    return new Date(NewDate);
}
function DateToFormatDateServer(date) {
    var SplitDate = date.split("-");

    var NewDate = "";
    var Day = "";
    var Month = "";
    var Year = "";

    Day = SplitDate[2];
    Month = SplitDate[1];
    Year = SplitDate[0];

    NewDate = Day + "/" + Month + "/" + Year;

    return NewDate;
}

function WaitingReadyElement(element) {
    var width = element.width();
    var height = element.height();
    var style = "";

    if (width > height) {
        style = 'max-width: 100%; height: 100%;';
    } else {
        style = 'width: 100%; max-height: 100%;';
    }

    var img = $(`<div style='position: absolute; opacity: 0.7; width: 100%; height: 100%; border-radius: inherit;
                            background: white;'>
                    <img src='assets/image/loading/loading_1.gif' 
                        style='margin: auto; display: block; border-radius: inherit;` + style + `'></img>
                <div>`);
    element.append(img);
    return img;
}