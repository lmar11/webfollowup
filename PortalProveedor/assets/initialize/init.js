﻿//Initialize Global Variables
g_app.$Loading = $("#PageLoading");
g_app.$Menu = $('#AppMenu');
g_app.ObjectModule = { items: new Array() };
g_app.$PartialMenu = $('#AppPartialMenu');
g_app.$Page = $('#AppPage');
g_app.$PageHeader = $('#AppPageHeader');
g_app._get = new Date().getTime();
g_app.api = { fileuploader: null };
g_app.filter = new Object();
g_app.globals = new Object();
g_app.home = "/";
g_app.isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (g_app.isMobile.Android() || g_app.isMobile.BlackBerry() || g_app.isMobile.iOS() || g_app.isMobile.Opera() || g_app.isMobile.Windows());
    }
};
g_app.menu = { currentId: 0, items: new Array() };
g_app.menuSelected = new Object();
g_app.menuSettings = false;
g_app.pageHeader = new Array();

(function () {
    BuildMenu();
    GetModules(); 
    GetNotifications();
})();

function GetModules() {
    var AppModule = g_app.ObjectModule;
    $.ajax({
        data: JSON.stringify({ "ParentId": 0 }),
        url: '/Home/GetModuleObject',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: _success,
        complete: _complete
    });
    function _success(r) {
        $.each(r.Response, function (index, object) {
            AppModule.items.push({
                "Id": object.CodiRubr,
                "Description": object.Descripcion,
                "$": null
            });
        });
    }
    function _complete(xhr, status) {
        BuildModules();
    }
}
function BuildModules() {
    var AppMenu = g_app.$Menu;
    var AppModuleItems = g_app.ObjectModule.items;
    var AppLi = $('<li class="nav-item active nav-module-item"></li>');

    var AppLiA = $(`<a class="collapsed nav-module-item" data-toggle="collapse" href="#nav_modules" aria-expanded="false">
                        <i class="fas fa-home"></i>
                        <p></p>
                        <span class="caret"></span>
                    </a>`);

    var AppUl = $('<div class="collapse" id="nav_modules"><ul class="nav nav-collapse"></ul></div>');    
    AppLi.append(AppLiA);
    AppLi.append(AppUl);

    $.each(AppModuleItems, function (index, object) {
        var AppUlLiA = $('<li><a href="#" class="nav-module-item"><span class="sub-item">' + object.Description + '</span></a></li>');
        AppUl.find("ul").append(AppUlLiA);
        AppUlLiA.on("click", "a", function (event) {
            event.preventDefault();
            AppLi.find("p").text(object.Description);
            ShowMenu(object.Id);
        });
    });
    AppMenu.prepend(AppLi);
    $(AppLi.find("div li")[0]).find("a").trigger("click");
}
function ShowMenu(RubroId) {
    var AppMenu = g_app.$Menu;
    AppMenu.find("li.nav-item:not(.nav-module-item)").addClass("d-block-none");
    AppMenu.find("li[data-rubro=" + RubroId + "]").removeClass("d-block-none");

    (function () {
        var l_url = window.location.hash.replace('#ajax/', '').split('?')[0];
        var l_item = g_app.fn_getItemAppMenu(l_url);
        
        if (l_item != null) {
            var l_parents = l_item.$.parentsUntil(g_app.$Menu);
            l_parents.each(function () {
                if ($(this).prop("tagName").toLowerCase() == "li") {
                    $(this).find("a").addClass('active');
                }
            });
            g_app.menu.currentId = parseInt(l_item.Id);
            g_app.hash = l_url;
            g_app.fn_loadAppPage(l_url, l_item.Description, null, null, false);
        } else {
            if (typeof g_app.globals.MenuActiveItem == 'undefined' || g_app.globals.MenuActiveItem == null) {
                var li = $(g_app.$Menu.find('li.nav-item:not(.nav-module-item)')[0]).find("a");
                li.trigger('click');
            } else {
                for (var i in g_app.menu.items) {
                    if (g_app.menu.items[i].Id == g_app.globals.MenuActiveItem) {
                        g_app.menu.items[i].$.trigger('click');
                        break;
                    }
                }
            }
        }
    }());
}
function BuildMenu() {
    var AppMenu = g_app.$Menu;
    var InitialId = 0;
    //clear items app
    g_app.menu.items = new Array();
    AppMenu.empty();
    AppMenu.data('Id', InitialId);

    //get item
    lf_getItem(InitialId, lf_buildMenu);
    function lf_getItem(p_Id, p_callback) {
        $.ajax({
            data: JSON.stringify({ "ParentId": p_Id }),
            url: '/Home/GetSecurityObject',
            type: 'post',
            dataType: 'json',
            async: false,
            contentType: "application/json; charset=utf-8",
            success: _success,
            complete: _complete
        });
        function _success(r) {
            $.each(r.Response, function (index, object) {
                g_app.menu.items.push({
                    "Id": object.Id,
                    "Description": object.Description,
                    "Icon": object.Icon,
                    "Color": object.Color,
                    "Controller": object.Controller,
                    "Function": object.Action,
                    "FatherId": p_Id,
                    "IsCompleted": object.Completed,
                    "IsDraw": false,
                    "IsPartialView": object.Completed,
                    "Menu": null,
                    "RubroId": object.CodiRubr,
                    "RubroDescription": object.Rubro,
                    "$": null
                });
            });
        }
        function _complete(xhr, status) {
            var li_NextId = null;
            //update IsCompleted
            for (var i in g_app.menu.items) {
                if (g_app.menu.items[i].Id == p_Id) {
                    g_app.menu.items[i].IsCompleted = true;
                    break;
                }
            }
            //all completed ?
            for (var i in g_app.menu.items) {
                if (!g_app.menu.items[i].IsCompleted) {
                    li_NextId = g_app.menu.items[i].Id;
                    break;
                }
            }
            //callback
            if (li_NextId == null) {
                if (typeof p_callback !== 'undefined') {
                    p_callback();
                }
            } else {
                lf_getItem(li_NextId, p_callback);
            }
            ShowSubMenuAtlantis();            
        }
    }
    function lf_buildMenu() {
        var l_index = 0;
        _build(AppMenu);        
        while (l_index < g_app.menu.items.length) {
            if (g_app.menu.items[l_index].Menu !== null) {
                _build($(g_app.menu.items[l_index].Menu));
            }
            l_index++;
        }
        //append js
        $('body').append(`<script src="` + g_app.home + `assets/initialize/initScript.js?v=` + g_app._get + `"></script>`);

        function _build(p_Father) {
            var _Id = parseInt(p_Father.data('Id'));
            //draw item
            for (var i in g_app.menu.items) {
                if (g_app.menu.items[i].FatherId == _Id && !g_app.menu.items[i].IsDraw) {
                    //are initial fathers ?
                    if (_Id == InitialId) {
                        //are multilevel ?
                        if (g_app.menu.items[i].IsPartialView) {
                            p_Father.append(_getHtmlItem(g_app.menu.items[i]));
                        } else {
                            p_Father.append(_getHtmlTreeview(g_app.menu.items[i]));
                        }
                    } else {
                        //are multilevel ?
                        if (g_app.menu.items[i].IsPartialView) {
                            p_Father.find("ul").append(_getHtmlSubItem(g_app.menu.items[i]));
                            //p_Father.find("ul").append(_getHtmlSubItem(g_app.menu.items[i]));
                        } else {
                            p_Father.append(_getHtmlTreeview(g_app.menu.items[i]));
                        }
                    }
                    g_app.menu.items[i].IsDraw = true;
                }
            }
        }
        function _getHtmlItem(item) {
            var AppLi = $('<li class="nav-item d-block-none"></li>'),
                AppLiA = $('<a></a>');
            AppLi.attr("data-rubro", item.RubroId);
            //build menu>li>a
            AppLiA.attr('href', item.Controller + '/' + item.Function);
            AppLiA.append('<i style="color: ' + item.Color + '" class="fa ' + item.Icon + '"></i><p>' + item.Description + '</p>');
            //save $ in item
            item.$ = AppLiA;
            //build menu>li
            AppLi.append(AppLiA);
            //save item
            AppLi.data('Item', item);
            return AppLi;
        }
        function _getHtmlTreeview(item) {
            //var div = $('<div class="navbar-dropdown animated fadeIn"></div>');
            var AppLi = $('<li class="nav-item d-block-none"></li>'),
                AppLiA = $('<a data-toggle="collapse"></a>'),
                AppLiUl = $('<div class="collapse" id="nav' + item.Id + '"><ul class="nav nav-collapse"></ul></div>');
            AppLi.attr("data-rubro", item.RubroId);
            //build menu>li>a
            AppLiA.attr('href', '#nav' + item.Id);
            AppLiA.append('<i style="color: ' + item.Color + '" class="nav-icon fa ' + item.Icon + '"></i><p>' + item.Description + '<span class="caret"></span></p>');
            //save $ in item
            item.$ = AppLiA;
            //build menu>li>ul
            //AppLiUl.addClass('pcoded-submenu');
            AppLiUl.data('Id', item.Id);
            //build menu>li
            //AppLi.addClass('pcoded-hasmenu');
            AppLi.append(AppLiA);
            AppLi.append(AppLiUl);
            //save item
            AppLi.data('Item', item);
            //save content
            item.Menu = AppLiUl;
            return AppLi;
        }
        function _getHtmlSubItem(item) {
            var AppLi = $('<li class="d-block-none"></li>'),
                AppLiA = $('<a></a>');
            AppLi.attr("data-rubro", item.RubroId);
            //build menu>li>a
            AppLiA.attr('href', item.Controller + '/' + item.Function);
            AppLiA.append('<span class="sub-item">' + item.Description + '</span>');
            //save $ in item
            item.$ = AppLiA;
            //build menu>li
            AppLi.append(AppLiA);
            //save item
            AppLi.data('Item', item);
            return AppLi;
        }
    }   
}
function ShowSubMenuAtlantis() {
    var navItemClicked = $('.page-navigation').find(".nav-item");
    navItemClicked.off("click");
    navItemClicked.on("click", function (e) {
        if (window.matchMedia('(max-width: 991px)').matches) {
            if (!($(this).hasClass('show-submenu'))) {
                navItemClicked.removeClass('show-submenu');
                $(this).addClass('show-submenu');
            } else {
                $(this).removeClass('show-submenu');
            }
        }
    });
}

/* Opciones Navbar */
function LoadGlobalScriptNavbar(NavBarModel) {
    $("#logout").on("click", function () {
        $.ajax({
            data: null,
            url: '/Account/Logout',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            complete: _complete
        });
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _complete() {
        window.location = '/';
        //HideLoading();
    }
}

/* Notificaciones */
function GetNotifications() {
    $.ajax({
        data: null,
        url: '/Account/List_Notificaciones',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: _success,
        complete: _complete
    });
    function _success(r) {
        var count = 0;
        $.each(r.Response, function (index, object) {
            count++;
            var item = $(`<a href="#" style="align-items: center;">
							<div class="notif-icon notif-primary" style="padding:12px;">
                                <i class="fa fa-comment"></i>
                            </div>
							<div class="notif-content">
								<span class="block">
									` + object.Titulo + `
								</span>
								<span class="time">` + object.Detalle + `</span> 
							</div>
						</a>`);
            $("#notification-body").append(item);
        });
        $("#notification-number-title").text("Tienes " + count + " notificacion(es) nueva(s)");
        $("#notification-number").text(count);
    }
    function _complete(xhr, status) {
    }
}

/* Session */
(function () {
    window.onfocus = function (e) {
        if (e.target !== this) {
            return;
        }
        var l_status = false, l_session, l_window_session = sessionStorage.getItem("window-session");
        $.ajax({
            data: null,
            url: g_app.home + 'Account/GetStatus',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () { },
            success: _success,
            complete: _complete,
            error: function () { }
        });
        function _success(r) {
            l_status = r.status;
            l_session = r.session;
        }
        function _complete() {
            if (l_status) {
                if (l_window_session !== l_session) {
                    window.location.reload(true);
                }
            } else {
                window.location = g_app.home + 'Account/Login';
            }
        }
    };
}());
(function () {
    $(document).bind("contextmenu", function (e) {
        return false;
    });
    setInterval(function () {
        $.ajax({
            data: null,
            url: g_app.home + 'Account/KeepActive',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8"
        });
    }, 300000);//active session every 5 minutes
}());
