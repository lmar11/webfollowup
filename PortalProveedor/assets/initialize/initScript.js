﻿g_app.$Menu.on("click", "li a",  function (evt) {
    evt.preventDefault();  
    if ($(this).hasClass("nav-module-item")) {
        return;
    }

    var AppMenu = g_app.$Menu,
        item = $(this).closest('li').data("Item"),
        parents = $(this).parentsUntil(AppMenu);

    if (item.IsPartialView) {        
        AppMenu.find('li a.active').each(function () {
            $(this).removeClass('active');
        });
        parents.each(function () {
            if ($(this).prop("tagName").toLowerCase() == "li") {
                $(this).find("a").addClass('active');
            }
        });
        g_app.menu.currentId = parseInt(item.Id);
        ShowLoading();
        g_app.fn_loadAppPage((item.Controller + "/" + item.Function), item.Description, undefined, undefined, true);
    }
});
/* --------------------------------------------------------
        Page Header
    --------------------------------------------------------   */
$(document).on("click", ".AppPageHeader a", function (evt) {
    evt.preventDefault();
    var ls_url, ls_description, l_parameters, l_callback;
    ls_url = $(this).attr("href");
    ls_url = ls_url.substring(1, ls_url.length);
    ls_description = $(this).text();
    l_parameters = null;
    l_callback = null;
    g_app.fn_loadAppPage(ls_url, ls_description, l_parameters, l_callback, false);
});
g_app.$PageHeader.on("click", "a", function (evt) {
    evt.preventDefault();
    var l_i = parseInt($(this).attr('data-i'));
    var ls_url, ls_description, l_parameters, l_callback;
    ls_url = g_app.pageHeader[l_i].Controller + '/' + g_app.pageHeader[l_i].Function;
    ls_description = g_app.pageHeader[l_i].Description;
    l_parameters = g_app.pageHeader[l_i].Parameters;
    l_callback = g_app.pageHeader[l_i].Callback;
    g_app.fn_loadAppPage(ls_url, ls_description, l_parameters, l_callback);
});

$(window).on('hashchange', function () {
    var l_url = window.location.hash.replace('#ajax/', '').split('?')[0];
    var l_item = g_app.fn_getItemAppMenu(l_url);
    if (l_item != null) {
        ShowLoading();
        g_app.hash = l_url;
        console.log(g_app.menu.currentId);
        if (l_item.Id == g_app.menu.currentId) {
            g_app.fn_loadAppPage(l_url, l_item.Description, g_app.globals.LoadParameters, g_app.globals.LoadCallback, false);
        } else {
            l_item.$.trigger('click');
        }
    }
});
//active item
(function () {
    //var l_url = window.location.hash.replace('#ajax/', '').split('?')[0];
    //var l_item = g_app.fn_getItemAppMenu(l_url);

    //if (l_item != null) {
    //    var l_parents = l_item.$.parentsUntil(g_app.$Menu);
    //    l_parents.each(function () {
    //        if ($(this).prop("tagName").toLowerCase() == "li") {
    //            $(this).find("a").addClass('active');
    //        }
    //    });
    //    g_app.menu.currentId = parseInt(l_item.Id);
    //    g_app.hash = l_url;
    //    g_app.fn_loadAppPage(l_url, l_item.Description, null, null, false);
    //} else {
    //    if (typeof g_app.globals.MenuActiveItem == 'undefined' || g_app.globals.MenuActiveItem == null) {
    //        g_app.$Menu.find('li a').eq(0).trigger('click');
    //    } else {
    //        for (var i in g_app.menu.items) {
    //            if (g_app.menu.items[i].Id == g_app.globals.MenuActiveItem) {
    //                g_app.menu.items[i].$.trigger('click');
    //                break;
    //            }
    //        }
    //    }
    //}
}());