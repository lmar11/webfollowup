﻿"use strict";
$(document).ready(function () {
    var ousername = $("#username"), opassword = $("#password"), oerror = $("#error"), oprofile = $("#profiles"),
        oaccount = $("#accounts"), oerrormessage = $("#error-message"), oprofilelist = $("#profiles-list"),
        oaccountlist = $("#accounts-list"), oprofileloading = $("#profiles-loading"),
        oaccountloading = $("#accounts-loading"), accresponse = "1000",
        vwaiting = false;
    $.ajaxSetup({ cache: false });
    ousername.focus();
    $("form").submit(function (evt) {
        evt.preventDefault();
        $("#btn-login").trigger("click");
    });
    $(document).keypress(function (evt) {
        //evt.preventDefault();
        if (evt.which == 13) {
            $("#btn-login").trigger("click");
        }        
    });
    $("#btn-login").click(function (evt) {
        evt.preventDefault();
        if (vwaiting) {
            return;
        }
        fn_initializeAll();
        var username = ousername.val(), password = opassword.val(), error = false, param;
        if (password.trim() === "") {
            opassword.focus();
            opassword.addClass("login-has-error");
            error = true;
        }
        if (username.trim() === "") {
            ousername.focus();
            ousername.addClass("login-has-error");
            error = true;
        }
        if (error) {
            oerrormessage.html("Campos vacíos");
            oerror.removeClass("d-none");
            return;
        }
        param = {
            "Username": username,
            "Password": password
        };
        console.log(param);
        $.ajax({
            data: JSON.stringify(param),
            url: '/Account/GetLoginValidate',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete,
            error: function () { }
        });
        function _beforeSend() {
            $("#btn-login").prop("disabled", true);
        }
        function _success(r) {
            if (parseInt(r.status) === 1) {
                if (r.response[0][1] != "0") {
                    accresponse = "1000";
                    fn_buildProfiles(r.response); //Modificado solo para este sistema
                    //LogInSeguridadTI(); //Modificado solo para este sistema
                    oerrormessage.html("");
                } else {
                    accresponse = "3000";
                    oerrormessage.html(r.response[0][2]);
                }                
            } else {
                oerrormessage.html(r.response);
                oerror.removeClass("d-none");
            }
        }
        function _complete(xhr, status) {            
            if (accresponse == "1000") {
                fn_account_list();
            }            
            if (oprofilelist.length == 1) {
                oprofilelist.find("button").trigger("click");
            }
        }
        function fn_buildProfiles(p_data) {
            var item;
            if (p_data[0][0] === "Error") {
                oerrormessage.html(p_data[0][2]);
                oerror.removeClass("d-none");
                return;
            }
            //
            oprofilelist.data("Param", param);
            oprofilelist.find("button").remove();
            for (var i in p_data) {
                item = $("<button />");
                item.text(p_data[i][2]);
                item.data("Code", p_data[i][1]);
                //item.addClass('btn btn-primary btn-outline-primary btn-block');
                item.addClass('btn btn-primary btn-block');
                oprofilelist.append(item);
            }
            //oprofile.removeClass("d-none");            
        }
    });
    function fn_account_list() {
        $.ajax({
            data: JSON.stringify({ "Username": ousername.val(), "Password": opassword.val()}),
            url: '/Account/GetAccountList',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete,
            error: function () { }
        });
        function _beforeSend() { }
        function _success(r) {
            if (parseInt(r.status) === 1) {
                fn_buildAccounts(r.data);
                $("#btn-login").prop("disabled", false);
            } else {
                $("#btn-login").prop("disabled", false);
                oerrormessage.html(r.data);
                oerror.removeClass("d-none");
            }
        }
        function _complete(xhr, status) { }
        function fn_buildAccounts(p_data) {
            var item;
            if (p_data[0][0] === "Error") {
                oerrormessage.html(p_data[0][2]);
                oerror.removeClass("d-none");
                return;
            }
            //
            //oaccountlist.data("Param", param);
            oaccountlist.find("button").remove();
            for (var i in p_data) {
                item = $("<button />");
                item.text(p_data[i][2]);
                item.data("codiCuen", p_data[i][1]);
                //item.addClass('btn btn-primary btn-outline-primary btn-block');
                item.addClass('btn btn-primary btn-block');
                oaccountlist.append(item);
            }
            oaccount.removeClass("d-none");
        }
    }
    oaccountlist.on("click", "button", function (evt) {
        evt.preventDefault();
        if (oprofilelist.length == 1) {            
            oprofilelist.find("button").data("codiCuen", $(this).data("codiCuen"));
            oprofilelist.find("button").trigger("click");
        }
    });
    oprofilelist.on('click', 'button', function (evt) {
        evt.preventDefault();
        var param = oprofilelist.data("Param"), go = false;
        param["ProfileId"] = $(this).data("Code");
        console.log(param);
        $.ajax({
            data: JSON.stringify(param),
            url: '/Account/Login',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });
        function _beforeSend() {
            fn_disableProfiles();
            vwaiting = true;
        }
        function _success(r) {
            if (r.status === 1) {
                go = true;
            }
        }
        function _complete() {
            if (go) {
                sessionStorage.setItem('window-profile', param.ProfileId);
                window.location = '/';
            } else {
                fn_enableProfiles();
                vwaiting = false;
            }
        }
    });
    $("#username, #password").keyup(function (evt) {
        evt.preventDefault();
        var text = $(this).val(), username = ousername.val(), password = opassword.val();
        if (text !== "" && $(this).hasClass("login-has-error")) {
            $(this).removeClass("login-has-error");
            if (username !== "" && password !== "") {
                oerror.addClass("d-none");
            }
        }
    });
    //
    function fn_initializeAll() {
        ousername.removeClass("login-has-error");
        opassword.removeClass("login-has-error");
        oerror.addClass("d-none");
        oerrormessage.html("");
        oprofile.addClass("d-none");
        oaccount.addClass("d-none");
        oprofilelist.find("button").remove();
        oaccountlist.find("button").remove();
        oprofileloading.addClass("d-none");
        oaccountloading.addClass("d-none");
    }
    function fn_disableProfiles() {
        oprofilelist.find("button").prop("disabled", true).addClass("disabled");
        oprofileloading.removeClass("d-none");
    }
    function fn_enableProfiles() {
        oprofilelist.find("button").prop("disabled", false).removeClass("disabled");
        oprofileloading.addClass("d-none");
    }
    function fn_disableAccounts() {
        oaccountlist.find("button").prop("disabled", true).addClass("disabled");
        oaccountloading.removeClass("d-none");
    }
    function fn_enableAccounts() {
        oaccountlist.find("button").prop("disabled", false).removeClass("disabled");
        oaccountloading.addClass("d-none");
    }

    function LogInSeguridadTI() {
        var param = new FormData(), go = false;
        param["ProfileId"] = 1;
        console.log(param);
        $.ajax({
            data: JSON.stringify(param),
            url: '/Account/Login',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });
        function _beforeSend() {
            vwaiting = true;
        }
        function _success(r) {
            if (r.status === 1) {
                go = true;
            }
        }
        function _complete() {
            if (go) {
                sessionStorage.setItem('window-profile', param.ProfileId);
                window.location = '/';
            } else {
                vwaiting = false;
            }
        }
    }

    (function () {
        setInterval(function () {
            $.ajax({
                data: null,
                url: 'Account/KeepActive',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8"
            });
        }, 300000);//active session every 5 minutes
    }());
});