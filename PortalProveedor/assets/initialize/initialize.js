﻿"use strict";
$("document").ready(function () {
    //App
    g_app.$Loading = $('#AppLoading');
    g_app.$Menu = $('#AppMenu');
    g_app.$Page = $('#AppPage');
    g_app.$PageHeader = $('#AppPageHeader');
    g_app._get = new Date().getTime();
    g_app.alertify = 0;
    g_app.api = { fileuploader: null };
    g_app.filter = new Object();
    g_app.globals = new Object();
    g_app.home = window.aG9tZQ;
    g_app.isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (g_app.isMobile.Android() || g_app.isMobile.BlackBerry() || g_app.isMobile.iOS() || g_app.isMobile.Opera() || g_app.isMobile.Windows());
        }
    };
    g_app.jgrowlClipboard = 0;
    g_app.menu = { currentId: 0, items: new Array() };
    g_app.menuSettings = false;
    g_app.pageHeader = new Array();
    g_app.pcoded = {
        themelayout: 'vertical',
        verticalMenuplacement: 'left', // value should be left/right
        verticalMenulayout: 'wide', // value should be wide/box
        MenuTrigger: 'click', // click / hover
        SubMenuTrigger: 'click', // click / hover
        activeMenuClass: 'active',
        ThemeBackgroundPattern: 'pattern1',  // pattern1, pattern2, pattern3, pattern4, pattern5, pattern6
        HeaderBackground: 'theme3',  // theme1, theme2, theme3, theme4, theme5  header color
        LHeaderBackground: 'theme6', // theme1, theme2, theme3, theme4, theme5, theme6   brand color
        NavbarBackground: 'theme1', // themelight1, theme1  // light  and dark sidebar
        ActiveItemBackground: 'theme10', // theme1, theme2, theme3, theme4, theme5, theme6, theme7, theme8, theme9, theme10, theme11, theme12  mennu active item color
        SubItemBackground: 'theme2',
        ActiveItemStyle: 'style0',
        ItemBorder: true,
        ItemBorderStyle: 'none',
        NavbarImage: 'true',
        ActiveNavbarImage: 'img1',
        SubItemBorder: true,
        DropDownIconStyle: 'style3', // Value should be style1,style2,style3
        menutype: 'st1', // Value should be st1, st2, st3, st4, st5 menu icon style
        layouttype: 'img', // Value should be light / dark
        FixedNavbarPosition: true,  // Value should be true / false  header postion
        FixedHeaderPosition: true,  // Value should be true / false  sidebar menu postion
        collapseVerticalLeftHeader: true,
        VerticalSubMenuItemIconStyle: 'style7', // value should be style1, style2, style3, style4, style5, style6
        VerticalNavigationView: 'view1',
        verticalMenueffect: {
            desktop: "shrink",
            tablet: "overlay",
            phone: "overlay",
        },
        defaultVerticalMenu: {
            desktop: "expanded", // value should be offcanvas/collapsed/expanded/compact/compact-acc/fullpage/ex-popover/sub-expanded
            tablet: "offcanvas", // value should be offcanvas/collapsed/expanded/compact/fullpage/ex-popover/sub-expanded
            phone: "offcanvas", // value should be offcanvas/collapsed/expanded/compact/fullpage/ex-popover/sub-expanded
        },
        onToggleVerticalMenu: {
            desktop: "offcanvas", // value should be offcanvas/collapsed/expanded/compact/fullpage/ex-popover/sub-expanded
            tablet: "expanded", // value should be offcanvas/collapsed/expanded/compact/fullpage/ex-popover/sub-expanded
            phone: "expanded", // value should be offcanvas/collapsed/expanded/compact/fullpage/ex-popover/sub-expanded
        },
    };
    //Globals
    g_app.globals.MenuActiveItem = null;
    g_app.globals.BackgroundColorInactive = "#ffeadb";
    g_app.globals.SearchByKey = false;
    //jqueryAjax
    $.ajaxSetup({ cache: false });
    //DataTable
    if (typeof $.fn.dataTable != 'undefined') {
        $.extend(true, $.fn.dataTable.defaults, {
            autoWidth: false,
            pagingType: 'full_numbers',
            bProcessing: true,
            aLengthMenu: [[5, 10, 25, 50, 75, 100], [5, 10, 25, 50, 75, 100]],
            iDisplayLength: 5,
            bSortMulti: false,
            iDisplayStart: 0,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
                "sInfoEmpty": "Mostrando del 0 al 0 de 0",
                "sInfoFiltered": "(filtrado de _MAX_)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            }
        });
    }
    //Fileuploader
    if (typeof $.fn.fileuploader != 'undefined') {
        $.fn.fileuploader.defaults.extensions = [];//['jpg', 'jpeg', 'png', 'audio/mp3', 'text/plain'];
        $.fn.fileuploader.defaults.addMore = true;
        $.fn.fileuploader.defaults.enableApi = true;
        $.fn.fileuploader.defaults.limit = null;
        $.fn.fileuploader.defaults.maxSize = 20;// file's maximal size in MB {null, Number} also with the appended files if null - has no limits example: 2
        $.fn.fileuploader.defaults.fileMaxSize = null;// each file's maximal size in MB {null, Number} if null - has no limits example: 2
        $.fn.fileuploader.defaults.dialogs = {
            alert: function (text) {
                toastr.error(text, 'Error');
            },
            confirm: function (text, callback) {
                $.daAlert({
                    ajax: null,
                    content: text,
                    maximizable: false,
                    title: 'Confirmación',
                    buttons: [{
                        type: 'remove',
                        fn: function (p) {
                            p.fn();
                            return true;
                        },
                        parameters: { 'fn': callback }
                    }, {
                        type: 'cancel',
                        fn: function (p) { return true; },
                        parameters: {}
                    }]
                });
            }
        };
        $.fn.fileuploader.defaults.thumbnails.removeConfirmation = true;// show a confirmation dialog by removing a file? {Boolean} it will not be shown in upload mode by canceling an upload
        $.fn.fileuploader.defaults.thumbnails.itemPrepend = true;// insert the thumbnail's item at the beginning of the list? {Boolean}
        $.fn.fileuploader.defaults.thumbnails.canvasImage = { width: null, height: null };
        $.fn.fileuploader.defaults.captions = {
            button: function (options) { return 'Seleccionar ' + (options.limit == 1 ? '' : ''); },
            feedback: function (options) { return 'Seleccionar ' + (options.limit == 1 ? 'archivo' : 'archivos') + ' para cargar'; },
            feedback2: function (options) { return options.length + ' ' + (options.length > 1 ? ' archivos fueron seleccionados' : ' archivo fue seleccionado'); },
            confirm: 'Confirmar',
            cancel: 'Cancelar',
            name: 'Nombre',
            type: 'Tipo',
            size: 'Tamaño',
            dimensions: 'Dimensiones',
            duration: 'Duración',
            crop: 'Cultivo',
            rotate: 'Girar',
            download: 'Descargar',
            remove: 'Eliminar',
            drop: 'Suelta los archivos aquí para subir',
            paste: '<div class="fileuploader-pending-loader"><div class="left-half" style="animation-duration: ${ms}s"></div><div class="spinner" style="animation-duration: ${ms}s"></div><div class="right-half" style="animation-duration: ${ms}s"></div></div> Pegando un archivo, haga clic aquí para cancelar.',
            removeConfirmation: '¿Seguro que quieres eliminar este archivo?',
            errors: {
                filesLimit: 'Solamente ${limit} archivos se pueden cargar.',
                filesType: 'Solamente archivos de tipo: ${extensions}, se pueden cargar.',
                fileSize: '${name} ¡Es demasiado grande! Seleccione un archivo hasta ${fileMaxSize}MB.',
                filesSizeAll: '¡Los archivos seleccionados son demasiados grandes! Seleccione archivos hasta ${maxSize} MB.',
                fileName: 'El archivo con el nombre ${name} ya está seleccionado.',
                folderUpload: 'No tienes permitido subir carpetas.'
            }
        };
    }
    //Toastr
    if (typeof toastr != 'undefined') {
        toastr.options = {
            tapToDismiss: false,
            toastClass: 'toast',
            containerId: 'toast-container',
            debug: false,

            showMethod: 'fadeIn', //fadeIn, slideDown, and show are built into jQuery
            showDuration: 300,
            showEasing: 'swing', //swing and linear are built into jQuery
            onShown: function () {
                new Howl({
                    src: [g_app.home + 'assets/sounds/jGrowl_default.mp3?' + g_app._get],
                    loop: false
                }).play();
            },
            hideMethod: 'fadeOut',
            hideDuration: 1000,
            hideEasing: 'swing',
            onHidden: undefined,
            closeMethod: false,
            closeDuration: false,
            closeEasing: false,
            closeOnHover: true,
            closeButton: true,

            extendedTimeOut: 1000,
            iconClasses: {
                error: 'toast-error',
                info: 'toast-info',
                success: 'toast-success',
                warning: 'toast-warning'
            },
            iconClass: 'toast-info',
            positionClass: 'toast-top-right',
            timeOut: 5000, // Set timeOut and extendedTimeOut to 0 to make it sticky
            titleClass: 'toast-title',
            messageClass: 'toast-message',
            escapeHtml: false,
            target: 'body',
            closeHtml: '<button type="button">&times;</button>',
            closeClass: 'toast-close-button',
            newestOnTop: true,
            preventDuplicates: false,
            progressBar: true,
            progressClass: 'toast-progress',
            rtl: false,
            clipboard: null
        };
    }
    //Datepicker
    if (typeof $.fn.datepicker != 'undefined') {
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: "Hoy",
            clear: "Limpiar",
            format: "dd/mm/yyyy",
            titleFormat: "MM yyyy",
            weekStart: 0
        };
        $.fn.datepicker.defaults.autoclose = true;
        $.fn.datepicker.defaults.calendarWeeks = false;
        $.fn.datepicker.defaults.clearBtn = false;
        $.fn.datepicker.defaults.container = "body";
        $.fn.datepicker.defaults.disableTouchKeyboard = true;
        //$.fn.datepicker.defaults.endDate = "01/01/2019";
        $.fn.datepicker.defaults.format = "dd/mm/yyyy";
        $.fn.datepicker.defaults.immediateUpdates = false;
        $.fn.datepicker.defaults.language = "es";
        //$.fn.datepicker.defaults.startDate = "01/01/2019";
        $.fn.datepicker.defaults.templates = {
            leftArrow: '<i class="icofont icofont-curved-left"></i>',
            rightArrow: '<i class="icofont icofont-curved-right"></i>'
        };
        $.fn.datepicker.defaults.todayBtn = false;
        $.fn.datepicker.defaults.todayHighlight = true;
        //$.fn.datepicker.defaults.weekStart = 0;
    }
    /* --------------------------------------------------------
        Get Pcoded
        --------------------------------------------------------   */
    (function () {
        g_app.pcoded = $.extend({}, g_app.pcoded, JSON.parse('{"themelayout":"vertical","verticalMenuplacement":"left","verticalMenulayout":"wide","MenuTrigger":"click","SubMenuTrigger":"click","activeMenuClass":"active","ThemeBackgroundPattern":"pattern1","HeaderBackground":"themelight5","LHeaderBackground":"theme6","NavbarBackground":"theme1","ActiveItemBackground":"theme10","SubItemBackground":"theme2","ActiveItemStyle":"style0","ItemBorder":true,"ItemBorderStyle":"none","NavbarImage":"true","ActiveNavbarImage":"img3","SubItemBorder":true,"menutype":"st1","layouttype":"img","FixedNavbarPosition":true,"FixedHeaderPosition":true,"collapseVerticalLeftHeader":true,"VerticalNavigationView":"view1","verticalMenueffect":{"desktop":"shrink","tablet":"overlay","phone":"overlay"},"defaultVerticalMenu":{"desktop":"expanded","tablet":"offcanvas","phone":"offcanvas"},"onToggleVerticalMenu":{"desktop":"offcanvas","tablet":"expanded","phone":"expanded"}}'));
        BuildMenu();
    }());
    /* --------------------------------------------------------
        Build Menu
        --------------------------------------------------------   */
    function BuildMenu() {
        var AppMenu = g_app.$Menu;
        var InitialId = 0;
        //clear items app
        g_app.menu.items = new Array();
        AppMenu.empty();
        AppMenu.data('Id', InitialId);
        //get item
        lf_getItem(InitialId, lf_buildMenu);
        function lf_getItem(p_Id, p_callback) {
            $.ajax({
                data: JSON.stringify({ "ParentId": p_Id }),
                url: g_app.home + 'Home/GetSecurityObject',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: _success,
                complete: _complete
            });
            function _success(r) {
                //append items
                for (var i in r.data) {
                    g_app.menu.items.push({
                        "Id": r.data[i].Id,
                        "Description": r.data[i].Description,
                        "Icon": r.data[i].Icon,
                        "Color": r.data[i].Color,
                        "Controller": r.data[i].Controller,
                        "Function": r.data[i].Action,
                        "FatherId": p_Id,
                        "IsCompleted": r.data[i].Completed,
                        "IsDraw": false,
                        "IsPartialView": r.data[i].Completed,
                        "Menu": null,
                        "$": null
                    });
                }
            }
            function _complete(xhr, status) {
                var li_NextId = null;
                //update IsCompleted
                for (var i in g_app.menu.items) {
                    if (g_app.menu.items[i].Id == p_Id) {
                        g_app.menu.items[i].IsCompleted = true;
                        break;
                    }
                }
                //all completed ?
                for (var i in g_app.menu.items) {
                    if (!g_app.menu.items[i].IsCompleted) {
                        li_NextId = g_app.menu.items[i].Id;
                        break;
                    }
                }
                //callback
                if (li_NextId == null) {
                    if (typeof p_callback !== 'undefined') {
                        p_callback();
                    }
                } else {
                    lf_getItem(li_NextId, p_callback);
                }
            }
        }
        function lf_buildMenu() {
            var l_index = 0;
            _build(AppMenu);
            while (l_index < g_app.menu.items.length) {
                if (g_app.menu.items[l_index].Menu !== null) {
                    _build($(g_app.menu.items[l_index].Menu));
                }
                l_index++;
            }
            //append js
            $('body').append(`<script src="` + g_app.home + `assets/js/pcoded.js?v=` + g_app._get + `"></script>`);
            $('body').append(`<script src="` + g_app.home + `assets/js/vertical/vertical-layout.js?v=` + g_app._get + `"></script>`);
            $('body').append(`<script src="` + g_app.home + `assets/js/script.js?v=` + g_app._get + `"></script>`);
            //
            function _build(p_Father) {
                var _Id = parseInt(p_Father.data('Id'));
                //draw item
                for (var i in g_app.menu.items) {
                    if (g_app.menu.items[i].FatherId == _Id && !g_app.menu.items[i].IsDraw) {
                        //are initial fathers ?
                        if (_Id == InitialId) {
                            //are multilevel ?
                            if (g_app.menu.items[i].IsPartialView) {
                                p_Father.append(_getHtmlItem(g_app.menu.items[i]));
                            } else {
                                p_Father.append(_getHtmlTreeview(g_app.menu.items[i]));
                            }
                        } else {
                            //are multilevel ?
                            if (g_app.menu.items[i].IsPartialView) {
                                p_Father.append(_getHtmlSubItem(g_app.menu.items[i]));
                            } else {
                                p_Father.append(_getHtmlTreeview(g_app.menu.items[i]));
                            }
                        }
                        g_app.menu.items[i].IsDraw = true;
                    }
                }
            }
            function _getHtmlItem(item) {
                var AppLi = $('<li />'), AppLiA = $('<a />');
                //build menu>li>a
                AppLiA.attr('href', item.Controller + '/' + item.Function);
                AppLiA.append('<span class="pcoded-micon"><i class="' + item.Icon + '"></i><b>' + item.Description.charAt(0).toUpperCase() + '</b></span>');
                AppLiA.append('<span class="pcoded-mtext">' + item.Description + '</span>');
                AppLiA.append('<span class="pcoded-mcaret"></span>');
                //save $ in item
                item.$ = AppLiA;
                //build menu>li
                AppLi.append(AppLiA);
                //save item
                AppLi.data('Item', item);
                return AppLi;
            }
            function _getHtmlTreeview(item) {
                var AppLi = $('<li />'), AppLiA = $('<a />'), AppLiUl = $('<ul />');
                //build menu>li>a
                AppLiA.attr('href', 'javascript:void(0)');
                AppLiA.append('<span class="pcoded-micon"><i class="' + item.Icon + '"></i><b>' + item.Description.charAt(0).toUpperCase() + '</b></span>');
                AppLiA.append('<span class="pcoded-mtext">' + item.Description + '</span>');
                AppLiA.append('<span class="pcoded-mcaret"></span>');
                //save $ in item
                item.$ = AppLiA;
                //build menu>li>ul
                AppLiUl.addClass('pcoded-submenu');
                AppLiUl.data('Id', item.Id);
                //build menu>li
                AppLi.addClass('pcoded-hasmenu');
                AppLi.append(AppLiA);
                AppLi.append(AppLiUl);
                //save item
                AppLi.data('Item', item);
                //save content
                item.Menu = AppLiUl;
                return AppLi;
            }
            function _getHtmlSubItem(item) {
                var AppLi = $('<li />'), AppLiA = $('<a />');
                //build menu>li>a
                AppLiA.attr('href', item.Controller + '/' + item.Function);
                AppLiA.append('<span class="pcoded-micon"><i class="' + item.Icon + '"></i></span>');
                AppLiA.append('<span class="pcoded-mtext">' + item.Description + '</span>');
                AppLiA.append('<span class="pcoded-mcaret"></span>');
                //save $ in item
                item.$ = AppLiA;
                //build menu>li
                AppLi.append(AppLiA);
                //save item
                AppLi.data('Item', item);
                return AppLi;
            }
        }
    }
    /* --------------------------------------------------------
        Session
        --------------------------------------------------------   */
    (function () {
        window.onfocus = function (e) {
            if (e.target !== this) {
                return;
            }
            var l_status = false, l_session, l_window_session = sessionStorage.getItem("window-session");
            $.ajax({
                data: null,
                url: g_app.home + 'Account/GetStatus',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: function () { },
                success: _success,
                complete: _complete,
                error: function () { }
            });
            function _success(r) {
                l_status = r.status;
                l_session = r.session;
            }
            function _complete() {
                if (l_status) {
                    if (l_window_session !== l_session) {
                        window.location.reload(true);
                    }
                } else {
                    window.location = g_app.home + 'Login';
                }
            }
        };
    }());
    (function () {
        setInterval(function () {
            $.ajax({
                data: null,
                url: g_app.home + 'Account/KeepActive',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8"
            });
        }, 300000);//active session every 5 minutes
    }());
});