﻿
var g_app = new Object();
//properties
g_app.$Loading;
g_app.$Menu;
g_app.ObjectModule = new Object();
g_app.$Page;
g_app.$PageHeader;
g_app._get;
g_app.alertify;
g_app.api;
g_app.filter;
g_app.globals;
g_app.hash;
g_app.home = "/";
g_app.isMobile;
g_app.jgrowlClipboard;
g_app.menu = new Object();
g_app.menuSelected = new Object();
g_app.menuSettings;
g_app.pageHeader;
g_app.root;
g_app.pcoded;
g_app.soundPath;
//functions
g_app.fn_loadAppPage = function (p_url, p_name, p_parameters, p_callback, p_hash) {
    var AppPage = g_app.$Page;
    var ls_url = g_app.home + p_url;    
    var l_parameters = typeof p_parameters == 'undefined' ? new Object() : p_parameters == null ? new Object() : p_parameters;
    ShowLoading();
    //buil app page header
    //g_app.fn_buildAppPageHeader(ls_url, p_name, p_parameters, p_callback);
    //load


    $(".main-panel").removeClass("m-width-100");
    $(".main-panel>div").addClass("container");
    g_app.$Page.addClass("mt-md-3");
    g_app.$Page.removeClass("borde-container");
    g_app.$Page.removeClass("mg-bottom-30");


    if (!p_hash || g_app.hash == p_url) {
        AppPage.load(ls_url, l_parameters, function (response, status, xhr) {
            if (status == "error") {
                var msg = "Lo siento, pero hubo un error: ";
                AppPage.html("<div style='font-size: 2em; padding: 20px; margin: auto; width: max-content;'>"
                    + msg + xhr.status + " " + xhr.statusText + "</div>");
                HideLoading();
            } else {
                if (typeof p_callback != 'undefined' && p_callback != null) {
                    p_callback();
                }
            }
        });
    } else {
        var l_hash = "ajax/" + p_url + "?";
        g_app.globals.LoadParameters = l_parameters;
        g_app.globals.LoadCallback = p_callback;
        window.location.hash = l_hash;
    }
};
g_app.fn_loadAppPagePartial = function (p_url, p_name, p_parameters, p_callback, p_hash) {
    var AppPage = g_app.$Page;
    var ls_url = g_app.home + p_url;
    var l_parameters = typeof p_parameters == 'undefined' ? new Object() : p_parameters == null ? new Object() : p_parameters;
    if (!p_hash || g_app.hash == p_url) {
        var l_hash = "ajax/" + p_url + "?";
        g_app.globals.LoadParameters = l_parameters;
        g_app.globals.LoadCallback = p_callback;
        window.location.hash = l_hash;
        AppPage.load(ls_url, l_parameters, function (response, status, xhr) {
            if (status == "error") {
                var msg = "Lo siento, pero hubo un error: ";
                AppPage.html(msg + xhr.status + " " + xhr.statusText);
            } else {
                if (typeof p_callback != 'undefined' && p_callback != null) {
                    p_callback();
                }
            }
        });
    } else {
        var l_hash = "ajax/" + p_url + "?";
        g_app.globals.LoadParameters = l_parameters;
        g_app.globals.LoadCallback = p_callback;
        window.location.hash = l_hash;
    }
};
g_app.fn_getControllerAction = function (p_url) {
    var l_idx = 0, l_cf = p_url.split('/');
    while (l_idx < l_cf.length) {
        if (l_cf[l_idx].trim() == '') {
            l_cf.splice(l_idx, 1);
            l_idx = -1;
        }
        l_idx++;
    }
    return l_cf;
}
g_app.fn_getItemAppMenu = function (p_url) {
    var l_cf = g_app.fn_getControllerAction(p_url);
    if (l_cf.length == 2) {
        for (var i in g_app.menu.items) {
            if (g_app.menu.items[i].Controller == l_cf[0] && g_app.menu.items[i].Function == l_cf[1]) {
                return g_app.menu.items[i];
            }
        }
    }
    return null;
}
g_app.fn_buildAppPageHeader = function (p_url, p_description, p_parameters, p_callback) {
    var l_inAppMenu, l_idxAppPageHeader, l_icon, l_description = p_description;
    var AppPageHeader = g_app.$PageHeader;
    var l_cf = g_app.fn_getControllerAction(p_url);
    //remove home composite
    if (g_app.home != '/') {
        l_cf.splice(0, 1);
    }
    //remove empty
    if (l_cf.length == 2) {
        //is from $Menu ?
        l_inAppMenu = false;
        for (var i in g_app.menu.items) {
            if (g_app.menu.items[i].Controller == l_cf[0] && g_app.menu.items[i].Function == l_cf[1]) {
                l_description = g_app.menu.items[i].Description;
                l_inAppMenu = true;
                break;
            }
        }
        if (l_inAppMenu) {
            //restart header
            g_app.pageHeader = [];
            g_app.pageHeader.push({ Controller: l_cf[0], Description: l_description, Function: l_cf[1], Url: p_url, Parameters: p_parameters, Callback: p_callback });
        } else {
            //append or remove to header
            l_idxAppPageHeader = -1;
            for (var i in g_app.pageHeader) {
                if (g_app.pageHeader[i].Controller == l_cf[0] && g_app.pageHeader[i].Function == l_cf[1]) {
                    l_idxAppPageHeader = parseInt(i);
                    break;
                }
            }
            if (l_idxAppPageHeader != -1) {
                //remove
                g_app.pageHeader.splice((l_idxAppPageHeader + 1), (g_app.pageHeader.length - (l_idxAppPageHeader + 1)));
            } else {
                //append
                g_app.pageHeader.push({ Controller: l_cf[0], Description: l_description, Function: l_cf[1], Url: p_url, Parameters: p_parameters, Callback: p_callback });
            }
        }
        //print AppPageHeader
        AppPageHeader.empty();
        for (var i in g_app.pageHeader) {
            if (parseInt(i) == 0) {
                l_icon = '<i class="' + lf_getIconMenu(g_app.pageHeader[i].Controller, g_app.pageHeader[i].Function) + '"></i>';
            } else {
                l_icon = '';
            }
            if (parseInt(i) == g_app.pageHeader.length - 1) {
                //last item
                AppPageHeader.append('<li class="breadcrumb-item">' + l_icon + g_app.pageHeader[i].Description + '</li>');
            } else {
                AppPageHeader.append('<li class="breadcrumb-item"><a href="' + g_app.pageHeader[i].Controller + '/' + g_app.pageHeader[i].Function + '" data-i="' + i + '">' + l_icon + g_app.pageHeader[i].Description + '</a></li>');
            }
        }
    } else {
        console.error('?/controller/function/? => ' + p_url);
    }
    //fn
    function lf_getIconMenu(p_controller, p_function) {
        var l_return = "fa fa-close";
        for (var i in g_app.menu.items) {
            if (g_app.menu.items[i].Controller == p_controller && g_app.menu.items[i].Function == p_function) {
                l_return = g_app.menu.items[i].Icon;
                break;
            }
        }
        return l_return;
    }
};
g_app.fn_showLoading = function (p_settings) {
    g_app.$Loading.removeClass("md-show");
};
g_app.fn_hideLoading = function () {
    g_app.$Loading.addClass("md-show");
};
g_app.fn_getClone = (function () {
    // @Private
    var _toString = Object.prototype.toString;

    // @Private
    function _clone(from, dest, objectsCache) {
        var prop;
        // determina si @from es un valor primitivo o una funcion
        if (from == null || typeof from != "object") return from;
        // revisa si @from es un objeto ya guardado en cache
        if (_toString.call(from) == "[object Object]") {
            if (objectsCache.filter(function (item) {
                return item == from;
            }).length) return from;
            // guarda la referencia de los objetos creados
            objectsCache.push(from);
        }
        // determina si @from es una instancia de alguno de los siguientes constructores
        if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
            from.constructor == String || from.constructor == Number || from.constructor == Boolean) {
            return new from.constructor(from);
        }
        if (from.constructor != Object && from.constructor != Array) return from;
        // crea un nuevo objeto y recursivamente itera sus propiedades
        dest = dest || new from.constructor();
        for (prop in from) {
            // TODO: allow overwrite existing properties
            dest[prop] = (typeof dest[prop] == "undefined" ?
                _clone(from[prop], null, objectsCache) :
                dest[prop]);
        }
        return dest;
    }

    // función retornada en el closure
    return function (from, dest) {
        var objectsCache = [];
        return _clone(from, dest, objectsCache);
    };

}());
g_app.fn_isValue = function (p_data) {
    return (typeof p_data != 'undefined' && p_data != null && p_data.toString().trim() != '');
};
g_app.fn_getNameMonth = function (p_number, pb_shortName, ps_language) {
    var li_number, lb_shortName, ls_language;
    var l_lang_es = ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];
    var l_lang_es_short = ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'];
    li_number = typeof p_number == 'string' ? parseInt(p_number) : p_number;
    lb_shortName = typeof pb_shortName == 'undefined' ? false : pb_shortName == null ? false : pb_shortName;
    ls_language = typeof ps_language == 'undefined' ? 'es' : ps_language == null ? 'es' : ps_language;
    return lb_shortName ? l_lang_es_short[(li_number - 1)] : l_lang_es[(li_number - 1)];
};
g_app.fn_getCapitalize = function (ps_string) {
    var l_str = ps_string.toLowerCase();
    return l_str.charAt(0).toUpperCase() + l_str.slice(1);
};
g_app.fn_getLoremIpsum = function (p_paragraphs, p_letters) {
    var l_lorem = "lorem ipsum dolor sit amet consectetur adipiscing elit phasellus convallis tempor turpis eu accumsan ex vestibulum at curabitur semper nam sed tellus nunc aliquam velit augue vel lectus tempus leo sagittis malesuada in suspendisse nisl ac quisque quis enim pulvinar arcu vehicula urna vivamus purus condimentum diam lacinia suscipit praesent efficitur ligula blandit donec euismod orci ornare tristique sollicitudin scelerisque bibendum tortor libero pretium justo vitae morbi auctor non felis interdum fusce faucibus pellentesque ultrices class aptent taciti sociosqu ad litora torquent per conubia nostra inceptos himenaeos cras mattis congue mauris nec dui aenean odio varius porta metus cursus ut lobortis laoreet neque volutpat massa sem integer ante commodo viverra a elementum eget quam mi eros feugiat dictum pharetra risus proin sapien mollis nulla facilisi facilisis venenatis maecenas nisi imperdiet gravida erat est et magna lacus potenti dignissim placerat maximus vulputate posuere fermentum iaculis ullamcorper etiam id eleifend porttitor egestas fringilla rutrum rhoncus molestie luctus tincidunt duis nullam finibus consequat sodales ultricies hendrerit aliquet nibh habitant senectus netus fames hac habitasse platea dictumst dapibus natoque penatibus magnis dis parturient montes nascetur ridiculus mus primis cubilia curae";
    var l_list_token = l_lorem.split(" ");
    var l_letters = typeof p_letters == 'undefined' ? 10 : p_letters;
    var l_paragraphs = typeof p_paragraphs == 'undefined' ? 5 : p_paragraphs;
    var l_return = null;
    for (var i = 1; i <= l_paragraphs; i++) {
        if (l_return == null) {
            l_return = g_app.fn_getCapitalize(l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)]) + " ";
        } else {
            l_return += " " + g_app.fn_getCapitalize(l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)]) + " ";
        }
        for (var j = 1; j <= l_letters - 1; j++) {
            if (j == l_letters - 1) {
                l_return += l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)] + ".";
            } else {
                l_return += l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)] + " ";
            }
        }
    }
    return l_return.trim();
};
g_app.fn_presaveForm = function (p_f, p_a) {
    var g = new Object();
    g.analysis = typeof p_a != 'undefined' ? p_a : typeof p_f.attr('data-presave-analysis') == 'undefined' ? 'insert' : p_f.attr('data-presave-analysis');//[insert|update]
    g.hasChange = false;
    g.hasError = false;
    g.presaveObjects = p_f.find('[data-presave-object]');//data-presave-object='Description'
    g.objects = [];
    g.rule = typeof p_f.attr('data-presave-rule') == 'undefined' ? 'object' : p_f.attr('data-presave-rule');//[object|attribute]
    g.erros = {
        'integer': 'entero',
        'numeric': 'numérico',
        'noZero': 'diferente de cero',
        'positive': 'positivo',
        'negative': 'negativo',
        'max': 'menor o igual a',
        'min': 'mayor o igual a',
        'maxLength': 'de longitud menor o igual a',
        'minLength': 'de longitud mayor o igual a',
        'length': 'de longitud igual a',
        'isRequired': 'es campo requerido',
        'maxDecimals': 'de cantidad de decimales menor o igual a',
        'minDecimals': 'de cantidad de decimales mayor o igual a',
        'email': 'un correo electrónico correcto'
    };

    //initialize list
    lf_initializeList();
    //initialize validation
    lf_initializeValidation();
    //set global error
    lf_setGlobalError();
    //look changes
    lf_lookChanges();
    //set gloval change
    lf_setGlobalChange();

    function lf_initializeList() {
        var l_o;
        for (var i = 0; i < g.presaveObjects.length; i++) {
            l_o = {};
            switch (g.rule) {
                case 'attribute':
                    l_o.json = typeof $(g.presaveObjects[i]).attr('data-presave-json') == 'undefined' ? null : $(g.presaveObjects[i]).attr('data-presave-json');
                    l_o.list = typeof $(g.presaveObjects[i]).attr('data-presave-list') == 'undefined' ? null : $(g.presaveObjects[i]).attr('data-presave-list');
                    l_o.required = typeof $(g.presaveObjects[i]).attr('data-presave-required') == 'undefined' ? false : $(g.presaveObjects[i]).attr('data-presave-required');
                    break;
                case 'object':
                    l_o.json = typeof $(g.presaveObjects[i]).data('PresaveJson') == 'undefined' ? null : $(g.presaveObjects[i]).data('PresaveJson');
                    l_o.list = typeof $(g.presaveObjects[i]).data('PresaveList') == 'undefined' ? null : $(g.presaveObjects[i]).data('PresaveList');
                    l_o.required = typeof $(g.presaveObjects[i]).data('PresaveRequired') == 'undefined' ? false : $(g.presaveObjects[i]).data('PresaveRequired');
                    break;
            }
            l_o.value = typeof $(g.presaveObjects[i]).val() == 'undefined' ? "" : $(g.presaveObjects[i]).val() == null ? "" : $(g.presaveObjects[i]).val().toString().trim();
            l_o.$ = $(g.presaveObjects[i]);
            l_o.hasError = false;
            l_o.errorMessage = '';
            l_o.hasException = false;
            l_o.hasChange = false;
            l_o.requiredError = false;
            l_o.valueInitial = typeof $(g.presaveObjects[i]).data('PresaveInitial') == 'undefined' ? null : $(g.presaveObjects[i]).data('PresaveInitial').toString().trim();
            l_o.description = $(g.presaveObjects[i]).attr('data-presave-object') == '' ? null : $(g.presaveObjects[i]).attr('data-presave-object');
            l_o.isEmpty = l_o.value == "";

            g.objects.push(l_o);
        }
    }
    function lf_initializeValidation() {
        for (var i in g.objects) {
            lf_validateRequired(g.objects[i]);
            //validate
            if (!g.objects[i].requiredError && !g.objects[i].isEmpty) {
                //validate json, list
                try {
                    lf_validateJson(g.objects[i]);
                    lf_validateList(g.objects[i]);
                } catch (e) {
                    g.objects[i].hasError = true;
                    g.objects[i].errorMessage = e.message;
                    g.objects[i].hasException = true;
                }
            }
            if (g.objects[i].hasError && !g.objects[i].hasException && !g.objects[i].requiredError) {
                g.objects[i].errorMessage = g.objects[i].errorMessage.trim();
                g.objects[i].errorMessage = 'Debe ser: ' + g.objects[i].errorMessage.substring(0, (g.objects[i].errorMessage.length - 1));
            }
            //remove message error
            lf_removeError(g.objects[i]);
            //set message error
            lf_setError(g.objects[i]);
        }
    }
    function lf_setGlobalError() {
        for (var i in g.objects) {
            if (g.objects[i].hasError || g.objects[i].requiredError) {
                g.hasError = true;
                break;
            }
        }
    }
    function lf_lookChanges() {
        if (g.analysis != 'update') {
            return;
        }
        for (var i in g.objects) {
            switch (g.objects[i].$.prop('tagName')) {
                case 'INPUT':
                    switch (g.objects[i].$.attr('type')) {
                        case 'radio':
                        case 'checkbox':
                            if ((g.objects[i].$.is(":checked") ? '1' : '0') != g.objects[i].valueInitial) {
                                g.objects[i].hasChange = true;
                            }
                            break;
                        default:
                            if (g.objects[i].value != g.objects[i].valueInitial) {
                                g.objects[i].hasChange = true;
                            }
                            break;
                    }
                    break;
                default:
                    if (g.objects[i].value != g.objects[i].valueInitial) {
                        g.objects[i].hasChange = true;
                    }
                    break;
            }
        }
    }
    function lf_setGlobalChange() {
        for (var i in g.objects) {
            if (g.objects[i].hasChange) {
                g.hasChange = true;
                break;
            }
        }
    }
    function lf_setError(p_o) {
        if (!p_o.hasError && !p_o.requiredError) {
            return;
        }
        switch (p_o.$.prop('tagName')) {
            case 'SELECT':
                if (p_o.$.next().hasClass("chosen-container")) {
                    p_o.$.next('div.chosen-container').find('a.chosen-single').addClass('app-error');
                    lf_setError_message(p_o.$.next('div.chosen-container'), p_o.errorMessage);
                } else {
                    p_o.$.addClass('app-error');
                    lf_setError_message(p_o.$, p_o.errorMessage);
                }
                break;
            default:
                p_o.$.addClass('app-error');
                lf_setError_message(p_o.$, p_o.errorMessage);
                break;
        }
    }
    function lf_setError_message(p_$, ps_message) {
        p_$.after('<div class="app-error-message">' + ps_message + '</div>');
    }
    function lf_removeError(p_o) {
        switch (p_o.$.prop('tagName')) {
            case 'SELECT':
                if (p_o.$.next().hasClass("chosen-container")) {
                    p_o.$.next('div.chosen-container').find('a.chosen-single').removeClass('app-error');
                    lf_removeError_message(p_o.$.next('div.chosen-container'));
                } else {
                    p_o.$.removeClass('app-error');
                    lf_removeError_message(p_o.$);
                }
                break;
            default:
                p_o.$.removeClass('app-error');
                lf_removeError_message(p_o.$);
                break;
        }
    }
    function lf_removeError_message(p_$) {
        if (p_$.next('div.app-error-message').length != 0) {
            p_$.next('div.app-error-message').remove();
        }
    }
    //
    function lf_validateRequired(p_o) {
        if (p_o.required && p_o.isEmpty) {
            p_o.requiredError = true;
            p_o.errorMessage = (p_o.description == null ? '[[ debe ingresar un valor en data-presave-object ]]' : p_o.description) + ' ' + g.erros.isRequired;
        }
    }
    function lf_validateJson(p_o) {
        if (p_o.json == null) {
            return;
        }
        var l_json = JSON.parse(p_o.json);
        for (var i in l_json) {
            switch (i) {
                case 'max':
                    if (!lf_getValidation_max_n(p_o.value, parseInt(l_json[i]))) {
                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'min':
                    if (!lf_getValidation_min_n(p_o.value, parseInt(l_json[i]))) {
                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'minLength':
                    if (!lf_getValidation_minLength_n(p_o.value, parseInt(l_json[i]))) {
                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'maxLength':
                    if (!lf_getValidation_maxLength_n(p_o.value, parseInt(l_json[i]))) {
                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'length':
                    if (!lf_getValidation_length_n(p_o.value, parseInt(l_json[i]))) {
                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'maxDecimals':
                    if (!lf_getValidation_maxDecimals_n(p_o.value, parseInt(l_json[i]))) {
                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'minDecimals':
                    if (!lf_getValidation_minDecimals_n(p_o.value, parseInt(l_json[i]))) {
                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                        p_o.hasError = true;
                    }
                    break;
            }
        }
    }
    function lf_validateList(p_o) {
        if (p_o.list == null) {
            return;
        }
        var l_list = p_o.list.split(',');
        var l_key;
        for (var i = 0; i < l_list.length; i++) {
            l_key = l_list[i].trim();
            switch (l_key) {
                case 'numeric':
                    if (!lf_getValidation_numeric(p_o.value)) {
                        p_o.errorMessage += g.erros[l_key] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'integer':
                    if (!lf_getValidation_integer(p_o.value)) {
                        p_o.errorMessage += g.erros[l_key] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'noZero':
                    if (!lf_getValidation_noZero(p_o.value)) {
                        p_o.errorMessage += g.erros[l_key] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'positive':
                    if (!lf_getValidation_positive(p_o.value)) {
                        p_o.errorMessage += g.erros[l_key] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'negative':
                    if (!lf_getValidation_negative(p_o.value)) {
                        p_o.errorMessage += g.erros[l_key] + ', ';
                        p_o.hasError = true;
                    }
                    break;
                case 'email':
                    if (!lf_getValidation_email(p_o.value)) {
                        p_o.errorMessage += g.erros[l_key] + ', ';
                        p_o.hasError = true;
                    }
                    break;
            }
        }
    }
    //
    //'minDecimals': 'n'
    function lf_getValidation_minDecimals_n(p_value, n) {
        try {
            if (!lf_getValidation_numeric(p_value)) {
                return false;
            }
            var ls_value = p_value.toString().trim();
            var li_posDot = ls_value.indexOf('.');
            if (li_posDot == -1) {
                return true;
            }
            return ((ls_value.length - 1) - li_posDot) >= n;
        } catch (e) {
            return false;
        }
    }
    //'maxDecimals': 'n'
    function lf_getValidation_maxDecimals_n(p_value, n) {
        try {
            if (!lf_getValidation_numeric(p_value)) {
                return false;
            }
            var ls_value = p_value.toString().trim();
            var li_posDot = ls_value.indexOf('.');
            if (li_posDot == -1) {
                return true;
            }
            return ((ls_value.length - 1) - li_posDot) <= n;
        } catch (e) {
            return false;
        }
    }
    //'length': 'n'
    function lf_getValidation_length_n(p_value, n) {
        return p_value.trim().length == n;
    }
    //'maxLength': 'n'
    function lf_getValidation_maxLength_n(p_value, n) {
        return p_value.trim().length <= n;
    }
    //'minLength': 'n'
    function lf_getValidation_minLength_n(p_value, n) {
        return p_value.trim().length >= n;
    }
    //'min': 'n'
    function lf_getValidation_min_n(p_value, n) {
        try {
            return lf_getValidation_numeric(p_value) ? parseFloat(p_value) >= parseFloat(n) : false;
        } catch (e) {
            return false;
        }
    }
    //'max': 'n'
    function lf_getValidation_max_n(p_value, n) {
        try {
            return lf_getValidation_numeric(p_value) ? parseFloat(p_value) <= parseFloat(n) : false;
        } catch (e) {
            return false;
        }
    }
    //'negative'
    function lf_getValidation_negative(p_value) {
        try {
            return lf_getValidation_numeric(p_value) ? parseFloat(p_value) < 0 : false;
        } catch (e) {
            return false;
        }
    }
    //'positive'
    function lf_getValidation_positive(p_value) {
        return g_app.fn_isPositive(p_value);
    }
    //'noZero'
    function lf_getValidation_noZero(p_value) {
        return g_app.fn_noZero(p_value);
    }
    //'integer'
    function lf_getValidation_integer(p_value) {
        return g_app.fn_isInteger(p_value);
    }
    //'number'
    function lf_getValidation_numeric(p_value) {
        return g_app.fn_isNumeric(p_value);
    }
    // 'email'
    function lf_getValidation_email(p_value) {
        return g_app.fn_isEmail(p_value);
    }
    return g;
};
g_app.fn_isNumeric = function (p_data) {
    return p_data.toString().trim().length == 0 ? false : !isNaN(p_data);
}
g_app.fn_isInteger = function (p_data) {
    try {
        return g_app.fn_isNumeric(p_data) ? parseFloat(p_data) % 1 == 0 : false;
    } catch (e) {
        return false;
    }
}
g_app.fn_noZero = function (p_data) {
    try {
        return g_app.fn_isNumeric(p_data) ? parseFloat(p_data) != 0 : false;
    } catch (e) {
        return false;
    }
}
g_app.fn_isPositive = function (p_data, p_noZero) {
    try {
        return g_app.fn_isNumeric(p_data) ? typeof p_noZero == 'undefined' ? parseFloat(p_data) >= 0 : p_noZero ? parseFloat(p_data) > 0 : parseFloat(p_data) >= 0 : false;
    } catch (e) {
        return false;
    }
}
g_app.fn_isEmail = function (p_data) {
    try {
        return /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(p_data);
    } catch (e) {
        return false;
    }
}
g_app.fn_modifiedFileuploader = function (p_names, p_api) {
    var l_api = typeof p_api == 'undefined' ? g_app.api.fileuploader : p_api;
    var l_list = l_api.getFileList();
    var l_exists;
    if (l_list.length != p_names.length) {
        return true;
    }
    for (var i in l_list) {
        l_exists = false;
        for (var j in p_names) {
            if (l_list[i].name == p_names[j]) {
                l_exists = true;
                break;
            }
        }
        if (!l_exists) {
            return true;
        }
    }
    return false;
}
g_app.fn_getFirstDateCurrentMonth = function (p_date) {
    var date = typeof p_date == 'undefined' ? new Date() : p_date == null ? new Date() : p_date;
    return new Date(date.getFullYear(), date.getMonth(), 1);
}
g_app.fn_getLastDateCurrentMonth = function (p_date) {
    var date = typeof p_date == 'undefined' ? new Date() : p_date == null ? new Date() : p_date;
    return new Date(date.getFullYear(), date.getMonth() + 1, 0);
}
g_app.fn_fileValidation = function (p_settings) {
    var l_default = {
        fileInput: null,
        imgPreview: null,
        extensions: /(\.jpg|\.jpeg|\.png|\.bmp)$/i,
        showPreview: true,
        fnError: function () {
        }
    };
    l_default = $.extend({}, l_default, p_settings);
    var filePath = l_default.fileInput.val(), fileInput = l_default.fileInput[0], reader;
    if (filePath.trim() != '' && !l_default.extensions.exec(filePath)) {
        l_default.fileInput.val('');
        l_default.fnError();
        return false;
    }
    if (l_default.showPreview) {
        if (fileInput.files && fileInput.files[0]) {
            reader = new FileReader();
            reader.onload = function (e) {
                l_default.imgPreview.attr('src', e.target.result);
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
    return true;
}
g_app.fn_isYear = function (p_data, p_min, p_max) {
    var l_min = typeof p_min == 'undefined' ? 0 : p_min, l_max = typeof p_max == 'undefined' ? Number.POSITIVE_INFINITY : p_max, l_data = p_data;
    if (!g_app.fn_isInteger(l_data)) {
        return false;
    }
    l_data = parseInt(l_data);
    return l_data >= l_min && l_data <= l_max;
}
g_app.fn_inArrayJson = function (p_item, p_array, p_key) {
    for (var i in p_array) {
        if (p_array[i][p_key] == p_item) {
            return true;
        }
    }
    return false;
}
g_app.fn_roundNumber = function (p_number, p_scale) {
    if (!("" + p_number).includes("e")) {
        return +(Math.round(p_number + "e+" + p_scale) + "e-" + p_scale);
    } else {
        var arr = ("" + p_number).split("e");
        var sig = ""
        if (+arr[1] + p_scale > 0) {
            sig = "+";
        }
        return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + p_scale)) + "e-" + p_scale);
    }
}
g_app.fn_yRotate3D = function (p_settings) {
    var l_default = {
        element: null,
        theback: null,
        time: 10,
        step: 1
    };
    l_default = $.extend({}, l_default, p_settings);
    if (typeof l_default.element == 'undefined' || l_default.element == null
        || typeof l_default.theback == 'undefined' || l_default.theback == null) {
        console.error('propiedades "element" y "theback" son requeridos');
        return;
    }
    var l_inRotation = typeof l_default.element.attr('data-in-rotation') == 'undefined' ? false
        : l_default.element.attr('data-in-rotation') == null ? false
            : parseInt(l_default.element.attr('data-in-rotation')) == 1;
    l_inRotation = l_inRotation
        || (typeof l_default.theback.attr('data-in-rotation') == 'undefined' ? false
            : l_default.theback.attr('data-in-rotation') == null ? false
                : parseInt(l_default.theback.attr('data-in-rotation')) == 1);

    var l_interval, l_ny = 0;
    if (!l_inRotation) {
        l_default.element.attr('data-in-rotation', 1);
        l_interval = setInterval(function () {
            l_ny = l_ny + l_default.step;
            if (l_ny == l_default.step) {
                l_default.theback.hide();
                l_default.element.css({ "transform": "rotateY(" + l_ny + "deg)" });
                l_default.element.show();
            } else if (l_ny < 90) {
                l_default.element.css({ "transform": "rotateY(" + l_ny + "deg)" });
            } else if (l_ny == 90) {
                l_default.element.hide();
                l_default.theback.css({ "transform": "rotateY(" + (180 - l_ny) + "deg)" });
                l_default.theback.show();
            } else if (l_ny > 90) {
                l_default.theback.css({ "transform": "rotateY(" + (180 - l_ny) + "deg)" });
            }
            //limit 180
            if (l_ny == 180) {
                clearInterval(l_interval);
                l_default.element.attr('data-in-rotation', 0);
            }
        }, l_default.time);
    }
}
g_app.fn_getAmChartsExport = function (p_customItem) {
    var l_export = {
        "backgroundColor": "#fff",//RGB code of the color for the background of the exported image
        "enabled": true,//Enables or disables export functionality
        "divId": undefined,//ID or a reference to div object in case you want the menu in a separate container.
        "fabric": {},//Overwrites the default drawing settings (fabricJS library)
        "fallback": {},//Holds the messages to guide the user to copy the generated output; false will disable the fallback feature
        "fileName": "export",//A file name to use for generated export files (an extension will be appended to it based on the export format)
        "legend": {},//Places the legend in case it is within an external container
        "libs": undefined,//3rd party required library settings
        "menu": [{
            "class": "export-main",
            "menu": [{
                "label": "Descargar",
                "menu": ["PNG", "JPG"]
            }, {
                "label": "Anotar",
                "action": "draw",
                "menu": [{
                    "class": "export-drawing",
                    "menu": [{
                        label: "Tamaño...",
                        action: "draw.widths",
                        widths: [5, 20, 30]
                    }, {
                        "label": "Editar",
                        "menu": ["UNDO", "REDO", "CANCEL"]
                    }, {
                        "label": "Guardar",
                        "format": "PNG"
                    }]
                }]
            }]
        }],//A list of menu or submenu items (see the next chapter for details)
        "pdfMake": {},//Overwrites the default settings for PDF export (pdfMake library)
        "position": "top-right",//A position of export icon. Possible values: "top-left", "top-right" (default), "bottom-left", "bottom-right"
        "removeImages": false,//If true export checks for and removes "tainted" images that area lodead from different domains
        "delay": undefined,//General setting to delay the capturing of the chart
        "exportFields": [],//If set, only fields in this array will be exported ( data export only )
        "exportTitles": false,//Exchanges the data field names with it’s dedicated title ( data export only )
        "columnNames": {},//An object of key/value pairs to use as column names when exporting to data formats. exportTitles needs to be set for this to work as well.
        "exportSelection": false,//Exports the current data selection only ( data export only )
        "dataDateFormat": undefined,//Format to convert date strings to date objects, uses by default charts dataDateFormat ( data export only )
        "dateFormat": "YYYY-MM-DD",//Formats the category field in given date format ( data export only )
        "keyListener": false,//If true it observes the pressed keys to undo/redo the annotations
        "fileListener": false,//If true it observes the drag and drop feature and loads the dropped image file into the annotation
        "drawing": {},//Object which holds all possible settings for the annotation mode
        "overflow": true,//Flag to overwrite the css attribute ‘overflow’ of the chart container to avoid cropping the menu on small container
        "border": {},//An object of key/value pairs to define the overlaying border
        "processData": undefined,//A function which can be used to change the dataProvider when exporting to CSV, XLSX, or JSON
        "pageOrigin": true,//A flag to show / hide the origin of the generated PDF ( pdf export only )
        "forceRemoveImages": false//If true export removes all images
    };
    if (typeof p_customItem != 'undefined') {
        for (var i in p_customItem) {
            l_export.menu[0].menu.push(p_customItem[i]);
        }
    }
    return l_export;
}
g_app.fn_exportToXLSX = function (p_settings) {
    var l_default = {
        elementId: null,
        fileName: 'demo.xlsx',
        sheetName: 'Hoja1'
    };
    l_default = $.extend({}, l_default, p_settings);
    if (typeof l_default.elementId == 'undefined' || l_default.elementId == null) {
        console.error('propiedad "elementId" es requido');
        return;
    }
    var l_elt = document.getElementById(l_default.elementId);
    var l_wb = XLSX.utils.table_to_book(l_elt, { sheet: l_default.sheetName });
    return XLSX.writeFile(l_wb, l_default.fileName);
}
g_app.fn_getAmCharts = function (p_identifier) {
    if (typeof AmCharts == 'undefined') {
        return null;
    }
    var l_allCharts = AmCharts.charts, l_chart = null;
    try {
        for (var i in l_allCharts) {
            if (p_identifier == l_allCharts[i].div.id) {
                l_chart = l_allCharts[i];
                break;
            }
        }
        return l_chart;
    } catch (e) {
        return null;
    }
}
g_app.fn_destroyAmCharts = function (p_identifier) {
    var l_chart = g_app.fn_getAmCharts(p_identifier);
    if (l_chart == null) {
        return;
    }
    l_chart.clear();
    l_chart = null;
}
g_app.fn_buildIntroJS = function (p_steps, p_key) {
    if (typeof introJs == 'undefined') {
        return null;
    }
    var l_intro = introJs(), l_alert;
    l_intro.setOptions({
        steps: p_steps,
        nextLabel: '<b><i class="icofont icofont-double-right"></i></b>',
        prevLabel: '<b><i class="icofont icofont-double-left"></i></b>',
        skipLabel: '<b>Salir</b>',
        doneLabel: '<b>Hecho</b>',
        hidePrev: true,
        hideNext: true,
        //tooltipPosition: null,
        //tooltipClass: null,
        //highlightClass: null,
        exitOnEsc: true,
        exitOnOverlayClick: false,
        showStepNumbers: true,
        keyboardNavigation: true,
        showButtons: true,
        showBullets: true,
        showProgress: false,
        scrollToElement: true,
        //scrollTo: null,
        //scrollPadding: null,
        overlayOpacity: 0.5,
        disableInteraction: true
    });
    l_intro.start();
    //l_intro.goToStep(n).start();
    //l_intro.goToStepNumber(n).start();
    //l_intro.addStep({
    //    element: document.querySelectorAll('#step2')[0],
    //    intro: "Ok, wasn't that fun?",
    //    position: 'right'
    //});
    //l_intro.addSteps([{
    //    element: document.querySelectorAll('#step2')[0],
    //    intro: "Ok, wasn't that fun?",
    //    position: 'right'
    //}]);
    //l_intro.start().nextStep();
    //l_intro.goToStep(n).start().previousStep();
    //l_intro.exit();
    //l_intro.refresh();
    //l_intro.oncomplete(function () {
    //});
    //l_intro.onbeforeexit(function () {
    //    return false;//[true|false=>don't exit the intro]
    //});
    //l_intro.onchange(function (targetElement) {
    //});
    //l_intro.onbeforechange(function (targetElement) {
    //});
    //l_intro.onafterchange(function (targetElement) {
    //});
    l_intro.onexit(function () {
        l_alert = $.daAlert({
            ajax: null,
            buttons: [{
                type: 'accept',
                fn: function (p) {
                    g_app.globals[p_key] = false;
                    localStorage.setItem(p_key, false);
                    return true;
                },
                parameters: {}
            }, {
                type: 'cancel',
                fn: function (p) {
                    return true;
                },
                parameters: {}
            }],
            content: '¿No seguir mostrando la Presentación?',
            focus: 0,
            maximizable: false,
            movable: true,
            title: 'Confirmación',
            transition: 'zoom',
            maxWidth: '300px',
            modal: true
        });
    });
    return l_intro;
}
g_app.fn_getRandomColorHex = function () {
    var l_pos, l_hex, l_rcolor;
    l_hex = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"];
    l_rcolor = "#";
    for (var i = 0; i < 6; i++) {
        l_pos = fn_random(0, l_hex.length)
        l_rcolor += l_hex[l_pos];
    }
    function fn_random(p_from, p_to) {
        var l_a = Math.random() * (p_to - p_from);
        l_a = Math.floor(l_a);
        return parseInt(p_from) + l_a;
    }
    return l_rcolor;
}
g_app.fn_loadPartialView = function (p_content, p_url, p_data) {
    var ls_url = g_app.home + p_url;
    p_content.load(ls_url, p_data, function (response, status, xhr) {
        if (status == "error") {
            var msg = "Lo siento, pero hubo un error: ";
            p_content.html(msg + xhr.status + " " + xhr.statusText);
        }
    });
}
g_app.fn_getLightenDarkenColor = function (r, n) {
    var a = r, t = !1; "#" == a[0] && (a = a.slice(1), t = !0); var e = parseInt(a, 16), g = (e >> 16) + n; 255 < g ? g = 255 : g < 0 && (g = 0); var i = (e >> 8 & 255) + n; 255 < i ? i = 255 : i < 0 && (i = 0); var o = (255 & e) + n; return 255 < o ? o = 255 : o < 0 && (o = 0), (t ? "#" : "") + (o | i << 8 | g << 16).toString(16)
}
g_app.fn_fillNumber = function (p_num, p_len) {
    var l_len = typeof p_len == 'undefined' ? 2 : p_len;
    return g_app.fn_roundNumber(parseFloat(p_num), l_len).toFixed(l_len);
}
g_app.fn_replaceAll = function (str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
g_app.fn_getExtension = function (p_name) {
    return p_name.split('.').pop();
}
//g_app.$PageHeader.find("li:nth-last-child(2)").find("a").trigger("click"); ==> to back