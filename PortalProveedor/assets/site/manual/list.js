﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var Global = GetGlobalVar();

    /* Functions Globales */
    $("#add").on("click", function () {
        if (Global.ProfileId == Global.ProveedorProfileId) {
            return;
        }
        var div = $(`<div class="form-group-1">
                        <label>Archivo</label>
                        <input data-input="file" type="file" style="border: 1px solid silver;" />
                    </div>`);
        //comentario de prueba

        $("body").ModalAlertPlugin({
            "Type": "Save",
            "Color": "Info",
            "Content": div,
            "Save": function () {
                var Element = $(this)[0].Element[0];
                var File = $(Element).find("input[data-input=file]").prop("files")[0];
                
                if (File == undefined || File == null) {
                    return false;
                } else {
                    var p_formaData = new FormData();
                    p_formaData.append("File", File);

                    Function_SetManualMantenimiento(p_formaData, function (response) {
                        if (response.Value == "1000") {
                            $("body").ModalFastAlert({
                                "Color": "Success",
                                "Text": response.Message,
                                "Title": "Correcto!"
                            });
                            GetManualActivo();
                        } else {
                            $("body").ModalFastAlert({
                                "MasterColor": "#000000",
                                "Color": "Danger",
                                "Text": response.Message,
                                "Title": "Error"
                            });
                        }
                    });
                    return true;
                }
            }
        });         
    });

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this));
    });

    function GetManualActivo() {
        Function_GetManualActivoMantenimiento(function (response) {
            if (response.Response != null) {
                $("#download").attr("href", "../assets/files/" + response.Response.NombreArchivo);
            }
        });
    }

    /* Obtener Listado */
    function BuildDataTable() {
        return;
        Function_ListManualMantenimiento(function (response) {
            var table = $("#manual-datatable");
            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {
                var l_tr = $(`<tr>
                                <td>` + object.NombreArchivo + `</td>
                                <td>` + object.FechaCreacion + `</td>
                            </tr>`);
                
                table.find("tbody").append(l_tr);
            });
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");
        });
    }

    (function () {
        /* Inicializar DataTablet */
        //BuildDataTable();
        GetManualActivo();
        //HideLoading();
    })();
});