﻿"use strict";

function List_ParametrosMantenimiento(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Parametros/List_ParametrosMantenimiento',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function SetParametrosMantenimiento(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Parametros/SetParametrosMantenimiento',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ExportToExcel(p_btn) {
    $.ajax({
        data: null,
        url: 'Parametros/ExportToExcel',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: function () { }
    });
    function _beforeSend() {
        p_btn.addClass('disabled');
        p_btn.prop('disabled', true);
    }
    function _success(r) {
        if (parseInt(r.status) == 1) {
            var tag_a = document.createElement('a');
            tag_a.href = 'Parametros/DownloadExcel?FileName=' + r.response.name + "&DownloadName=Parametros.xlsx";
            tag_a.download = 'myExport.xlsx';
            tag_a.click();
            tag_a = null;
        } else {
            $.jGrowl(r.response, {
                header: 'Error',
                type: 'error',
                clipboard: r.exception
            });
        }
    }
    function _complete(xhr, status) {
        p_btn.removeClass('disabled');
        p_btn.prop('disabled', false);
    }
    function _error(xhr, status) {
    }
}
