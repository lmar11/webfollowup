﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var Global = GetGlobalVar();

    /* Functions Globales */
    $("#buscar").on("click", function () {
        BuildDataTable();
    });

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this));
    });

    /* Obtener Listado */
    function BuildDataTable() {
        List_ParametrosMantenimiento(function (response) {
            var table = $("#parametros-datatable");
            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {                
                var l_tr = $(`<tr>
                                <td>` + object.Clave + `</td>
                                <td>` + object.Datos + `</td>
                                <td style="white-space: unset !important;">` + object.Descripcion + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-light-blue" data-button="edit"><i class="fa fa-edit"></i></button>
                                    </div>
                                </td>
                            </tr>`);
                
                l_tr.find("button[data-button=edit]").on("click", function () {
                    var div = $(`<div class="form-group-1">
                                        <label>Parametros - Datos</label>
                                        <input data-input="datos" style="border: 1px solid silver;" />
                                    </div>`);
                    div.find("input[data-input=datos]").val(object.Datos);
                    $("body").ModalAlertPlugin({
                        "Type": "Save",
                        "Color": "Warning",
                        "Content": div,
                        "Save": function () {
                            var Element = $(this)[0].Element[0];
                            var Datos = $(Element).find("input[data-input=datos]").val();
                            if (Datos == "") {
                                return false;
                            } else {
                                var Parametros =
                                {
                                    Clave: object.Clave,
                                    Datos: Datos
                                };
                                SetParametrosMantenimiento(Parametros, function (response) {
                                    if (response.Value == "1000") {
                                        $("body").ModalFastAlert({
                                            "Color": "Success",
                                            "Text": response.Message,
                                            "Title": "Correcto!"
                                        });
                                        BuildDataTable();
                                    } else {
                                        $("body").ModalFastAlert({
                                            "MasterColor": "#000000",
                                            "Color": "Danger",
                                            "Text": response.Message,
                                            "Title": "Error"
                                        });
                                    }
                                });
                                return true;
                            }
                        }
                    }); 
                });
                
                table.find("tbody").append(l_tr);
            });
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");
        });
    }

    (function () {
        BuildDataTable();
    })();
});