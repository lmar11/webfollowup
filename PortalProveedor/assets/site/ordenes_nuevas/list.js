﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var EntryModel =
    {
        CodiFana: 0,
        Proceso: null,
        CodigoSapProveedor: null
    };
    var EntryOrdenCompra =
    {
        CodiFana: 0,
        Proceso: 0,
        Tipo: null,
        Consulta: null
    };

    var Global = GetGlobalVar();

    /* Functions Globales */
    function DateToFormatDate(date) {
        var Split = date.split('/');
        var FinalDate = Split[2] + "-" + Split[1] + "-" + Split[0];
        return FinalDate;
    }

    /* Funcion de Carga Página */
    var CountFunctionsReady = 0;
    function ValidateLoadPageAsync() {
        if (CountFunctionsReady == 1) {
            HideLoading();
        }
    }

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this), EntryOrdenCompra);
    });

    /* Obtener Listado de Ordenes */
    $("#buscar").on("click", function () {
        var Analista = $("#analista").val() == undefined ? 0 : $("#analista").val();
        var Proceso = $("#proceso").val() == undefined ? 0 : $("#proceso").val();
        var Tipo = $("#tipo").val();
        var Consulta = $("#consulta").val();

        EntryOrdenCompra.CodiFana = Analista;
        EntryOrdenCompra.Proceso = Proceso;
        EntryOrdenCompra.Tipo = Tipo;
        EntryOrdenCompra.Consulta = Consulta;

        CountFunctionsReady = 0;
        BuildDataTable();
        Search();
    });

    function BuildDataTable() {
        var l_td;
        Function_ListOrdenesCompra(EntryOrdenCompra, function (response) {
            var table = $("#ordenes-compra-datatable");
            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {
                var Color = "";
                switch (object.Estatus) {
                    case "PD":
                        Color = "bg-btn-info";
                        break;
                    case "PA":
                        Color = "bg-btn-disabled";
                        break;
                    case "PR":
                        Color = "bg-btn-danger";
                        break;
                }

                if (Global.ProfileId != Global.ProveedorProfileId) {
                    if (object.FlagDescargado == "S") {
                        Color = "bg-btn-disabled";
                    } else {
                        Color = "bg-btn-info";
                    }
                    var l_tr = $(`<tr>
                                <td><button class="button-estatus ` + Color + `">
                                        <i class="fa fa-cubes"></i></button></td>
                                <td title="` + object.Proveedor + `">` + (object.Proveedor.length > 7 ? object.Proveedor.substring(0, 7) : object.Proveedor) + `</td>
                                <td>` + object.DocumentoCompra + `</td>
                                <td>` + object.ImporteTotal + `</td>
                                <td>` + object.TipoMoneda + `</td>
                                <td>` + object.FechaDocumento + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-light-orange" data-button="ver">Ver Documento</button>
                                        <button class="bg-btn-light-violete" data-button="download">Descargar</button>
                                    </div>
                                </td>
                            </tr>`);

                    l_tr.find("button[data-button=ver]").on("click", function () {
                        BuildDetailDataTableVerDocumento(l_tr, object);
                    });
                    l_tr.find("button[data-button=download]").on("click", function () {
                        var ExistePdf = Function_ExistUrl({ "Url": object.Url_Pdf });

                        if (ExistePdf) {
                            var tag_a = document.createElement('a');
                            tag_a.href = object.Url_Pdf;
                            tag_a.download = 'Orden#' + object.DocumentoCompra + '.pdf';
                            tag_a.target = '_blank';
                            tag_a.click();
                            tag_a = null;
                        } else {
                            $("body").ModalFastAlert({
                                "MasterColor": "#000000",
                                "Color": "Danger",
                                "Text": "No existe Pdf para descargar.",
                                "Title": "Aviso!"
                            });
                        }
                    });
                    table.find("tbody").append(l_tr);
                } else {
                    var l_tr = $(`<tr>
                                <td><button class="button-estatus ` + Color + `">
                                        <i class="fa fa-cubes"></i></button></td>
                                <td title="` + object.Analista + `">` + (object.Analista.length > 7 ? object.Analista.substring(0, 7) : object.Analista) + `</td>
                                <td>` + object.DocumentoCompra + `</td>
                                <td>` + object.ImporteTotal + `</td>
                                <td>` + object.TipoMoneda + `</td>
                                <td>` + object.FechaDocumento + (object.FlagNotificado == "S" ? `<button class="m-1 button-estatus bg-btn-danger">
                                        <i class="fa fa-cubes"></i></button>` : ``) + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-light-blue" data-button="aceptar">Aceptar</button>
                                        <button class="bg-btn-light-orange" data-button="observar">Observar</button>
                                    </div>
                                </td>
                            </tr>`);

                    table.find("tbody").append(l_tr);

                    if (Global.RolId != 1) {
                        return;
                    }

                    l_tr.find("button.button-estatus").on("click", function () {
                        if (object.Estatus == "PD") {
                            Function_InsertLogOrdenesCompra({ "CodiOrco": object.CodiOrco, "Proceso": "DescargaPdf" },
                                function (response) {
                                    if (response.Value == "1000") {
                                        var ExistePdf = Function_ExistUrl({ "Url": object.Url_Pdf });

                                        if (ExistePdf) {
                                            var tag_a = document.createElement('a');
                                            tag_a.href = object.Url_Pdf;
                                            tag_a.download = 'Orden#' + object.DocumentoCompra + '.pdf';
                                            tag_a.target = '_blank';
                                            tag_a.click();
                                            tag_a = null;
                                        } else {
                                            $("body").ModalFastAlert({
                                                "MasterColor": "#000000",
                                                "Color": "Danger",
                                                "Text": "No existe Pdf para descargar.",
                                                "Title": "Aviso!"
                                            });
                                        }

                                        BuildDataTable();
                                    } else {
                                        $("body").ModalFastAlert({
                                            "MasterColor": "#000000",
                                            "Color": "Danger",
                                            "Text": Response.Message,
                                            "Title": "Error"
                                        });
                                    }
                                });
                        } else if (object.Estatus == "PA") {
                            BuildDetailDataTableVerDocumentoDescarga(l_tr, object);
                        }
                    });
                    //l_tr.find("button[data-button=aceptar]").on("click", function () {
                    //    if (object.Estatus == "PA") {
                    //        BuildDetailDataTableAceptar(l_tr, object);
                    //    }
                    //});
                    l_tr.find("button[data-button=aceptar]").on("click", function () {
                        if (object.Estatus == "PA") {
                            $("body").ModalAlert({
                                "Type": "Confirm",
                                "Color": "Info",
                                "Text": "¿Esta seguro que desea guardar?",
                                "Confirm": function () {
                                    Function_SetAcceptOrdenNueva({ "CodiOrco": object.CodiOrco }, function (response) {
                                        if (response.Value == "1000") {
                                            $("body").ModalFastAlert({
                                                "Color": "Success",
                                                "Text": response.Message,
                                                "Title": "Correcto!"
                                            });
                                            $("#buscar").trigger("click");
                                        } else {
                                            $("body").ModalFastAlert({
                                                "MasterColor": "#000000",
                                                "Color": "Danger",
                                                "Text": response.Message,
                                                "Title": "Error"
                                            });
                                        }
                                    });

                                    return true;
                                }
                            });
                        }
                    });
                    l_tr.find("button[data-button=observar]").on("click", function () {
                        if (object.Estatus == "PA") {
                            Function_ListOrdenesCompraItem({ "CodiOrco": object.CodiOrco }, function (response) {
                                Function_ListMotivo(function (resp) {
                                    BuildDatailDataTableObservar(l_tr, object, response.Response, resp.Response);
                                });
                            });
                        }
                    });
                }
            });
            $.fn.dataTable.moment = function (format, locale) {
                var types = $.fn.dataTable.ext.type;

                // Add type detection
                types.detect.unshift(function (d) {
                    return moment(d, format, locale, true).isValid() ?
                        'moment-' + format :
                        null;
                });

                // Add sorting method - use an integer for the sorting
                types.order['moment-' + format + '-pre'] = function (d) {
                    return moment(d, format, locale, true).unix();
                };
            };
            $.fn.dataTable.moment('DD/MM/YYYY');
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[5, "asc"]],
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");
            CountFunctionsReady++;
            ValidateLoadPageAsync();
        });
        function BuildDetailDataTableAceptar(l_tr, object) {
            if (l_td != null && l_td != undefined) {
                l_td.remove();
            }

            var Response = Function_ExistUrl({ "Url": object.Url_Pdf });
            if (Response) {
                l_td = $(`<td colspan="12">
                                    <div class="row mt-3 mb-3">
                                        <div class="col-7">
                                            <span style="font-size: 16px; font-weight: 800;">
                                                Orden Compra #` + object.DocumentoCompra + `</span>
                                        </div>
                                        <div class="col-5">
                                            <div class="acciones-datatable-1" style="justify-content: flex-end;">
                                                <button class="bg-btn-danger" data-button="back">Atras</button>
                                                <button class="bg-btn-light-blue" data-button="ok">Aceptar</button>                                                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <embed src="data:application/pdf;base64,` + Function_GetBase64(object.Url_Pdf) + `" type="application/pdf" width="100%" height="400px" />
                                        </div>
                                    </div>
                                </td>`);
            } else {
                l_td = $(`<td colspan="12">
                                    <div class="row mt-3 mb-3">
                                        <div class="col-7">
                                            <span style="font-size: 16px; font-weight: 800;">
                                                Orden Compra #` + object.DocumentoCompra + `</span>
                                        </div>
                                        <div class="col-5">
                                            <div class="acciones-datatable-1" style="justify-content: flex-end;">
                                                <button class="bg-btn-danger" data-button="back">Atras</button>
                                                <button class="bg-btn-light-blue" data-button="ok">Aceptar</button>                                                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <label style='padding-bottom: 20px; display: block; text-align: center;'>
                                                No se encontró el Pdf del Documento</label>
                                        </div>
                                    </div>
                                </td>`);
            }

            l_td.insertAfter(l_tr);
            l_td.find("button[data-button=back]").on("click", function () {
                l_td.remove();
            });
            l_td.find("button[data-button=ok]").on("click", function () {
                $("body").ModalAlert({
                    "Type": "Confirm",
                    "Color": "Info",
                    "Text": "¿Esta seguro que desea guardar?",
                    "Confirm": function () {
                        Function_SetAcceptOrdenNueva({ "CodiOrco": object.CodiOrco }, function (response) {
                            if (response.Value == "1000") {
                                $("body").ModalFastAlert({
                                    "Color": "Success",
                                    "Text": response.Message,
                                    "Title": "Correcto!"
                                });
                                $("#buscar").trigger("click");
                            } else {
                                $("body").ModalFastAlert({
                                    "MasterColor": "#000000",
                                    "Color": "Danger",
                                    "Text": response.Message,
                                    "Title": "Error"
                                });
                            }
                        });

                        return true;
                    }
                });
            });

            return l_td;
        }
        function BuildDatailDataTableObservar(l_tr, object, items, motivos) {
            if (l_td != null && l_td != undefined) {
                l_td.remove();
            }
            l_td = $(`<td colspan="12" style="background: #f2f2f2;">
                    <div class="row mt-3 mb-3">
                        <div class="col-7">
                            <span style="font-size: 13px; font-weight: 800;">
                                Motivo Observación</span>
                        </div>
                        <div class="col-5">
                            <div class="acciones-datatable-1" style="justify-content: flex-end;">
                                <button class="bg-btn-danger" data-button="back">Atras</button>
                                <button class="bg-btn-light-violete" data-button="see">Ver Documento</button>
                                <button class="bg-btn-light-blue" data-button="save">Guardar</button>                                                    
                            </div>
                        </div>
                    </div>
                    <button data-button="addMotivo" class="btn-alone bg-btn-light-orange m-3" style="float: right;">
                        Agregar Motivo</button>
                </td>`);

            l_td.insertAfter(l_tr);

            l_td.find("button[data-button=back]").on("click", function () {
                l_td.remove();
            });
            l_td.find("button[data-button=see]").on("click", function () {
                BuildDetailDataTableVerDocumento(l_tr, object);
            });
            l_td.find("button[data-button=save]").on("click", function () {
                var flagError = false;
                var ArrayObservacion = new Array();
                $.each(l_td.find(".motivo"), function (idx, obj) {
                    var CodiOcrm = $(obj).find("select[data-select=item]").val();
                    var CodiMrec = $(obj).find("select[data-select=motivo]").val();
                    var Comentario = $(obj).find("textarea[data-input=comentario]").val();
                    var Cambio = "";

                    switch (CodiMrec) {
                        case "1":
                            var Value = $(obj).find("input[data-input=cambio-precio]").val();
                            if (parseFloat(Value) != 0 && !isNaN(parseFloat(Value)) && parseFloat(Value) != null) {
                                Cambio = parseFloat(Value).toString();
                            }
                            break;
                        case "2":
                            var Value = $(obj).find("select[data-select=cambio-moneda]").val();
                            Cambio = Value;
                            break;
                        case "3":
                            var Value = $(obj).find("input[data-input=cambio-fecha]").val();
                            if (Value != "") {
                                Cambio = Value;
                            }
                            break;
                        case "4":
                            var Value = $(obj).find("input[data-input=cambio-especificacion-tecnica]").val();
                            if (Value != "") {
                                Cambio = Value;
                            }
                            break;
                        case "5":
                            var Value = $(obj).find("input[data-input=cambio-lote-minimo]").val();
                            if (Value != "") {
                                Cambio = Value;
                            }
                            break;
                        case "6":
                            var Value = $(obj).find("input[data-input=cambio-otros]").val();
                            if (Value != "") {
                                Cambio = Value;
                            }
                            break;
                    }

                    if (Cambio == "") {
                        $("body").ModalFastAlert({
                            "MasterColor": "#000000",
                            "Color": "Danger",
                            "Text": "Cambio incorrecto o vacío!",
                            "Title": "Error"
                        });
                        flagError = true;
                        return;
                    } else if (Comentario == "") {
                        $("body").ModalFastAlert({
                            "MasterColor": "#000000",
                            "Color": "Danger",
                            "Text": "Comentario vacío!",
                            "Title": "Error"
                        });
                        flagError = true;
                        return;
                    }

                    var ObservacionModel =
                    {
                        CodiOrcm: CodiOcrm,
                        CodiMrec: CodiMrec,
                        Comentario: Comentario,
                        Cambio: Cambio
                    };
                    ArrayObservacion.push(ObservacionModel);
                });

                if (flagError) {
                    return;
                }

                var EntryObservaciones =
                {
                    ObservacionMotivo: ArrayObservacion
                };

                $("body").ModalAlert({
                    "Type": "Confirm",
                    "Color": "Info",
                    "Text": "¿Esta seguro que desea guardar?",
                    "Confirm": function () {
                        Function_InsertObservaciones(EntryObservaciones, function (response) {
                            if (response.Value == "1000") {
                                $("body").ModalFastAlert({
                                    "Color": "Success",
                                    "Text": response.Message,
                                    "Title": "Correcto!"
                                });
                                $("#buscar").trigger("click");
                            } else {
                                $("body").ModalFastAlert({
                                    "MasterColor": "#000000",
                                    "Color": "Danger",
                                    "Text": response.Message,
                                    "Title": "Error"
                                });
                            }
                        });
                        return true;
                    }
                });
            });
            l_td.find("button[data-button=addMotivo]").on("click", function () {
                var m = $(`<div class="motivo" style="background: white;padding: 20px;border-radius: 10px;
                        margin-bottom: 10px; position: relative;">
                        <div class="close-btn close-motivo">
                            <i class="fa fa-times"></i></div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group-1">
                                    <label>Ítem</label>
                                    <select data-select="item"></select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group-1">
                                    <label>Cambio</label>
                                    <input data-input="cambio-precio" type="text" class="motivo-cambio d-block-none" />
                                    <select data-select="cambio-moneda" class="motivo-cambio d-block-none">
                                        <option value="1" selected>PEN</option>
                                        <option value="2">USD</option>
                                        <option value="3">EUR</option>
                                    </select>
                                    <input data-input="cambio-fecha" type="text" class="motivo-cambio d-block-none" />
                                    <input data-input="cambio-especificacion-tecnica" type="text" class="motivo-cambio d-block-none" />
                                    <input data-input="cambio-lote-minimo" type="text" class="motivo-cambio d-block-none" />
                                    <input data-input="cambio-otros" type="text" class="motivo-cambio d-block-none" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group-1">
                                    <label>Motivo de Observación</label>
                                    <select data-select="motivo"></select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group-1">
                                    <label>Comentario</label>
                                    <textarea data-input="comentario" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>`);

                jQueryMoneda(m.find("input[data-input=cambio-precio]"));
                m.find("input[data-input=cambio-fecha]").datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                jQueryLetterNumeric(m.find("input[data-input=cambio-especificacion-tecnica]"));
                jQueryLetterNumeric(m.find("input[data-input=cambio-lote-minimo]"));
                jQueryLetterNumeric(m.find("input[data-input=cambio-otros]"));
                m.find("select[data-select=motivo]").on("change", function () {
                    var ValueMotivo = m.find("select[data-select=motivo]").val();
                    switch (ValueMotivo) {
                        case "1":
                            m.find(".motivo-cambio").addClass("d-block-none");
                            m.find("input[data-input=cambio-precio]").removeClass("d-block-none");
                            break;
                        case "2":
                            m.find(".motivo-cambio").addClass("d-block-none");
                            m.find("select[data-select=cambio-moneda]").removeClass("d-block-none");
                            break;
                        case "3":
                            m.find(".motivo-cambio").addClass("d-block-none");
                            m.find("input[data-input=cambio-fecha]").removeClass("d-block-none");
                            break;
                        case "4":
                            m.find(".motivo-cambio").addClass("d-block-none");
                            m.find("input[data-input=cambio-especificacion-tecnica]").removeClass("d-block-none");
                            break;
                        case "5":
                            m.find(".motivo-cambio").addClass("d-block-none");
                            m.find("input[data-input=cambio-lote-minimo]").removeClass("d-block-none");
                            break;
                        case "6":
                            m.find(".motivo-cambio").addClass("d-block-none");
                            m.find("input[data-input=cambio-otros]").removeClass("d-block-none");
                            break;
                    }
                });
                m.find("select[data-select=item]").select2();

                $.each(items, function (idx, obj) {
                    var opt = $("<option value='" + obj.CodiOrcm + "'>" + obj.Posicion + " - " + obj.Descripcion + "</option>");
                    m.find("select[data-select=item]").append(opt);
                });
                $.each(motivos, function (idx, obj) {
                    var opt = $("<option value='" + obj.CodiMrec + "'>" + obj.Descripcion + "</option>");
                    m.find("select[data-select=motivo]").append(opt);
                });

                m.find("select[data-select=motivo]").val(1).trigger("change");
                m.find(".close-motivo").on("click", "i", function () {
                    m.remove();
                });

                m.insertBefore($(this));
            });

            l_td.find("button[data-button=addMotivo]").trigger("click");

            return l_td;
        }
        function BuildDetailDataTableVerDocumento(l_tr, object) {
            if (l_td != null && l_td != undefined) {
                l_td.remove();
            }

            var Response = Function_ExistUrl({ "Url": object.Url_Pdf });

            if (Response) {
                l_td = $(`<td colspan="12">
                                    <div class="row mt-3 mb-3">
                                        <div class="col-7">
                                            <span style="font-size: 16px; font-weight: 800;">
                                                Orden Compra #` + object.DocumentoCompra + `</span>
                                        </div>
                                        <div class="col-5">
                                            <div class="acciones-datatable-1" style="justify-content: flex-end;">
                                                <button class="bg-btn-danger" data-button="back">Atras</button>                                                   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <embed src="data:application/pdf;base64,` + Function_GetBase64(object.Url_Pdf) + `" type="application/pdf" width="100%" height="400px" />
                                        </div>
                                    </div>
                                </td>`);
            } else {
                l_td = $(`<td colspan="12">
                                    <div class="row mt-3 mb-3">
                                        <div class="col-7">
                                            <span style="font-size: 16px; font-weight: 800;">
                                                Orden Compra #` + object.DocumentoCompra + `</span>
                                        </div>
                                        <div class="col-5">
                                            <div class="acciones-datatable-1" style="justify-content: flex-end;">
                                                <button class="bg-btn-danger" data-button="back">Atras</button>                                                   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <label style='padding-bottom: 20px; display: block; text-align: center;'>
                                                No se encontró el Pdf del Documento</label>
                                        </div>
                                    </div>
                                </td>`);
            }

            l_td.insertAfter(l_tr);
            l_td.find("button[data-button=back]").on("click", function () {
                l_td.remove();
            });
        }
        function BuildDetailDataTableVerDocumentoDescarga(l_tr, object) {
            if (l_td != null && l_td != undefined) {
                l_td.remove();
            }
            var Response = Function_ExistUrl({ "Url": object.Url_Pdf });

            if (Response) {
                l_td = $(`<td colspan="12">
                                    <div class="row mt-3 mb-3">
                                        <div class="col-7">
                                            <span style="font-size: 16px; font-weight: 800;">
                                                Orden Compra #` + object.DocumentoCompra + `</span>
                                        </div>
                                        <div class="col-5">
                                            <div class="acciones-datatable-1" style="justify-content: flex-end;">
                                                <button class="bg-btn-light-violete" data-button="download">Descargar</button>                                                   
                                                <button class="bg-btn-danger" data-button="back">Atras</button>                                                   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <embed src="data:application/pdf;base64,` + Function_GetBase64(object.Url_Pdf) + `" type="application/pdf" width="100%" height="400px" />
                                        </div>
                                    </div>
                                </td>`);
            } else {
                l_td = $(`<td colspan="12">
                                    <div class="row mt-3 mb-3">
                                        <div class="col-7">
                                            <span style="font-size: 16px; font-weight: 800;">
                                                Orden Compra #` + object.DocumentoCompra + `</span>
                                        </div>
                                        <div class="col-5">
                                            <div class="acciones-datatable-1" style="justify-content: flex-end;">
                                                <button class="bg-btn-light-violete" data-button="download">Descargar</button>                                                   
                                                <button class="bg-btn-danger" data-button="back">Atras</button>                                                   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <label style='padding-bottom: 20px; display: block; text-align: center;'>
                                                No se encontró el Pdf del Documento</label>
                                        </div>
                                    </div>
                                </td>`);
            }

            l_td.insertAfter(l_tr);
            l_td.find("button[data-button=back]").on("click", function () {
                l_td.remove();
            });
            l_td.find("button[data-button=download]").on("click", function () {
                var ExistePdf = Function_ExistUrl({ "Url": object.Url_Pdf });

                if (ExistePdf) {
                    var tag_a = document.createElement('a');
                    tag_a.href = object.Url_Pdf;
                    tag_a.download = 'Orden#' + object.DocumentoCompra + '.pdf';
                    tag_a.target = '_blank';
                    tag_a.click();
                    tag_a = null;
                } else {
                    $("body").ModalFastAlert({
                        "MasterColor": "#000000",
                        "Color": "Danger",
                        "Text": "No existe Pdf para descargar.",
                        "Title": "Aviso!"
                    });
                }               
            });
        }
    }

    /* Crear Graficos */
    function Search() {
        EntryModel.CodiFana = $("#analista").val() == undefined ? 0 : $("#analista").val();
        EntryModel.Proceso = $("#proceso").val() == undefined ? "-" : $("#proceso").val();

        /* Grafico Ordenes Vigentes */
        Function_GetGraficoOrdenesVigentes(EntryModel, function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vigente", am4charts.XYChart);

                chart.data = Data;

                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 },
                    { timeUnit: "month", count: 2 },
                    { timeUnit: "month", count: 4 },
                    { timeUnit: "month", count: 6 },
                    { timeUnit: "month", count: 12 },
                    { timeUnit: "month", count: 24 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}";

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $("#grafico-vigente").closest(".card"));

        /* Grafico Ordenes Vencidas */
        Function_GetGraficoOrdenesVencidas(EntryModel, function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vencido", am4charts.XYChart);

                chart.data = Data;
                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                chart.colors.list = [
                    am4core.color("red"),
                    am4core.color("#FF6F91"),
                    am4core.color("#FF9671"),
                    am4core.color("#FFC75F"),
                    am4core.color("#F9F871")
                ];

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 },
                    { timeUnit: "month", count: 2 },
                    { timeUnit: "month", count: 4 },
                    { timeUnit: "month", count: 6 },
                    { timeUnit: "month", count: 12 },
                    { timeUnit: "month", count: 24 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}";

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $("#grafico-vencido").closest(".card"));
    }

    (function () {
        /* Inicializar Filtros para el Analista */
        if (Global.ProfileId != Global.ProveedorProfileId) {
            Function_ListAnalista(function (response) {
                var selectAnalista = $("#analista");
                $.each(response.Response.Analista, function (index, object) {
                    var option = $("<option value='" + object.CodiFana + "'>" + object.Nombre + "</option>");
                    selectAnalista.append(option);
                });
                selectAnalista.append("<option value='0'>Todos</option>");
                selectAnalista.val(Global.AnalistaId).trigger("click");

                Function_ListProceso(function (response) {
                    var selectProceso = $("#proceso");
                    $.each(response.Response, function (index, object) {
                        var option = $("<option value='" + object.CodiProc + "'>" + object.Descripcion + "</option>");
                        selectProceso.append(option);
                    });
                    selectProceso.append("<option value='0'>Todos</option>");
                    $(selectProceso.find("option")[0]).trigger("click");
                    //Search();                    
                    $("#buscar").trigger("click");
                });
            });
        } else {
            /* Empezar la pagina con la Busqueda de las opciones */
            //Search();
            $("#buscar").trigger("click");
        }

    })();
});