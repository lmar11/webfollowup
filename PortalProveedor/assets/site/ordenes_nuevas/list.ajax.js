﻿"use strict";
function Function_GetGraficoOrdenesVigentes(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesNuevas/GetDataGraficoOrdenesVigentes',
        type: 'post',
        dataType: 'json',
        //async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_GetGraficoOrdenesVencidas(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesNuevas/GetDataGraficoOrdenesVencidas',
        type: 'post',
        dataType: 'json',
        //async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ListOrdenesCompra(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesNuevas/List_OrdenesCompra',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_ListOrdenesCompraItem(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesNuevas/List_OrdenCompraItem',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ListAnalista(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Helpers/List_Analista',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_ListProceso(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Helpers/List_Proceso',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_ListMotivo(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/OrdenesNuevas/List_TipoMotivo',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ExistUrl(p_Parameters) {
    var l_Response = false;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Helpers/Exist_Url',
        type: 'post',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r.Response;
        } else {
            //$("body").ModalFastAlert({
            //    "MasterColor": "#000000",
            //    "Color": "Danger",
            //    "Text": r.Exception,
            //    "Title": "Error"
            //});
            l_Response = r.Response;
        }
    }
    function _complete(xhr, status) {
        //if (typeof f_Callback != 'undefined') {
        //    f_Callback(l_Response);
        //    HideLoading();
        //} else {
            HideLoading();
        //}
    }
    function _error(xhr, status) { }
    return l_Response;
}
function Function_GetBase64(Url) {
    var l_response = null;
    var p_param = {
        Ruta: Url
    };
    $.ajax({
        data: JSON.stringify(p_param),
        url: '/Helpers/GetFile',
        type: 'post',
        dataType: 'html',
        async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
    }
    function _success(r) {
        l_response = r;
    }
    function _complete(xhr, status) {
    }
    function _error(xhr, status) {
        console.log("Error");
    }
    return l_response;
}

function Function_InsertLogOrdenesCompra(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesNuevas/Insert_LogOrdenCompra',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_InsertObservaciones(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesNuevas/Insert_Observaciones',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_SetAcceptOrdenNueva(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesNuevas/SetAcceptOrdenesNuevas',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ExportToExcel(p_btn, p_param) {
    $.ajax({
        data: JSON.stringify(p_param),
        url: 'OrdenesNuevas/ExportToExcel',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: function () { }
    });
    function _beforeSend() {
        p_btn.addClass('disabled');
        p_btn.prop('disabled', true);
    }
    function _success(r) {
        if (parseInt(r.status) == 1) {
            var tag_a = document.createElement('a');
            tag_a.href = 'OrdenesNuevas/DownloadExcel?FileName=' + r.response.name + "&DownloadName=OrdenesNuevas.xlsx";
            tag_a.download = 'myExport.xlsx';
            tag_a.click();
            tag_a = null;
        } else {
            $.jGrowl(r.response, {
                header: 'Error',
                type: 'error',
                clipboard: r.exception
            });
        }
    }
    function _complete(xhr, status) {
        p_btn.removeClass('disabled');
        p_btn.prop('disabled', false);
    }
    function _error(xhr, status) {
    }
}