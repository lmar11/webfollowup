﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var EntryModel =
    {
        CodiFana: 0,
        Proceso: null,
        CodigoSapProveedor: null
    };
    
    var Global = GetGlobalVar();
    
    /* Functions Globales */
    function DateToFormatDate(date) {
        var Split = date.split('/');
        var FinalDate = Split[2] + "-" + Split[1] + "-" + Split[0];
        return FinalDate;
    }

    /* Enviar Detalle del Problema */
    $("#send").on("click", function () {
        var MainDetail = $("#main-detail").val();
        var AddedDetail = $("#added-detail").val();

        if (MainDetail == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Detalle Principal no puede ser vacío.",
                "Title": "Error"
            });
            return;
        }

        $("body").ModalAlert({
            "Type": "Confirm",
            "Color": "Info",
            "Text": "¿Esta seguro que desea enviar?",
            "Confirm": function () {
                Function_SendDetail({ "DetallePrincipal": MainDetail, "DetalleAdicional": AddedDetail }, function (response) {
                    if (response.Value == "1000") {
                        $("body").ModalFastAlert({
                            "Color": "Success",
                            "Text": response.Message,
                            "Title": "Correcto!"
                        });

                        $("#main-detail").val("");
                        $("#added-detail").val("");
                    } else {
                        $("body").ModalFastAlert({
                            "MasterColor": "#000000",
                            "Color": "Danger",
                            "Text": response.Message,
                            "Title": "Error"
                        });
                    }
                });

                return true;
            }
        }); 
    });

    /* Crear Graficos */
    function Search() {
        /* Grafico Ordenes Vigentes */
        Function_GetGraficoOrdenesVigentes(function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vigente", am4charts.XYChart);

                chart.data = Data;

                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}"; 
                
                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
        });

        /* Grafico Ordenes Vencidas */
        Function_GetGraficoOrdenesVencidas(function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vencido", am4charts.XYChart);

                chart.data = Data;
                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                chart.colors.list = [
                    am4core.color("red"),
                    am4core.color("#FF6F91"),
                    am4core.color("#FF9671"),
                    am4core.color("#FFC75F"),
                    am4core.color("#F9F871")
                ];

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}";                                

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
        });
    }

    (function () {
        HideLoading();
    })();
});