﻿"use strict";
function Function_GetGraficoOrdenesVigentes(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/CentroSoporte/GetDataGraficoOrdenesVigentes',
        type: 'post',
        dataType: 'json',
        //async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_GetGraficoOrdenesVencidas(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/CentroSoporte/GetDataGraficoOrdenesVencidas',
        type: 'post',
        dataType: 'json',
        //async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_SendDetail(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/CentroSoporte/SendDetail',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}
