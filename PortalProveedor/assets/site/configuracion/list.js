﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var EntryModel =
    {
        CodiFana: 0,
        Proceso: null,
        CodigoSapProveedor: null
    };
    var ModelParametro =
    {
        Ruc: null,
        RazonSocial: null,
        Analista: null,
        Password: null,
        Solicitante: null,
        ContactoFacturacion: null,
        ContactoGerencia: null,
        ContactoTecnico: null
    };
    var ModelParametroSolicitante =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ModelParametroContactoGerencia =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ModelParametroContactoFacturacion =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ModelParametroContactoTecnico =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ResponseModel = null;
    
    var Global = GetGlobalVar();
    
    /* Functions Globales */
    function DateToFormatDate(date) {
        var Split = date.split('/');
        var FinalDate = Split[2] + "-" + Split[1] + "-" + Split[0];
        return FinalDate;
    }
        
    /* Crear Graficos */
    function Search() {
        /* Grafico Ordenes Vigentes */
        Function_GetGraficoOrdenesVigentes(function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vigente", am4charts.XYChart);

                chart.data = Data;

                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}"; 
                
                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
        });

        /* Grafico Ordenes Vencidas */
        Function_GetGraficoOrdenesVencidas(function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vencido", am4charts.XYChart);

                chart.data = Data;
                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                chart.colors.list = [
                    am4core.color("red"),
                    am4core.color("#FF6F91"),
                    am4core.color("#FF9671"),
                    am4core.color("#FFC75F"),
                    am4core.color("#F9F871")
                ];

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}";                                

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
        });
    }

    /* Cargar Datos */
    function GetDatosProveedor() {
        Function_GetDatosProveedor(function (response) {
            ChangeResponseProveedorFormat(response.Response);
            //ResponseModel = response.Response;
            $("#ruc-login").val(ResponseModel.Ruc);
            $("#razon-social-login").val(ResponseModel.RazonSocial);

            $("#nombre-login").val(ResponseModel.Solicitante.Nombre);
            $("#ap-paterno-login").val(ResponseModel.Solicitante.ApellidoPaterno);
            $("#ap-materno-login").val(ResponseModel.Solicitante.ApellidoMaterno);
            $("#cargo-login").val(ResponseModel.Solicitante.Cargo);
            $("#correo-login").val(ResponseModel.Solicitante.Correo);
            $("#celular-login").val(ResponseModel.Solicitante.Celular);
            $("#password-1-login").val(ResponseModel.Password);
            $("#password-2-login").val(ResponseModel.Password);

            $("#nombre-gerencia-login").val(ResponseModel.ContactoGerencia.Nombre);
            $("#ap-paterno-gerencia-login").val(ResponseModel.ContactoGerencia.ApellidoPaterno);
            $("#ap-materno-gerencia-login").val(ResponseModel.ContactoGerencia.ApellidoMaterno);
            $("#cargo-gerencia-login").val(ResponseModel.ContactoGerencia.Cargo);
            $("#correo-gerencia-login").val(ResponseModel.ContactoGerencia.Correo);
            $("#celular-gerencia-login").val(ResponseModel.ContactoGerencia.Celular);

            $("#nombre-facturacion-login").val(ResponseModel.ContactoFacturacion.Nombre);
            $("#ap-paterno-facturacion-login").val(ResponseModel.ContactoFacturacion.ApellidoPaterno);
            $("#ap-materno-facturacion-login").val(ResponseModel.ContactoFacturacion.ApellidoMaterno);
            $("#cargo-facturacion-login").val(ResponseModel.ContactoFacturacion.Cargo);
            $("#correo-facturacion-login").val(ResponseModel.ContactoFacturacion.Correo);
            $("#celular-facturacion-login").val(ResponseModel.ContactoFacturacion.Celular);

            $("#nombre-tecnico-login").val(ResponseModel.ContactoTecnico.Nombre);
            $("#ap-paterno-tecnico-login").val(ResponseModel.ContactoTecnico.ApellidoPaterno);
            $("#ap-materno-tecnico-login").val(ResponseModel.ContactoTecnico.ApellidoMaterno);
            $("#cargo-tecnico-login").val(ResponseModel.ContactoTecnico.Cargo);
            $("#correo-tecnico-login").val(ResponseModel.ContactoTecnico.Correo);
            $("#celular-tecnico-login").val(ResponseModel.ContactoTecnico.Celular);

            $("#nombre-login").trigger("keyup");
            $("#nombre-facturacion-login").trigger("keyup");
            $("#nombre-tecnico-login").trigger("keyup");
        });
    }
    function ChangeResponseProveedorFormat(Response) {
        ResponseModel = {
            Ruc: Response.Ruc,
            RazonSocial: Response.Proveedor,
            Password: Response.Password,
            Solicitante: null,
            ContactoFacturacion: null,
            ContactoGerencia: null,
            ContactoTecnico: null
        };
        ResponseModel.Solicitante = {
            Nombre: Response.Nombre,
            ApellidoPaterno: Response.ApellidoPaterno,
            ApellidoMaterno: Response.ApellidoMaterno,
            Cargo: Response.Cargo,
            Correo: Response.Correo,
            Celular: Response.Celular
        };
        $.each(Response.Contactos, function (index, object) {
            if (object.Tipo == "GV") {
                ResponseModel.ContactoGerencia = {
                    Nombre: object.Nombre,
                    ApellidoPaterno: object.ApellidoPaterno,
                    ApellidoMaterno: object.ApellidoMaterno ,
                    Cargo: object.Cargo,
                    Correo: object.Correo,
                    Celular: object.Celular
                };
            } else if (object.Tipo == "CF") {
                ResponseModel.ContactoFacturacion = {
                    Nombre: object.Nombre,
                    ApellidoPaterno: object.ApellidoPaterno,
                    ApellidoMaterno: object.ApellidoMaterno,
                    Cargo: object.Cargo,
                    Correo: object.Correo,
                    Celular: object.Celular
                };
            } else if (object.Tipo == "CT") {
                ResponseModel.ContactoTecnico = {
                    Nombre: object.Nombre,
                    ApellidoPaterno: object.ApellidoPaterno,
                    ApellidoMaterno: object.ApellidoMaterno,
                    Cargo: object.Cargo,
                    Correo: object.Correo,
                    Celular: object.Celular
                };
            }
        });
    }

    /* Solicitante */
    $("#next-page-1").on("click", function () {
        $(".crear-cuenta-2").removeClass("d-block-none");
        $(".crear-cuenta-1").addClass("d-block-none");
    });
    $("#ruc-login, #razon-social-login, #nombre-login, #ap-paterno-login, #ap-materno-login," +
        "#cargo-login, #correo-login, #celular-login, #password-1-login, #password-2-login").on("keyup", function () {

            var ruc = $("#ruc-login").val();
            var razon_social = $("#razon-social-login").val();
            var nombre = $("#nombre-login").val();
            var ap_paterno = $("#ap-paterno-login").val();
            var ap_materno = $("#ap-materno-login").val();
            var cargo = $("#cargo-login").val();
            var correo = $("#correo-login").val();
            var celular = $("#celular-login").val();
            var password_1 = $("#password-1-login").val();
            var password_2 = $("#password-2-login").val();

            if (ruc.length != 11) {
                PassRuc = false;
                $("#razon-social-login").val("");
            }

            if (ruc == "" || razon_social == "" || nombre == "" || ap_paterno == "" || ap_materno == "" ||
                cargo == "" || correo == "" || celular == "" || password_1 == "" || password_2 == "") {
                ModelParametro.Solicitante = null;
                $("#save-changes-1").prop("disabled", true);
                return;
            }

            ModelParametro.Solicitante = ModelParametroSolicitante;

            ModelParametro.Ruc = ruc;
            ModelParametro.RazonSocial = razon_social;
            ModelParametro.Password = password_1;

            ModelParametro.Solicitante.Nombre = nombre;
            ModelParametro.Solicitante.ApellidoPaterno = ap_paterno;
            ModelParametro.Solicitante.ApellidoMaterno = ap_materno;
            ModelParametro.Solicitante.Cargo = cargo;
            ModelParametro.Solicitante.Correo = correo;
            ModelParametro.Solicitante.Celular = celular;

            $("#save-changes-1").prop("disabled", false);            
        });
    
    /* Contacto Gerencia y Facturacion */
    $("#preview-page-2").on("click", function () {
        $(".crear-cuenta-1").removeClass("d-block-none");
        $(".crear-cuenta-2").addClass("d-block-none");
    });
    $("#next-page-2").on("click", function () {
        $(".crear-cuenta-3").removeClass("d-block-none");
        $(".crear-cuenta-2").addClass("d-block-none");
    });
    $("#nombre-gerencia-login, #ap-paterno-gerencia-login, #ap-materno-gerencia-login, #cargo-gerencia-login," +
        "#correo-gerencia-login, #celular-gerencia-login, #nombre-facturacion-login, #ap-paterno-facturacion-login," +
        "#ap-materno-facturacion-login, #cargo-facturacion-login, #correo-facturacion-login," +
        "#celular-facturacion-login").on("keyup", function () {
            var nombre_gerencia = $("#nombre-gerencia-login").val();
            var ap_paterno_gerencia = $("#ap-paterno-gerencia-login").val();
            var ap_materno_gerencia = $("#ap-materno-gerencia-login").val();
            var cargo_gerencia = $("#cargo-gerencia-login").val();
            var correo_gerencia = $("#correo-gerencia-login").val();
            var celular_gerencia = $("#celular-gerencia-login").val();
            var nombre_facturacion = $("#nombre-facturacion-login").val();
            var ap_paterno_facturacion = $("#ap-paterno-facturacion-login").val();
            var ap_materno_facturacion = $("#ap-materno-facturacion-login").val();
            var cargo_facturacion = $("#cargo-facturacion-login").val();
            var correo_facturacion = $("#correo-facturacion-login").val();
            var celular_facturacion = $("#celular-facturacion-login").val();

            if (nombre_gerencia == "" || ap_paterno_gerencia == "" || ap_materno_gerencia == "" || cargo_gerencia == "" ||
                correo_gerencia == "" || celular_gerencia == "" || nombre_facturacion == "" || ap_paterno_facturacion == "" ||
                ap_materno_facturacion == "" || cargo_facturacion == "" || correo_facturacion == "" ||
                celular_facturacion == "") {
                ModelParametro.ContactoFacturacion = null;
                ModelParametro.ContactoGerencia = null;
                $("#save-changes-2").prop("disabled", true);
                return;
            }

            ModelParametro.ContactoGerencia = ModelParametroContactoGerencia;
            ModelParametro.ContactoFacturacion = ModelParametroContactoFacturacion;

            ModelParametro.ContactoGerencia.Nombre = nombre_gerencia;
            ModelParametro.ContactoGerencia.ApellidoPaterno = ap_paterno_gerencia;
            ModelParametro.ContactoGerencia.ApellidoMaterno = ap_materno_gerencia;
            ModelParametro.ContactoGerencia.Cargo = cargo_gerencia;
            ModelParametro.ContactoGerencia.Correo = correo_gerencia;
            ModelParametro.ContactoGerencia.Celular = celular_gerencia;

            ModelParametro.ContactoFacturacion.Nombre = nombre_facturacion;
            ModelParametro.ContactoFacturacion.ApellidoPaterno = ap_paterno_facturacion;
            ModelParametro.ContactoFacturacion.ApellidoMaterno = ap_materno_facturacion;
            ModelParametro.ContactoFacturacion.Cargo = cargo_facturacion;
            ModelParametro.ContactoFacturacion.Correo = correo_facturacion;
            ModelParametro.ContactoFacturacion.Celular = celular_facturacion;

            $("#save-changes-2").prop("disabled", false);
        });
    
    /* Contacto Tecnico */
    $("#preview-page-3").on("click", function () {
        $(".crear-cuenta-2").removeClass("d-block-none");
        $(".crear-cuenta-3").addClass("d-block-none");
    });
    $("#nombre-tecnico-login, #ap-paterno-tecnico-login, #ap-materno-tecnico-login, #cargo-tecnico-login," +
        "#correo-tecnico-login, #celular-tecnico-login").on("keyup", function () {
            var nombre_tecnico = $("#nombre-tecnico-login").val();
            var ap_paterno_tecnico = $("#ap-paterno-tecnico-login").val();
            var ap_materno_tecnico = $("#ap-materno-tecnico-login").val();
            var cargo_tecnico = $("#cargo-tecnico-login").val();
            var correo_tecnico = $("#correo-tecnico-login").val();
            var celular_tecnico = $("#celular-tecnico-login").val();

            if (nombre_tecnico == "" || ap_paterno_tecnico == "" || ap_materno_tecnico == "" || cargo_tecnico == "" ||
                correo_tecnico == "" || celular_tecnico == "") {
                ModelParametro.ContactoTecnico = null;
                $("#save-changes-3").prop("disabled", true);
                return;
            }

            ModelParametro.ContactoTecnico = ModelParametroContactoTecnico;

            ModelParametro.ContactoTecnico.Nombre = nombre_tecnico;
            ModelParametro.ContactoTecnico.ApellidoPaterno = ap_paterno_tecnico;
            ModelParametro.ContactoTecnico.ApellidoMaterno = ap_materno_tecnico;
            ModelParametro.ContactoTecnico.Cargo = cargo_tecnico;
            ModelParametro.ContactoTecnico.Correo = correo_tecnico;
            ModelParametro.ContactoTecnico.Celular = celular_tecnico;

            $("#save-changes-3").prop("disabled", false);
        });

    /* Save Changes */
    $("#save-changes-1, #save-changes-2, #save-changes-3").on("click", function () {
        SolicitanteEmailValidation();
    });
    function SignUpValidation() {
        /* Validacion General */
        if (ModelParametro.Password == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar campos",
                "Title": "Error"
            });
            return false;
        }

        /* Validacion Solicitante */
        if (ModelParametro.Solicitante.Nombre == "" ||
            ModelParametro.Solicitante.ApellidoPaterno == "" ||
            ModelParametro.Solicitante.ApellidoMaterno == "" ||
            ModelParametro.Solicitante.Cargo == "" ||
            ModelParametro.Solicitante.Correo == "" ||
            ModelParametro.Solicitante.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Solicitante.",
                "Title": "Error"
            });
            return false;
        }
        var password_1 = $("#password-1-login").val();
        var password_2 = $("#password-2-login").val();
        if (ModelParametro.Solicitante == null) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos.",
                "Title": "Error"
            });
            return false;
        }
        if (!ModelParametro.Solicitante.Correo.includes("@") || !ModelParametro.Solicitante.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return false;
        }
        if (ModelParametro.Solicitante.Celular.length < 9) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El celular no tiene los digitos necesarios.",
                "Title": "Error"
            });
            return false;
        }
        if (password_1 != password_2) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Las contraseñas no coinciden.",
                "Title": "Error"
            });
            return false;
        }        

        /* Validacion Contacto Gerencia */
        if (ModelParametro.ContactoGerencia.Nombre == "" ||
            ModelParametro.ContactoGerencia.ApellidoPaterno == "" ||
            ModelParametro.ContactoGerencia.ApellidoMaterno == "" ||
            ModelParametro.ContactoGerencia.Cargo == "" ||
            ModelParametro.ContactoGerencia.Correo == "" ||
            ModelParametro.ContactoGerencia.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Contacto de Gerencia.",
                "Title": "Error"
            });
            return false;
        }
        if (!ModelParametro.ContactoGerencia.Correo.includes("@") || !ModelParametro.ContactoGerencia.Correo.includes(".") ||
            !ModelParametro.ContactoFacturacion.Correo.includes("@") || !ModelParametro.ContactoFacturacion.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return false;
        }
        if (ModelParametro.ContactoGerencia.Celular.length < 9 || ModelParametro.ContactoFacturacion.Celular.length < 9) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El celular no tiene los digitos necesarios.",
                "Title": "Error"
            });
            return false;
        }

        /* Validacion Contacto Facturacion */
        if (ModelParametro.ContactoFacturacion.Nombre == "" ||
            ModelParametro.ContactoFacturacion.ApellidoPaterno == "" ||
            ModelParametro.ContactoFacturacion.ApellidoMaterno == "" ||
            ModelParametro.ContactoFacturacion.Cargo == "" ||
            ModelParametro.ContactoFacturacion.Correo == "" ||
            ModelParametro.ContactoFacturacion.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Contacto de Facturación.",
                "Title": "Error"
            });
            return false;
        }

        /* Validacion Contacto Tecnico */
        if (ModelParametro.ContactoTecnico.Nombre == "" ||
            ModelParametro.ContactoTecnico.ApellidoPaterno == "" ||
            ModelParametro.ContactoTecnico.ApellidoMaterno == "" ||
            ModelParametro.ContactoTecnico.Cargo == "" ||
            ModelParametro.ContactoTecnico.Correo == "" ||
            ModelParametro.ContactoTecnico.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Contacto Técnico.",
                "Title": "Error"
            });
            return false;
        }
        if (!ModelParametro.ContactoTecnico.Correo.includes("@") || !ModelParametro.ContactoTecnico.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return false;
        }
        if (ModelParametro.ContactoTecnico.Celular.length < 9) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El celular no tiene los digitos necesarios.",
                "Title": "Error"
            });
            return false;
        }

        return true;
    }
    function SolicitanteEmailValidation() {
        if (ModelParametro.Solicitante.Correo != ResponseModel.Solicitante.Correo) {
            Function_ValidateEmail({ "Correo": ModelParametro.Solicitante.Correo }, function (response) {
                if (response.Value == "1000") {
                    if (SignUpValidation()) {
                        ResponseModel.Password = ModelParametro.Password;

                        ResponseModel.Solicitante.Nombre = ModelParametro.Solicitante.Nombre;
                        ResponseModel.Solicitante.ApellidoPaterno = ModelParametro.Solicitante.ApellidoPaterno;
                        ResponseModel.Solicitante.ApellidoMaterno = ModelParametro.Solicitante.ApellidoMaterno;
                        ResponseModel.Solicitante.Cargo = ModelParametro.Solicitante.Cargo;
                        ResponseModel.Solicitante.Correo = ModelParametro.Solicitante.Correo;
                        ResponseModel.Solicitante.Celular = ModelParametro.Solicitante.Celular;

                        ResponseModel.ContactoGerencia.Nombre = ModelParametro.ContactoGerencia.Nombre;
                        ResponseModel.ContactoGerencia.ApellidoPaterno = ModelParametro.ContactoGerencia.ApellidoPaterno;
                        ResponseModel.ContactoGerencia.ApellidoMaterno = ModelParametro.ContactoGerencia.ApellidoMaterno;
                        ResponseModel.ContactoGerencia.Cargo = ModelParametro.ContactoGerencia.Cargo;
                        ResponseModel.ContactoGerencia.Correo = ModelParametro.ContactoGerencia.Correo;
                        ResponseModel.ContactoGerencia.Celular = ModelParametro.ContactoGerencia.Celular;

                        ResponseModel.ContactoFacturacion.Nombre = ModelParametro.ContactoFacturacion.Nombre;
                        ResponseModel.ContactoFacturacion.ApellidoPaterno = ModelParametro.ContactoFacturacion.ApellidoPaterno;
                        ResponseModel.ContactoFacturacion.ApellidoMaterno = ModelParametro.ContactoFacturacion.ApellidoMaterno;
                        ResponseModel.ContactoFacturacion.Cargo = ModelParametro.ContactoFacturacion.Cargo;
                        ResponseModel.ContactoFacturacion.Correo = ModelParametro.ContactoFacturacion.Correo;
                        ResponseModel.ContactoFacturacion.Celular = ModelParametro.ContactoFacturacion.Celular;

                        ResponseModel.ContactoTecnico.Nombre = ModelParametro.ContactoTecnico.Nombre;
                        ResponseModel.ContactoTecnico.ApellidoPaterno = ModelParametro.ContactoTecnico.ApellidoPaterno;
                        ResponseModel.ContactoTecnico.ApellidoMaterno = ModelParametro.ContactoTecnico.ApellidoMaterno;
                        ResponseModel.ContactoTecnico.Cargo = ModelParametro.ContactoTecnico.Cargo;
                        ResponseModel.ContactoTecnico.Correo = ModelParametro.ContactoTecnico.Correo;
                        ResponseModel.ContactoTecnico.Celular = ModelParametro.ContactoTecnico.Celular;

                        Function_UpdateConfiguracion(ResponseModel, function (response) {
                            $("body").ModalAlert({
                                "Color": "Info",
                                "Icon": "fa fa-info",
                                "Text": response.Message
                            });
                        });
                    }
                } else if (response.Value == "1500") {
                    $("body").ModalAlert({
                        "Color": "Info",
                        "Icon": "fa fa-info",
                        "Text": response.Message
                    });
                } else if (response.Value == "2000") {
                    $("body").ModalAlert({
                        "Color": "Info",
                        "Icon": "fa fa-info",
                        "Text": response.Message
                    });
                } else if (response.Value == "3000") {
                    $("body").ModalFastAlert({
                        "MasterColor": "#000000",
                        "Color": "Danger",
                        "Text": response.Message,
                        "Title": "Error"
                    });
                }
            });
        } else {
            if (SignUpValidation()) {
                ResponseModel.Password = ModelParametro.Password;

                ResponseModel.Solicitante.Nombre = ModelParametro.Solicitante.Nombre;
                ResponseModel.Solicitante.ApellidoPaterno = ModelParametro.Solicitante.ApellidoPaterno;
                ResponseModel.Solicitante.ApellidoMaterno = ModelParametro.Solicitante.ApellidoMaterno;
                ResponseModel.Solicitante.Cargo = ModelParametro.Solicitante.Cargo;
                ResponseModel.Solicitante.Correo = ModelParametro.Solicitante.Correo;
                ResponseModel.Solicitante.Celular = ModelParametro.Solicitante.Celular;

                ResponseModel.ContactoGerencia.Nombre = ModelParametro.ContactoGerencia.Nombre;
                ResponseModel.ContactoGerencia.ApellidoPaterno = ModelParametro.ContactoGerencia.ApellidoPaterno;
                ResponseModel.ContactoGerencia.ApellidoMaterno = ModelParametro.ContactoGerencia.ApellidoMaterno;
                ResponseModel.ContactoGerencia.Cargo = ModelParametro.ContactoGerencia.Cargo;
                ResponseModel.ContactoGerencia.Correo = ModelParametro.ContactoGerencia.Correo;
                ResponseModel.ContactoGerencia.Celular = ModelParametro.ContactoGerencia.Celular;

                ResponseModel.ContactoFacturacion.Nombre = ModelParametro.ContactoFacturacion.Nombre;
                ResponseModel.ContactoFacturacion.ApellidoPaterno = ModelParametro.ContactoFacturacion.ApellidoPaterno;
                ResponseModel.ContactoFacturacion.ApellidoMaterno = ModelParametro.ContactoFacturacion.ApellidoMaterno;
                ResponseModel.ContactoFacturacion.Cargo = ModelParametro.ContactoFacturacion.Cargo;
                ResponseModel.ContactoFacturacion.Correo = ModelParametro.ContactoFacturacion.Correo;
                ResponseModel.ContactoFacturacion.Celular = ModelParametro.ContactoFacturacion.Celular;

                ResponseModel.ContactoTecnico.Nombre = ModelParametro.ContactoTecnico.Nombre;
                ResponseModel.ContactoTecnico.ApellidoPaterno = ModelParametro.ContactoTecnico.ApellidoPaterno;
                ResponseModel.ContactoTecnico.ApellidoMaterno = ModelParametro.ContactoTecnico.ApellidoMaterno;
                ResponseModel.ContactoTecnico.Cargo = ModelParametro.ContactoTecnico.Cargo;
                ResponseModel.ContactoTecnico.Correo = ModelParametro.ContactoTecnico.Correo;
                ResponseModel.ContactoTecnico.Celular = ModelParametro.ContactoTecnico.Celular;

                Function_UpdateConfiguracion(ResponseModel, function (response) {
                    $("body").ModalAlert({
                        "Color": "Info",
                        "Icon": "fa fa-info",
                        "Text": response.Message
                    });
                });
            }
        }
    }

    (function () {
        /* Cargar Datos del Proveedor */
        GetDatosProveedor();

        /* Declaración Solicitante*/
        jQuerySize($("#ruc-login"), 11);
        jQueryNumeric($("#ruc-login"));
        jQueryLetter($("#nombre-login"));
        jQueryLetter($("#ap-paterno-login"));
        jQueryLetter($("#ap-materno-login"));
        jQueryLetterNumeric($("#cargo-login"));
        jQuerySize($("#celular-login"), 12);
        jQueryPhoneNumeric($("#celular-login"));

        /* Declaración Contacto Gerencia y Facturación */
        jQueryLetter($("#nombre-gerencia-login"));
        jQueryLetter($("#ap-paterno-gerencia-login"));
        jQueryLetter($("#ap-materno-gerencia-login"));
        jQueryLetterNumeric($("#cargo-gerencia-login"));
        jQuerySize($("#celular-gerencia-login"), 12);
        jQueryPhoneNumeric($("#celular-gerencia-login"));

        jQueryLetter($("#nombre-facturacion-login"));
        jQueryLetter($("#ap-paterno-facturacion-login"));
        jQueryLetter($("#ap-materno-facturacion-login"));
        jQueryLetterNumeric($("#cargo-facturacion-login"));
        jQuerySize($("#celular-facturacion-login"), 12);
        jQueryPhoneNumeric($("#celular-facturacion-login"));

        /* Declaración Contacto Tecnico */
        jQueryLetter($("#nombre-tecnico-login"));
        jQueryLetter($("#ap-paterno-tecnico-login"));
        jQueryLetter($("#ap-materno-tecnico-login"));
        jQueryLetterNumeric($("#cargo-tecnico-login"));
        jQuerySize($("#celular-tecnico-login"), 12);
        jQueryPhoneNumeric($("#celular-tecnico-login"));
    })();
});