﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var EntryOrdenCompra =
    {
        CodiFana: 0,
        Proceso: 0,
        Tipo: null,
        Status: null,
        Consulta: null
    };
    var MotivosGlobal = null;

    var Global = GetGlobalVar();

    /* Functions Globales */
    $("#editar").on("click", function () {

        if (Global.RolId != 1) {
            return;
        }

        $(this).addClass("d-block-none");
        $("#guardar").removeClass("d-block-none");
        $("#ordenes-compra-datatable tbody").find("button[data-button=editar]").prop("disabled", true);
        $("#ordenes-compra-datatable tbody").find("input[data-input=fecha_confirmacion]").prop("disabled", false);
        $("#ordenes-compra-datatable tbody").find("select[data-select=motivo]").prop("disabled", true);
    });
    $("#guardar").on("click", function () {
        var ArrayOrden = new Array();

        $.each($("#ordenes-compra-datatable tbody").find("tr"), function (index, object) {
            var DataObject = $(object).data("Data");
            var fecha_entrega = DataObject.FechaEntrega;
            var fecha_confirmacion = $(object).find("input[data-input=fecha_confirmacion]").val();
            var motivo = $(object).find("select[data-select=motivo]").val();
            var aprob_object = $(object).find("button[data-button=estado]");

            if (!aprob_object.hasClass("bg-btn-success") ||
                (StringDateToJavaScriptDate(fecha_entrega) < StringDateToJavaScriptDate(fecha_confirmacion) && motivo == 0)) {
                return;
            }
            
            var Orden =
            {
                "CodiOrcm": DataObject.CodiOrcm,
                "FechaConfirmacion": fecha_confirmacion,
                "CodiMoti": motivo
            };
            ArrayOrden.push(Orden);
            
        });

        if (ArrayOrden.length == 0) {
            return;
        }

        var Parametros =
        {
            "OrdenCompra": ArrayOrden
        };

        if (Global.RolId != 1) {
            return;
        }

        $("body").ModalAlert({
            "Type": "Confirm",
            "Color": "Info",
            "Text": "¿Esta seguro que desea guardar?",
            "Confirm": function () {
                Function_SetOrdenesPorEntregarProveedor(Parametros, function (response) {
                    if (response.Value == "1000") {
                        $("body").ModalFastAlert({
                            "Color": "Success",
                            "Text": response.Message,
                            "Title": "Correcto!"
                        });
                        $("#buscar").trigger("click");
                    } else {
                        $("body").ModalFastAlert({
                            "MasterColor": "#000000",
                            "Color": "Danger",
                            "Text": response.Message,
                            "Title": "Error"
                        });
                    }
                });

                return true;
            }
        });
    });

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {        
        Function_ExportToExcel($(this), EntryOrdenCompra);
    });

    /* Obtener Listado de Ordenes */
    $("#buscar").on("click", function () {
        var Analista = $("#analista").val() == undefined ? 0 : $("#analista").val();
        var Proceso = $("#proceso").val() == undefined ? 0 : $("#proceso").val();
        var Tipo = $("#tipo").val();
        var Status = $("#status").val();
        var Consulta = $("#consulta").val();

        EntryOrdenCompra.CodiFana = Analista;
        EntryOrdenCompra.Proceso = Proceso;
        EntryOrdenCompra.Tipo = Tipo;
        EntryOrdenCompra.Status = Status;
        EntryOrdenCompra.Consulta = Consulta;

        $("#editar").removeClass("d-block-none");
        $("#guardar").addClass("d-block-none");

        $("#editar").prop("disabled", false);
        
        BuildDataTable();
    });
    
    function BuildDataTable() {
        Function_ListOrdenesCompra(EntryOrdenCompra, function (response) {
            var table = $("#ordenes-compra-datatable");
            table.DataTable().clear().destroy();

            $.each(response.Response, function (index, object) {
                var Color = "";
                switch (object.Estado) {
                    case "VE":
                        Color = "bg-btn-danger";
                        break;
                    case "PV":
                        Color = "bg-btn-warning";
                        break;
                    case "VI":
                        Color = "bg-btn-success";
                        break;
                }
                if (Global.ProfileId != Global.ProveedorProfileId) {
                    var l_tr = $(`<tr>
                                <td><button data-action="detail" data-estado-detail="close" class="button-detail">
                                    <i class="fa fa-plus-circle"></i></button></td>
                                <td><button class="button-estatus bg-btn-disabled">
                                        <i class="fa fa-cubes"></i></button></td>
                                <td title="` + object.Proveedor + `">`
                                    + (object.Proveedor.length > 7 ? object.Proveedor.substring(0, 7) : object.Proveedor) + `</td>
                                <td>` + object.DocumentoCompra + `</td>
                                <td>` + object.Posicion + `</td>
                                <td title="` + object.Descripcion + `">` + (object.Descripcion.length > 7 ? object.Descripcion.substring(0, 7) : object.Descripcion) + `</td>
                                <td>` + object.CantidadPendiente + `</td>
                                <td>` + object.UM + `</td>
                                <td class="TotalSearch" title="` + object.Analista + `">` + (object.Analista.length > 7 ? object.Analista.substring(0, 7) : object.Analista) + `</td>
                                <td>` + object.FechaEntrega + ` <label class="status-cube ` + Color + `"></label></td>
                                <td>` + (object.FechaConfirmacion == '01/01/1901' ? `` : object.FechaConfirmacion) + `</td>
                                <td title="` + (object.Motivo == undefined ? `` : object.Motivo) + `">` +
                                    (object.Motivo == undefined ? `` : (object.Motivo.length > 9 ? object.Motivo.substring(0, 9) : object.Motivo)) + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                    </div>
                                </td>
                            </tr>`);
                    
                    if (object.FechaConfirmacion == '01/01/1901') {
                        var btn = $(`<button class="bg-btn-success o-disabled" data-button="aprobar"><i class="fa fa-check"></i></button>
                                    <button class="bg-btn-danger o-disabled" data-button="rechazar"><i class="fa fa-times"></i></button>
                                    <button class="bg-btn-warning" data-button="remove"><i class="fa fa-trash"></i></button>`);
                        l_tr.find(".acciones-datatable-1").append(btn);
                    } else if (object.Motivo == undefined || object.Motivo == "") {
                        if (StringDateToJavaScriptDate(object.FechaEntrega) >= StringDateToJavaScriptDate(object.FechaConfirmacion)) {
                            var btn = $(`<button class="bg-btn-success o-disabled" data-button="aprobar"><i class="fa fa-check"></i></button>
                                        <button class="bg-btn-warning" data-button="remove"><i class="fa fa-trash"></i></button>`);
                            l_tr.find(".acciones-datatable-1").append(btn);
                        }
                    } else {
                        var btn = $(`<button class="bg-btn-success" data-button="aprobar"><i class="fa fa-check"></i></button>
                                    <button class="bg-btn-danger" data-button="rechazar"><i class="fa fa-times"></i></button>
                                    <button class="bg-btn-warning" data-button="remove"><i class="fa fa-trash"></i></button>`);
                        l_tr.find(".acciones-datatable-1").append(btn);
                        
                        l_tr.find(".acciones-datatable-1").on("click", "button[data-button=aprobar]", function () {
                            var ArrayOrden = new Array();
                            var Orden =
                            {
                                "CodiOrcm": object.CodiOrcm,
                                "Estado": "A"
                            };
                            ArrayOrden.push(Orden);
                            var Parametros =
                            {
                                "OrdenCompra": ArrayOrden
                            };

                            $("body").ModalAlert({
                                "Type": "Confirm",
                                "Color": "Info",
                                "Text": "¿Esta seguro que desea aprobar?",
                                "Confirm": function () {
                                    Function_SetOrdenesPorEntregarAnalista(Parametros, function (response) {
                                        if (response.Value == "1000") {
                                            $("body").ModalFastAlert({
                                                "Color": "Success",
                                                "Text": response.Message,
                                                "Title": "Correcto!"
                                            });
                                            $("#buscar").trigger("click");
                                        } else {
                                            $("body").ModalFastAlert({
                                                "MasterColor": "#000000",
                                                "Color": "Danger",
                                                "Text": response.Message,
                                                "Title": "Error"
                                            });
                                        }
                                    });

                                    return true;
                                }
                            }); 
                        });
                        l_tr.find(".acciones-datatable-1").on("click", "button[data-button=rechazar]", function () {
                            var ArrayOrden = new Array();
                            var Orden =
                            {
                                "CodiOrcm": object.CodiOrcm,
                                "Estado": "R"
                            };
                            ArrayOrden.push(Orden);
                            var Parametros =
                            {
                                "OrdenCompra": ArrayOrden
                            };

                            $("body").ModalAlert({
                                "Type": "Confirm",
                                "Color": "Info",
                                "Text": "¿Esta seguro que desea rechazar?",
                                "Confirm": function () {
                                    Function_SetOrdenesPorEntregarAnalista(Parametros, function (response) {
                                        if (response.Value == "1000") {
                                            $("body").ModalFastAlert({
                                                "Color": "Success",
                                                "Text": response.Message,
                                                "Title": "Correcto!"
                                            });
                                            $("#buscar").trigger("click");
                                        } else {
                                            $("body").ModalFastAlert({
                                                "MasterColor": "#000000",
                                                "Color": "Danger",
                                                "Text": response.Message,
                                                "Title": "Error"
                                            });
                                        }
                                    });

                                    return true;
                                }
                            }); 
                        });                        
                    }

                    l_tr.find(".acciones-datatable-1").on("click", "button[data-button=remove]", function () {
                        var motivo = "-";

                        var Parametros =
                        {
                            "CodiOrcm": object.CodiOrcm,
                            "Motivo": motivo
                        };

                        $("body").ModalAlert({
                            "Type": "Confirm",
                            "Color": "Danger",
                            "Text": "¿Esta seguro que desea eliminar esta orden?",
                            "Confirm": function () {
                                Function_RemoveOrdenesPorEntregar(Parametros, function (response) {
                                    if (response.Value == "1000") {
                                        $("body").ModalFastAlert({
                                            "Color": "Success",
                                            "Text": response.Message,
                                            "Title": "Correcto!"
                                        });
                                        $("#buscar").trigger("click");
                                    } else {
                                        $("body").ModalFastAlert({
                                            "MasterColor": "#000000",
                                            "Color": "Danger",
                                            "Text": response.Message,
                                            "Title": "Error"
                                        });
                                    }
                                });

                                return true;
                            }
                        });
                    });
                    l_tr.find("button[data-action=detail]").on("click", function () {
                        var estado = $(this).attr("data-estado-detail");
                        if (estado == "open") {
                            $(this).attr("data-estado-detail", "close");
                            $(this).find("i").addClass("fa-plus-circle");
                            $(this).find("i").removeClass("fa-minus-circle");
                            if ($(this).closest("tr").next()[0].localName == "td") {
                                $(this).closest("tr").next().remove();
                            }
                        } else {
                            $(this).attr("data-estado-detail", "open");
                            $(this).find("i").removeClass("fa-plus-circle");
                            $(this).find("i").addClass("fa-minus-circle");

                            var tab = $(`<td colspan="5"><table class="display table table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 60px;">Importe Total</th>
                                            <th style="width: 40px;">Moneda</th>
                                            <th style="width: 60px;">Fecha Documento</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>` + object.ImporteTotal + `</td>
                                            <td>` + object.TipoMoneda + `</td>
                                            <td>` + object.FechaDocumento + `</td>
                                        </tr>
                                    </tbody>
                                </table></td>`);
                            tab.insertAfter(l_tr);
                        }
                    });                    

                    table.find("tbody").append(l_tr);
                } else {
                    var l_tr = $(`<tr>
                                <td><button data-action="detail" data-estado-detail="close" class="button-detail">
                                    <i class="fa fa-plus-circle"></i></button></td>
                                <td><button class="button-estatus bg-btn-disabled">
                                        <i class="fa fa-cubes"></i></button></td>
                                <td>` + object.DocumentoCompra + `</td>
                                <td>` + object.Posicion + `</td>
                                <td title="` + object.Descripcion + `">` + (object.Descripcion.length > 7 ? object.Descripcion.substring(0, 7) : object.Descripcion) + `</td>
                                <td>` + object.CantidadPendiente + `</td>
                                <td>` + object.UM + `</td >
                                <td title="` + object.Analista + `">`                                
                                    + (object.Analista.length > 7 ? object.Analista.substring(0, 7) : object.Analista) + `</td>
                                <td>` + object.FechaEntrega + ` <label class="status-cube ` + Color + `"></label></td>
                                <td><input type="text" data-input="fecha_confirmacion"  style="width: 66px;" disabled/></td>
                                <td><select data-select="motivo" style="width: 120px" disabled></select></td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-disabled-2" data-button="editar" style="color: #b6b6b6; margin-left: 0px;">
                                            <i class="fa fa-edit"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-disabled-2" data-button="estado" style="color: white; margin-left: 0px;">
                                            <i class="fa fa-check"></i></button>
                                        <button class="bg-btn-warning" data-button="detail" style="color: white; margin-left: 3px;">
                                            <i class="fa fa-eye"></i></button>
                                    </div>
                                </td>
                            </tr>`);

                    l_tr.data("Data", object);
                    table.find("tbody").append(l_tr);

                    if (Global.RolId != 1) {
                        return;
                    }
                    
                    $.each(MotivosGlobal, function (index, object) {
                        if (index == 0) {
                            l_tr.find("select[data-select=motivo]").append("<option value='0'>-- Seleccione --</option>");
                        }
                        var option = $("<option value='" + object.CodiMoti + "'>" + object.Descripcion + "</option>");
                        l_tr.find("select[data-select=motivo]").append(option);
                    });
                    //l_tr.find("input[data-input=fecha_confirmacion]").datetimepicker({
                    //    format: 'DD/MM/YYYY'
                    //});
                    l_tr.find("input[data-input=fecha_confirmacion]").datepicker({ dateFormat: 'dd/mm/yy' });

                    l_tr.find(".acciones-datatable-1").on("click", "button[data-button=guardar]", function () {
                        var fecha_entrega = object.FechaEntrega;
                        var fecha_confirmacion = l_tr.find("input[data-input=fecha_confirmacion]").val();
                        var motivo = l_tr.find("select[data-select=motivo]").val();
                        var aprob_object = l_tr.find("button[data-button=estado]");

                        if (!aprob_object.hasClass("bg-btn-success") ||
                            (StringDateToJavaScriptDate(fecha_entrega) < StringDateToJavaScriptDate(fecha_confirmacion) && motivo == 0)) {
                            return;
                        }

                        var ArrayOrden = new Array();
                        var Orden =
                        {
                            "CodiOrcm": object.CodiOrcm,
                            "FechaConfirmacion": fecha_confirmacion,
                            "CodiMoti": motivo
                        };
                        ArrayOrden.push(Orden);
                        var Parametros =
                        {
                            "OrdenCompra": ArrayOrden
                        };

                        $("body").ModalAlert({
                            "Type": "Confirm",
                            "Color": "Info",
                            "Text": "¿Esta seguro que desea guardar?",
                            "Confirm": function () {
                                Function_SetOrdenesPorEntregarProveedor(Parametros, function (response) {
                                    if (response.Value == "1000") {
                                        $("body").ModalFastAlert({
                                            "Color": "Success",
                                            "Text": response.Message,
                                            "Title": "Correcto!"
                                        });
                                        $("#buscar").trigger("click");
                                    } else {
                                        $("body").ModalFastAlert({
                                            "MasterColor": "#000000",
                                            "Color": "Danger",
                                            "Text": response.Message,
                                            "Title": "Error"
                                        });
                                    }
                                });

                                return true;
                            }
                        }); 
                    });
                    l_tr.find(".acciones-datatable-1").on("click", "button[data-button=detail]", function () {
                        var div = `<table class='display table table-striped table-hover'>
                                    <thead><tr><th>Motivo</th><th>Fecha C.</th></tr></thead><tbody>`;
                        Function_ListOrdenesCompraDetalle({ "CodiOrcm": object.CodiOrcm }, function (response) {
                            var cont = 0;
                            $.each(response.Response, function (idx, obj) {
                                div = div + "<tr><td>" + obj.Motivo + "</td><td>" + obj.FechaConfirmacion + "</td></tr>";
                                cont++;
                            });

                            if (cont == 0) {
                                return;
                            }

                            div = div + "</tbody></table>";

                            $("body").ModalAlertPlugin({
                                "Type": "Info",
                                "Color": "Danger",
                                "Content": div
                            });
                        });                        
                    });
                    l_tr.find("button[data-button=editar]").on("click", function () {
                        var btn_save = $(`<button class="bg-btn-disabled-2" data-button="guardar" style="color: #b6b6b6;" disabled>
                                            <i class="fa fa-save"></i></button>`);
                        var section_btn = $(this).closest("div");
                        section_btn.append(btn_save);
                        $(this).remove();

                        $("#editar").prop("disabled", true);

                        l_tr.find("input[data-input=fecha_confirmacion]").prop("disabled", false);
                    });
                    l_tr.find("input[data-input=fecha_confirmacion]").on("change", function () {
                        l_tr.find("select[data-select=motivo]").val(0);
                        var fecha_entrega = object.FechaEntrega;
                        var fecha_confirmacion = l_tr.find("input[data-input=fecha_confirmacion]").val();
                        var motivo = l_tr.find("select[data-select=motivo]").val();
                        var aprob_object = l_tr.find("button[data-button=estado]");
                        
                        if (StringDateToJavaScriptDate(fecha_entrega) >= StringDateToJavaScriptDate(fecha_confirmacion)) {
                            aprob_object.addClass("bg-btn-success");
                            l_tr.find("select[data-select=motivo]").prop("disabled", true);
                            l_tr.find("button[data-button=guardar]").prop("disabled", false);
                        } else if (StringDateToJavaScriptDate(fecha_entrega) < StringDateToJavaScriptDate(fecha_confirmacion)
                            && motivo > 0) {
                            aprob_object.addClass("bg-btn-success");  
                            
                            l_tr.find("select[data-select=motivo]").prop("disabled", false);
                            l_tr.find("button[data-button=guardar]").prop("disabled", false);
                        } else if (StringDateToJavaScriptDate(fecha_entrega) < StringDateToJavaScriptDate(fecha_confirmacion)) {
                            aprob_object.removeClass("bg-btn-success");
                            l_tr.find("select[data-select=motivo]").prop("disabled", false);
                        } else {
                            aprob_object.removeClass("bg-btn-success");
                            l_tr.find("select[data-select=motivo]").prop("disabled", true);
                            l_tr.find("button[data-button=guardar]").prop("disabled", true);
                        }
                    });
                    l_tr.find("select[data-select=motivo]").on("change", function () {
                        var fecha_entrega = object.FechaEntrega;
                        var fecha_confirmacion = l_tr.find("input[data-input=fecha_confirmacion]").val();
                        var motivo = l_tr.find("select[data-select=motivo]").val();
                        var aprob_object = l_tr.find("button[data-button=estado]");

                        if (StringDateToJavaScriptDate(fecha_entrega) >= StringDateToJavaScriptDate(fecha_confirmacion)) {
                            aprob_object.addClass("bg-btn-success");
                            l_tr.find("select[data-select=motivo]").prop("disabled", true);
                            l_tr.find("button[data-button=guardar]").prop("disabled", false);
                        } else if (StringDateToJavaScriptDate(fecha_entrega) < StringDateToJavaScriptDate(fecha_confirmacion)
                            && motivo > 0) {
                            aprob_object.addClass("bg-btn-success");
                            l_tr.find("select[data-select=motivo]").prop("disabled", false);
                            l_tr.find("button[data-button=guardar]").prop("disabled", false);
                        } else if (StringDateToJavaScriptDate(fecha_entrega) < StringDateToJavaScriptDate(fecha_confirmacion)) {
                            aprob_object.removeClass("bg-btn-success");
                            l_tr.find("select[data-select=motivo]").prop("disabled", false);
                        } else {
                            aprob_object.removeClass("bg-btn-success");
                            l_tr.find("select[data-select=motivo]").prop("disabled", true);
                            l_tr.find("button[data-button=guardar]").prop("disabled", true);
                        }
                    });
                    l_tr.find("button[data-action=detail]").on("click", function () {
                        var estado = $(this).attr("data-estado-detail");
                        if (estado == "open") {
                            $(this).attr("data-estado-detail", "close");
                            $(this).find("i").addClass("fa-plus-circle");
                            $(this).find("i").removeClass("fa-minus-circle");
                            if ($(this).closest("tr").next()[0].localName == "td") {
                                $(this).closest("tr").next().remove();
                            }
                        } else {
                            $(this).attr("data-estado-detail", "open");
                            $(this).find("i").removeClass("fa-plus-circle");
                            $(this).find("i").addClass("fa-minus-circle");

                            var tab = $(`<td colspan="5"><table class="display table table-striped table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th>Importe Total</th>
                                            <th>Moneda</th>
                                            <th>Fecha Documento</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>` + object.ImporteTotal + `</td>
                                            <td>` + object.TipoMoneda + `</td>
                                            <td>` + object.FechaDocumento + `</td>
                                        </tr>
                                    </tbody>
                                </table></td>`);
                            tab.insertAfter(l_tr);
                        }
                    });
                }
            });

            if (Global.ProfileId != Global.ProveedorProfileId) {

                if ($("#analista").val() == "0" || $("#analista").val() == null) {
                    $(".TotalSearch").removeClass("d-block-none");
                } else {
                    $(".TotalSearch").addClass("d-block-none");
                }

                $.fn.dataTable.moment = function (format, locale) {
                    var types = $.fn.dataTable.ext.type;

                    // Add type detection
                    types.detect.unshift(function (d) {
                        return moment(d, format, locale, true).isValid() ?
                            'moment-' + format :
                            null;
                    });

                    // Add sorting method - use an integer for the sorting
                    types.order['moment-' + format + '-pre'] = function (d) {
                        return moment(d, format, locale, true).unix();
                    };
                };
                $.fn.dataTable.moment('DD/MM/YYYY');
                table.DataTable({
                    "scrollX": true,
                    "aaSorting": [[9, "asc"]],
                    "aoColumns": [
                        { "bSortable": false },
                        { "bSortable": false },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": false }
                    ],
                    "oLanguage": {
                        "sLengthMenu": "Mostrar _MENU_ registros por página",
                        "sZeroRecords": "No se encontró nada, lo sentimos",
                        "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                        "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                        "sSearch": "Buscar:",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            } else {
                $.fn.dataTable.moment = function (format, locale) {
                    var types = $.fn.dataTable.ext.type;

                    // Add type detection
                    types.detect.unshift(function (d) {
                        return moment(d, format, locale, true).isValid() ?
                            'moment-' + format :
                            null;
                    });

                    // Add sorting method - use an integer for the sorting
                    types.order['moment-' + format + '-pre'] = function (d) {
                        return moment(d, format, locale, true).unix();
                    };
                };
                $.fn.dataTable.moment('DD/MM/YYYY');
                table.DataTable({
                    "scrollX": true,
                    "aaSorting": [[12, "asc"]],
                    "aoColumns": [
                        { "bSortable": false },
                        { "bSortable": false },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": true },
                        { "bSortable": false },
                        { "bSortable": false },
                        { "bSortable": false },
                        { "bSortable": false }
                    ],
                    "oLanguage": {
                        "sLengthMenu": "Mostrar _MENU_ registros por página",
                        "sZeroRecords": "No se encontró nada, lo sentimos",
                        "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                        "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                        "sSearch": "Buscar:",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        }
                    }
                });
            }            
            
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");
        });
    }

    (function () {
        /* Inicializar Filtros para el Analista */
        if (Global.ProfileId != Global.ProveedorProfileId) {
            Function_ListAnalista(function (response) {
                var selectAnalista = $("#analista");
                $.each(response.Response.Analista, function (index, object) {
                    var option = $("<option value='" + object.CodiFana + "'>" + object.Nombre + "</option>");
                    selectAnalista.append(option);
                });
                selectAnalista.append("<option value='0'>Todos</option>");
                selectAnalista.val(Global.AnalistaId).trigger("click");

                Function_ListProceso(function (response) {
                    var selectProceso = $("#proceso");
                    $.each(response.Response, function (index, object) {
                        var option = $("<option value='" + object.CodiProc + "'>" + object.Descripcion + "</option>");
                        selectProceso.append(option);
                    });
                    selectProceso.append("<option value='0'>Todos</option>");
                    $(selectProceso.find("option")[0]).trigger("click");      
                    $("#buscar").trigger("click");
                });
            });
        } else {
            /* Empezar la pagina con la Busqueda de las opciones */
            Function_ListMotivo(function (response) {
                MotivosGlobal = response.Response;

                $("#buscar").trigger("click");
            });            
        }

    })();
});