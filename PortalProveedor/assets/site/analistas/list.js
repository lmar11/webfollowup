﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var Global = GetGlobalVar();

    /* Functions Globales */
    $("#add").on("click", function () {
        var div = $(`<div class="form-group-1">
                        <label>Nombre</label>
                        <input data-input="nombre" style="border: 1px solid silver;" />
                        <input data-input="display" style="border: 1px solid silver; display: none;" />
                    </div>
                    <div class="form-group-1">
                        <label>Grupo de Compra</label>
                        <input data-input="grupo_compra" style="border: 1px solid silver;" />
                    </div>`);
        div.find("input[data-input=nombre]").autocomplete({
            source: function (request, response) {
                $.ajax({
                    data: { "Termino": request.term },
                    url: "/Analistas/List_AnalistasAutocompleteMantenimiento",
                    type: "POST",
                    dataType: "json",
                    //data: {
                    //    search: request.term
                    //},
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (item) {
                            return {
                                label: item.Nombres,
                                value: item.Nombres,
                                result: item.CodiPers
                            };
                        }));
                    }
                });
            },
            minLength: 5,
            select: function (evnt, ui) {
                div.find("input[data-input=display]").val(ui.item.result);
            }
        });
        $("body").ModalAlertPlugin({
            "Type": "Save",
            "Color": "Info",
            "Content": div,
            "Save": function () {
                var Element = $(this)[0].Element[0];
                var CodiPers = $(Element).find("input[data-input=display]").val();
                var GrupoCompra = $(Element).find("input[data-input=grupo_compra]").val();
                if (CodiPers == "" || GrupoCompra == "") {
                    return false;
                } else {
                    var Parametros =
                    {
                        CodiPers: CodiPers,
                        GrupoCompra: GrupoCompra
                    };
                    Function_SetAnalistaMantenimiento(Parametros, function (response) {
                        if (response.Value == "1000") {
                            $("body").ModalFastAlert({
                                "Color": "Success",
                                "Text": response.Message,
                                "Title": "Correcto!"
                            });
                            BuildDataTable();
                        } else {
                            $("body").ModalFastAlert({
                                "MasterColor": "#000000",
                                "Color": "Danger",
                                "Text": response.Message,
                                "Title": "Error"
                            });
                        }
                    });
                    return true;
                }
            }
        });         
    });

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this));
    });

    /* Obtener Listado */
    function BuildDataTable() {
        Function_ListAnalistasMantenimiento(function (response) {
            var table = $("#analistas-datatable");
            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {
                var l_tr = $(`<tr>
                                <td>` + object.Nombres + `</td>
                                <td>` + object.GrupoCompra + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-danger" data-button="remove"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>`);
                
                l_tr.find("button[data-button=remove]").on("click", function () {
                    $("body").ModalAlert({
                        "Type": "Confirm",
                        "Color": "Info",
                        "Text": "¿Esta seguro que desea eliminar a este analista?",
                        "Confirm": function () {
                            var Parametros =
                            {
                                CodiPers: object.CodiPers
                            };
                            Function_RemoveAnalistaMantenimiento(Parametros, function (response) {
                                if (response.Value == "1000") {
                                    $("body").ModalFastAlert({
                                        "Color": "Success",
                                        "Text": response.Message,
                                        "Title": "Correcto!"
                                    });
                                    BuildDataTable();
                                } else {
                                    $("body").ModalFastAlert({
                                        "MasterColor": "#000000",
                                        "Color": "Danger",
                                        "Text": response.Message,
                                        "Title": "Error"
                                    });
                                }
                            });

                            return true;
                        }
                    });
                });
                
                table.find("tbody").append(l_tr);
            });
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");
        });
    }

    (function () {
        /* Inicializar DataTablet */
        BuildDataTable();
    })();
});