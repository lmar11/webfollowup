﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var Global = GetGlobalVar();

    /* Functions Globales */
    $("#add").on("click", function () {
        $("body").ModalAlertPlugin({
            "Type": "Save",
            "Color": "Info",
            "Content": `<div class="form-group-1">
                            <label>Categoria de Material - Clave</label>
                            <input data-input="clave" style="border: 1px solid silver;" />
                        </div>
                        <div class="form-group-1">
                            <label>Categoria de Material</label>
                            <input data-input="categoria" style="border: 1px solid silver;" />
                        </div>`,
            "Save": function () {
                var Element = $(this)[0].Element[0];
                var Clave = $(Element).find("input[data-input=clave]").val();
                var Descripcion = $(Element).find("input[data-input=categoria]").val();
                if (Clave == "" || Descripcion == "") {
                    return false;
                } else {
                    var Parametros =
                    {
                        CodiCate: 0,
                        CodiProc: $("#proceso-filtro").val(),
                        Clave: Clave,
                        Descripcion: Descripcion
                    };
                    Function_SetCategoriaMantenimiento(Parametros, function (response) {
                        if (response.Value == "1000") {
                            $("body").ModalFastAlert({
                                "Color": "Success",
                                "Text": response.Message,
                                "Title": "Correcto!"
                            });
                            BuildDataTable();
                        } else {
                            $("body").ModalFastAlert({
                                "MasterColor": "#000000",
                                "Color": "Danger",
                                "Text": response.Message,
                                "Title": "Error"
                            });
                        }
                    });
                    return true;
                }
            }
        });         
    });
    $("#buscar").on("click", function () {
        BuildDataTable();
    });

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this));
    });

    /* Obtener Listado */
    function BuildDataTable() {
        var proceso = $("#proceso-filtro").val();
        Function_ListCategoriasMantenimiento({ "CodiProc": proceso }, function (response) {
            var table = $("#categorias-datatable");
            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {
                var l_tr = $(`<tr>
                                <td>` + object.Clave + `</td>
                                <td>` + object.Descripcion + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-light-blue" data-button="edit"><i class="fa fa-edit"></i></button>
                                        <button class="bg-btn-danger" data-button="remove"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>`);
                
                l_tr.find("button[data-button=edit]").on("click", function () {
                    var div = $(`<div class="form-group-1">
                                        <label>Categoria de Material - Clave</label>
                                        <input data-input="clave" style="border: 1px solid silver;" />
                                    </div>
                                    <div class="form-group-1">
                                        <label>Categoria de Material</label>
                                        <input data-input="categoria" style="border: 1px solid silver;" />
                                    </div>`);
                    div.find("input[data-input=clave]").val(object.Clave);
                    div.find("input[data-input=categoria]").val(object.Descripcion);
                    $("body").ModalAlertPlugin({
                        "Type": "Save",
                        "Color": "Warning",
                        "Content": div,
                        "Save": function () {
                            var Element = $(this)[0].Element[0];
                            var Clave = $(Element).find("input[data-input=clave]").val();
                            var Descripcion = $(Element).find("input[data-input=categoria]").val();
                            if (Clave == "" || Descripcion == "") {
                                return false;
                            } else {
                                var Parametros =
                                {
                                    CodiCate: object.CodiCate,
                                    CodiProc: $("#proceso-filtro").val(),
                                    Clave: Clave,
                                    Descripcion: Descripcion
                                };
                                Function_SetCategoriaMantenimiento(Parametros, function (response) {
                                    if (response.Value == "1000") {
                                        $("body").ModalFastAlert({
                                            "Color": "Success",
                                            "Text": response.Message,
                                            "Title": "Correcto!"
                                        });
                                        BuildDataTable();
                                    } else {
                                        $("body").ModalFastAlert({
                                            "MasterColor": "#000000",
                                            "Color": "Danger",
                                            "Text": response.Message,
                                            "Title": "Error"
                                        });
                                    }
                                });
                                return true;
                            }
                        }
                    }); 
                });
                l_tr.find("button[data-button=remove]").on("click", function () {
                    $("body").ModalAlert({
                        "Type": "Confirm",
                        "Color": "Info",
                        "Text": "¿Esta seguro que desea eliminar esta Categoria de Material?",
                        "Confirm": function () {
                            var Parametros =
                            {
                                CodiCate: object.CodiCate
                            };
                            Function_RemoveCategoriaMantenimiento(Parametros, function (response) {
                                if (response.Value == "1000") {
                                    $("body").ModalFastAlert({
                                        "Color": "Success",
                                        "Text": response.Message,
                                        "Title": "Correcto!"
                                    });
                                    BuildDataTable();
                                } else {
                                    $("body").ModalFastAlert({
                                        "MasterColor": "#000000",
                                        "Color": "Danger",
                                        "Text": response.Message,
                                        "Title": "Error"
                                    });
                                }
                            });

                            return true;
                        }
                    });
                });
                
                table.find("tbody").append(l_tr);
            });
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            //$(".dataTables_filter").addClass("d-block-none");
            $(".dataTables_filter").addClass("dataTable-Filter-active");
        });
    }

    (function () {
        /* Inicializar Procesos */
        Function_ListProcesosMantenimiento(function (response) {
            var select = $("#proceso-filtro");
            $.each(response.Response, function (index, object) {
                var option = $("<option value='" + object.CodiProc + "'>" + object.Descripcion + "</option>");
                select.append(option);
            });
            $("#buscar").trigger("click");
        });
    })();
});