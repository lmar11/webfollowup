﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var EntryModel =
    {
        CodiFana: 0,
        Proceso: null,
        CodigoSapProveedor: null
    };
    var EntryOrdenCompra =
    {
        CodiFana: 0,
        Proceso: 0,
        Tipo: null,
        Consulta: null
    };

    var Global = GetGlobalVar();

    /* Functions Globales */
    function DateToFormatDate(date) {
        var Split = date.split('/');
        var FinalDate = Split[2] + "-" + Split[1] + "-" + Split[0];
        return FinalDate;
    }

    /* Funcion de Carga Página */
    var CountFunctionsReady = 0;
    function ValidateLoadPageAsync() {
        if (CountFunctionsReady == 1) {
            HideLoading();
        }
    }

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this), EntryOrdenCompra);
    });

    /* Obtener Listado de Ordenes */
    $("#buscar").on("click", function () {
        var Analista = $("#analista").val() == undefined ? 0 : $("#analista").val();
        var Proceso = $("#proceso").val() == undefined ? 0 : $("#proceso").val();
        var Tipo = $("#tipo").val();
        var Consulta = $("#consulta").val();

        EntryOrdenCompra.CodiFana = Analista;
        EntryOrdenCompra.Proceso = Proceso;
        EntryOrdenCompra.Tipo = Tipo;
        EntryOrdenCompra.Consulta = Consulta;

        CountFunctionsReady = 0;
        BuildDataTable();
        Search();
    });

    function BuildDataTable() {
        var l_td;
        Function_ListOrdenesCompra(EntryOrdenCompra, function (response) {
            var table = $("#ordenes-compra-datatable");

            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {
                if (Global.ProfileId != Global.ProveedorProfileId) {
                    var l_tr = $(`<tr>
                                <td>` + object.DocumentoCompra + `</td>                                
                                <td>` + object.Posicion + `</td>       
                                <td title="` + object.Descripcion + `">`
                        + (object.Descripcion.length > 20 ? object.Descripcion.substring(0, 20) : object.Descripcion) + `</td>
                                <td>` + object.FechaDocumento + `</td>
                                <td>` + object.Motivo + `</td>
                                <td title="` + object.Proveedor + `">`
                        + (object.Proveedor.length > 7 ? object.Proveedor.substring(0, 7) : object.Proveedor) + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-light-orange" data-button="ver" data-estado="closed">Ver Observación</button>
                                    </div>
                                </td>
                            </tr>`);

                    l_tr.find("button[data-button=ver]").on("click", function () {
                        if ($(this).attr("data-estado") == "closed") {
                            table.find("button[data-estado=openned]").text("Ver Observación");
                            table.find("button[data-estado=openned]").attr("data-estado", "closed");

                            $(this).attr("data-estado", "openned");
                            $(this).text("Ocultar Observación");
                            BuildDatailAnalistaDataTable(l_tr, object);
                        } else {
                            $(this).text("Ver Observación");
                            try {
                                $(this).attr("data-estado", "closed");
                                l_td.remove();
                            } catch {

                            }
                        }
                    });
                    table.find("tbody").append(l_tr);
                } else {
                    var l_tr = $(`<tr>
                                <td>` + object.DocumentoCompra + `</td>                                
                                <td>` + object.Posicion + `</td>
                                <td title="` + object.Descripcion + `">`
                        + (object.Descripcion.length > 20 ? object.Descripcion.substring(0, 20) : object.Descripcion) + `</td>
                                <td>` + object.FechaDocumento + `</td>
                                <td>` + object.Motivo + `</td>
                                <td title="` + object.Analista + `">`
                        + (object.Analista.length > 7 ? object.Analista.substring(0, 7) : object.Analista) + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        <button class="bg-btn-light-orange" data-button="ver" data-estado="closed">
                                            Ver Observación</button>
                                    </div>
                                </td>
                            </tr>`);

                    table.find("tbody").append(l_tr);

                    if (Global.RolId != 1) {
                        return;
                    }

                    l_tr.find("button[data-button=ver]").on("click", function () {
                        if ($(this).attr("data-estado") == "closed") {
                            table.find("button[data-estado=openned]").text("Ver Observación");
                            table.find("button[data-estado=openned]").attr("data-estado", "closed");

                            $(this).attr("data-estado", "openned");
                            $(this).text("Ocultar Observación");
                            BuildDatailDataTable(l_tr, object);
                        } else {
                            $(this).text("Ver Observación");
                            try {
                                $(this).attr("data-estado", "closed");
                                l_td.remove();
                            } catch {

                            }
                        }
                    });
                }
            });
            $.fn.dataTable.moment = function (format, locale) {
                var types = $.fn.dataTable.ext.type;

                // Add type detection
                types.detect.unshift(function (d) {
                    return moment(d, format, locale, true).isValid() ?
                        'moment-' + format :
                        null;
                });

                // Add sorting method - use an integer for the sorting
                types.order['moment-' + format + '-pre'] = function (d) {
                    return moment(d, format, locale, true).unix();
                };
            };
            $.fn.dataTable.moment('DD/MM/YYYY');
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[3, "asc"]],
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");

            CountFunctionsReady++;
            ValidateLoadPageAsync();
        });
        function BuildDatailDataTable(l_tr, object) {
            if (l_td != null && l_td != undefined) {
                l_td.remove();
            }

            l_td = $(`<td colspan="12">
                    <div class="row mt-3 mb-3">
                        <div class="col-1"></div>
                        <div class="col-8 motivo_row"></div>
                        <div class="col-2" style="display: flex; align-items: center;">
                        </div>
                        <div class="col-1"></div>
                    </div>
                </td>`);

            Function_ListOrdenesCompraDetail({ "CodiOrcm": object.CodiOrcm }, function (response) {
                $.each(response.Response, function (idx, obj) {
                    var Col_1 = "Motivo " + (idx + 1);
                    var Col_2 = obj.Motivo + " OC";
                    var Col_3 = obj.Motivo + " Nuevo";

                    var Col_1_Data = obj.Motivo;
                    var Col_2_Data = obj.ValorActual;
                    var Col_3_Data = obj.Cambio;

                    var Motivo_Row = $(`<div class="row" style="font-weight: 800; margin-bottom: 5px; padding-top: 5px;
                                            border-top: 1px solid #cacaca65;">
                                            <div class="col-4"><span>` + Col_1 + `</span></div>
                                            <div class="col-4"><span style="white-space: break-spaces;">` + Col_2 + `</span></div>
                                            <div class="col-4"><span>` + Col_3 + `</span></div>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="col-4"><span>` + Col_1_Data + `</span></div>
                                            <div class="col-4"><span style="white-space: break-spaces;">` + Col_2_Data + `</span></div>
                                            <div class="col-4"><span>` + Col_3_Data + `</span></div>
                                        </div>`);

                    l_td.find(".motivo_row").append(Motivo_Row);
                });
            });

            l_td.insertAfter(l_tr);

            return l_td;
        }
        function BuildDatailAnalistaDataTable(l_tr, object) {
            if (l_td != null && l_td != undefined) {
                l_td.remove();
            }

            l_td = $(`<td colspan="12">
                    <div class="row mt-3 mb-3">
                        <div class="col-1"></div>
                        <div class="col-8 motivo_row"></div>
                        <div class="col-2" style="display: flex; align-items: center;">
                            <div>
                                <label style="font-weight: 800; font-size: 10px !important; text-align: center;
                                    display: block;">Aprobación</label>
                                <div class="acciones-datatable-1" style="justify-content: center;">
                                    <button class="bg-btn-success" data-button="aprobar"><i class="fa fa-check"></i></button>
                                    <button class="bg-btn-danger" data-button="rechazar"><i class="fa fa-times"></i></button>                                                 
                                </div>
                            </div>
                        </div>
                        <div class="col-1"></div>
                    </div>
                </td>`);

            Function_ListOrdenesCompraDetail({ "CodiOrcm": object.CodiOrcm }, function (response) {
                $.each(response.Response, function (idx, obj) {
                    var Col_1 = "Motivo " + (idx + 1);
                    var Col_2 = obj.Motivo + " OC";
                    var Col_3 = obj.Motivo + " Nuevo";

                    var Col_1_Data = obj.Motivo;
                    var Col_2_Data = obj.ValorActual;
                    var Col_3_Data = obj.Cambio;

                    var Motivo_Row = $(`<div class="row" style="font-weight: 800; margin-bottom: 5px; padding-top: 5px;
                                            border-top: 1px solid #cacaca65;">
                                            <div class="col-4"><span>` + Col_1 + `</span></div>
                                            <div class="col-4"><span style="white-space: break-spaces;">` + Col_2 + `</span></div>
                                            <div class="col-4"><span style="white-space: break-spaces;">` + Col_3 + `</span></div>
                                        </div>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="col-4"><span>` + Col_1_Data + `</span></div>
                                            <div class="col-4"><span style="white-space: break-spaces;">` + Col_2_Data + `</span></div>
                                            <div class="col-4"><span style="white-space: break-spaces;">` + Col_3_Data + `</span></div>
                                        </div>`);

                    l_td.find(".motivo_row").append(Motivo_Row);
                });
            });

            l_td.insertAfter(l_tr);

            l_td.find("button[data-button=aprobar]").on("click", function () {
                $("body").ModalAlert({
                    "Type": "Confirm",
                    "Color": "Info",
                    "Text": "¿Esta seguro que desea aprobar?",
                    "Confirm": function () {
                        Function_SetAcceptOrdenObservada({ "CodiOrcm": object.CodiOrcm, "Estado": "A" }, function (response) {
                            if (response.Value == "1000") {
                                $("body").ModalFastAlert({
                                    "Color": "Success",
                                    "Text": response.Message,
                                    "Title": "Correcto!"
                                });
                                $("#buscar").trigger("click");
                            } else {
                                $("body").ModalFastAlert({
                                    "MasterColor": "#000000",
                                    "Color": "Danger",
                                    "Text": response.Message,
                                    "Title": "Error"
                                });
                            }
                        });
                        return true;
                    }
                });
            });
            l_td.find("button[data-button=rechazar]").on("click", function () {
                $("body").ModalAlert({
                    "Type": "Confirm",
                    "Color": "Info",
                    "Text": "¿Esta seguro que desea rechazar?",
                    "Confirm": function () {
                        Function_SetAcceptOrdenObservada({ "CodiOrcm": object.CodiOrcm, "Estado": "R" }, function (response) {
                            if (response.Value == "1000") {
                                $("body").ModalFastAlert({
                                    "Color": "Success",
                                    "Text": response.Message,
                                    "Title": "Correcto!"
                                });
                                $("#buscar").trigger("click");
                            } else {
                                $("body").ModalFastAlert({
                                    "MasterColor": "#000000",
                                    "Color": "Danger",
                                    "Text": response.Message,
                                    "Title": "Error"
                                });
                            }
                        });
                        return true;
                    }
                });
            });

            return l_td;
        }
    }

    /* Crear Graficos */
    function Search() {
        EntryModel.CodiFana = $("#analista").val() == undefined ? 0 : $("#analista").val();
        EntryModel.Proceso = $("#proceso").val() == undefined ? "-" : $("#proceso").val();

        /* Grafico Ordenes Vigentes */
        Function_GetGraficoOrdenesVigentes(EntryModel, function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vigente", am4charts.XYChart);

                chart.data = Data;

                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 },
                    { timeUnit: "month", count: 2 },
                    { timeUnit: "month", count: 4 },
                    { timeUnit: "month", count: 6 },
                    { timeUnit: "month", count: 12 },
                    { timeUnit: "month", count: 24 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}";

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });

            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $("#grafico-vigente").closest(".card"));

        /* Grafico Ordenes Vencidas */
        Function_GetGraficoOrdenesVencidas(EntryModel, function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vencido", am4charts.XYChart);

                chart.data = Data;
                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                chart.colors.list = [
                    am4core.color("red"),
                    am4core.color("#FF6F91"),
                    am4core.color("#FF9671"),
                    am4core.color("#FFC75F"),
                    am4core.color("#F9F871")
                ];

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 },
                    { timeUnit: "month", count: 2 },
                    { timeUnit: "month", count: 4 },
                    { timeUnit: "month", count: 6 },
                    { timeUnit: "month", count: 12 },
                    { timeUnit: "month", count: 24 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}";

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });

            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $("#grafico-vencido").closest(".card"));
    }

    (function () {
        /* Inicializar Filtros para el Analista */
        if (Global.ProfileId != Global.ProveedorProfileId) {
            Function_ListAnalista(function (response) {
                var selectAnalista = $("#analista");
                $.each(response.Response.Analista, function (index, object) {
                    var option = $("<option value='" + object.CodiFana + "'>" + object.Nombre + "</option>");
                    selectAnalista.append(option);
                });
                selectAnalista.append("<option value='0'>Todos</option>");
                selectAnalista.val(Global.AnalistaId).trigger("click");

                Function_ListProceso(function (response) {
                    var selectProceso = $("#proceso");
                    $.each(response.Response, function (index, object) {
                        var option = $("<option value='" + object.CodiProc + "'>" + object.Descripcion + "</option>");
                        selectProceso.append(option);
                    });
                    selectProceso.append("<option value='0'>Todos</option>");
                    $(selectProceso.find("option")[0]).trigger("click");
                    //Search();                    
                    $("#buscar").trigger("click");
                });
            });
        } else {
            /* Empezar la pagina con la Busqueda de las opciones */
            //Search();
            $("#buscar").trigger("click");
        }

    })();
});