﻿"use strict";
function Function_GetGraficoOrdenesVigentes(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesObservadas/GetDataGraficoOrdenesVigentes',
        type: 'post',
        dataType: 'json',
        //async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_GetGraficoOrdenesVencidas(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesObservadas/GetDataGraficoOrdenesVencidas',
        type: 'post',
        dataType: 'json',
        //async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ListOrdenesCompra(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesObservadas/List_OrdenesCompra',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_ListOrdenesCompraDetail(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesObservadas/List_OrdenCompraDetail',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ListAnalista(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Helpers/List_Analista',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_ListProceso(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Helpers/List_Proceso',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_SetAcceptOrdenObservada(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/OrdenesObservadas/SetAcceptOrdenesObservadas',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ExportToExcel(p_btn, p_param) {
    $.ajax({
        data: JSON.stringify(p_param),
        url: 'OrdenesObservadas/ExportToExcel',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: function () { }
    });
    function _beforeSend() {
        p_btn.addClass('disabled');
        p_btn.prop('disabled', true);
    }
    function _success(r) {
        if (parseInt(r.status) == 1) {
            var tag_a = document.createElement('a');
            tag_a.href = 'OrdenesObservadas/DownloadExcel?FileName=' + r.response.name + "&DownloadName=OrdenesObservadas.xlsx";
            tag_a.download = 'myExport.xlsx';
            tag_a.click();
            tag_a = null;
        } else {
            $.jGrowl(r.response, {
                header: 'Error',
                type: 'error',
                clipboard: r.exception
            });
        }
    }
    function _complete(xhr, status) {
        p_btn.removeClass('disabled');
        p_btn.prop('disabled', false);
    }
    function _error(xhr, status) {
    }
}