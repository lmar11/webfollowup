﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var Global = GetGlobalVar();

    /* Functions Globales */
    $("#add").on("click", function () {
        var div = $(`<div class="form-group-1">
                        <label>Nombre</label>
                        <input data-input="nombre" style="border: 1px solid silver;" />
                        <input data-input="display" style="border: 1px solid silver; display: none;" />
                        <input data-input="user" style="border: 1px solid silver; display: none;" />
                    </div>
                    <div class="form-group-1">
                        <label>Perfil</label>
                        <select data-select="perfil" style="border: 1px solid silver;">
                            <option value="` + Global.AdministradorProfileId + `">Administrador</option>
                            <option value="` + Global.AnalistaProfileId + `">Analista</option>
                        </select>
                    </div>`);
        div.find("input[data-input=nombre]").autocomplete({
            source: function (request, response) {
                $.ajax({
                    data: { "Termino": request.term },
                    url: "/Perfiles/List_AnalistasAutocompleteMantenimiento",
                    type: "POST",
                    dataType: "json",
                    //data: {
                    //    search: request.term
                    //},
                    success: function (data) {
                        console.log(data);
                        response($.map(data, function (item) {
                            return {
                                label: item.Nombres,
                                value: item.Nombres,
                                result: item.CodiPers,
                                user: item.Username
                            };
                        }));
                    }
                });
            },
            minLength: 5,
            select: function (evnt, ui) {
                div.find("input[data-input=display]").val(ui.item.result);
                div.find("input[data-input=user]").val(ui.item.user);
            }
        });
        $("body").ModalAlertPlugin({
            "Type": "Save",
            "Color": "Info",
            "Content": div,
            "Save": function () {
                var Element = $(this)[0].Element[0];
                //var CodiPers = $(Element).find("input[data-input=display]").val();
                var Username = $(Element).find("input[data-input=user]").val();
                var Perfil = $(Element).find("select[data-select=perfil]").val();
                if (Username == "") {
                    return false;
                } else {
                    var Parametros =
                    {
                        CodiPerf: Perfil,
                        Username: Username,
                        Estado: 1
                    };
                    Function_SetPerfilesMantenimiento(Parametros, function (response) {
                        if (response.Value == "1000") {
                            $("body").ModalFastAlert({
                                "Color": "Success",
                                "Text": response.Message,
                                "Title": "Correcto!"
                            });
                            BuildDataTable();
                        } else {
                            $("body").ModalFastAlert({
                                "MasterColor": "#000000",
                                "Color": "Danger",
                                "Text": response.Message,
                                "Title": "Error"
                            });
                        }
                    });
                    return true;
                }
            }
        });         
    });

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this));
    });

    /* Obtener Listado */
    function BuildDataTable() {
        Function_ListPerfilesMantenimiento(function (response) {
            var table = $("#perfiles-datatable");
            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {
                var l_tr = $(`<tr>
                                <td>` + object.Nombres + `</td>
                                <td>` + object.Username + `</td>
                                <td>` + object.Perfil + `</td>
                                <td>` + (object.Estado == 1 ? "Activo" : "Inactivo") + `</td>
                                <td>
                                    <div class="acciones-datatable-1">
                                        ` + (object.Estado == 0 ?
                                            '<button class="bg-btn-success" data-button="active" title="Habilitar"><i class="fa fa-check"></i></button>'
                                            : '<button class="bg-btn-danger" data-button="inactive" title="Inhabilitar"><i class="fa fa-trash"></i></button>') + `
                                    </div>
                                </td>
                            </tr>`);

                l_tr.find("button[data-button=active]").on("click", function () {
                    $("body").ModalAlert({
                        "Type": "Confirm",
                        "Color": "Info",
                        "Text": "¿Esta seguro que desea habilitar el perfil de este analista?",
                        "Confirm": function () {
                            var Parametros =
                            {
                                CodiPerf: object.CodiPerf,
                                Username: object.Username,
                                Estado: 1
                            };
                            Function_SetPerfilesMantenimiento(Parametros, function (response) {
                                if (response.Value == "1000") {
                                    $("body").ModalFastAlert({
                                        "Color": "Success",
                                        "Text": response.Message,
                                        "Title": "Correcto!"
                                    });
                                    BuildDataTable();
                                } else {
                                    $("body").ModalFastAlert({
                                        "MasterColor": "#000000",
                                        "Color": "Danger",
                                        "Text": response.Message,
                                        "Title": "Error"
                                    });
                                }
                            });

                            return true;
                        }
                    });
                });
                l_tr.find("button[data-button=inactive]").on("click", function () {
                    $("body").ModalAlert({
                        "Type": "Confirm",
                        "Color": "Info",
                        "Text": "¿Esta seguro que desea inhabilitar el perfil de este analista?",
                        "Confirm": function () {
                            var Parametros =
                            {
                                CodiPerf: object.CodiPerf,
                                Username: object.Username,
                                Estado: 0
                            };
                            Function_SetPerfilesMantenimiento(Parametros, function (response) {
                                if (response.Value == "1000") {
                                    $("body").ModalFastAlert({
                                        "Color": "Success",
                                        "Text": response.Message,
                                        "Title": "Correcto!"
                                    });
                                    BuildDataTable();
                                } else {
                                    $("body").ModalFastAlert({
                                        "MasterColor": "#000000",
                                        "Color": "Danger",
                                        "Text": response.Message,
                                        "Title": "Error"
                                    });
                                }
                            });

                            return true;
                        }
                    });
                });
                
                table.find("tbody").append(l_tr);
            });
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[1, "asc"]],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");
        });
    }

    (function () {
        /* Inicializar DataTablet */
        BuildDataTable();
    })();
});