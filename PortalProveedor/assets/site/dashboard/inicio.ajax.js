﻿"use strict";
function Function_GetGraficoOrdenItem(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Dashboard/GetDataGraficoOrdenItem',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_GetGraficoOrdenBarra(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Dashboard/GetDataGraficoOrdenBarra',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_GetGraficoOrdenesVigentes(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Dashboard/GetDataGraficoOrdenesVigentes',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_GetGraficoOrdenesVencidas(p_Parameters, f_Callback, element) {
    var l_Response = null;
    var Waiting = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Dashboard/GetDataGraficoOrdenesVencidas',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        Waiting = WaitingReadyElement(element);
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            Waiting.remove();
        } else {
            //HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_ListAnalista(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Helpers/List_Analista',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_ListProceso(f_Callback) {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Helpers/List_Proceso',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}