﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var EntryModel =
    {
        CodiFana: 0,
        Proceso: null,
        CodigoSapProveedor: null
    };
    var Global = GetGlobalVar();

    /* Functions Globales */
    function DateToFormatDate(date) {
        var Split = date.split('/');
        var FinalDate = Split[2] + "-" + Split[1] + "-" + Split[0];
        return FinalDate;
    }

    /* Funcion de Carga Página */
    var CountFunctionsReady = 0;
    function ValidateLoadPageAsync() {
        if (CountFunctionsReady == 0) {
            HideLoading();
        }
    }

    /* Busqueda del Dashboard */
    $("#buscar").on("click", function () {
        //ShowLoading();
        //CountFunctionsReady = 0;
        Search();
    });
    function Search() {
        EntryModel.CodiFana = $("#analista").val() == undefined ? 0 : $("#analista").val();
        EntryModel.Proceso = $("#proceso").val() == undefined ? "-" : $("#proceso").val();

        /* Grafico Ordenes Item */
        Function_GetGraficoOrdenItem(EntryModel, function (response) {
            $(".graficos-item-section").empty();
            $.each(response.Response.Grafico, function (index, object) {
                var Tipo = "";
                var Color = "";
                switch (object.Tipo) {
                    case "N":
                        Tipo = "ÓRDENES NUEVAS";
                        Color = "bg-btn-light-blue";
                        break;
                    case "P":
                        Tipo = "ÓRDENES POR ENTREGAR";
                        Color = "bg-btn-light-violete";
                        break;
                    case "O":
                        Tipo = "ÓRDENES OBSERVADAS";
                        Color = "bg-btn-light-orange";
                        break;
                }

                var item = $(`  <div class="col-lg-3" style="cursor: pointer;">
                            <div class="card">
                                <div class="card-body box-dash-ordenes" style="padding: 10px;">
                                    <div class="box-title">` + Tipo + `</div>
                                    <div class="box-number">
                                        <span>` + object.Valor + `</span>
                                    </div>
                                    <div class="box-items">
                                        <label class="` + Color + `" style="cursor: pointer;">
                                            NRO. ÍTEMS: ` + object.NumeroItem + `</label>
                                    </div>
                                </div>
                            </div>
                        </div>`);
                $(".graficos-item-section").append(item);

                item.on("click", function () {
                    if (object.Tipo == "N") {
                        g_app.fn_loadAppPage("OrdenesNuevas/List", "Ordenes Nuevas", undefined, undefined, true);
                    } else if (object.Tipo == "O") {
                        g_app.fn_loadAppPage("OrdenesObservadas/List", "Ordenes Observadas", undefined, undefined, true);
                    } else if (object.Tipo == "P") {
                        g_app.fn_loadAppPage("OrdenesPorEntregar/List", "Ordenes Por Entregar", undefined, undefined, true);
                    }
                });
            });
            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $(".graficos-item-section").find(".card"));

        /* Grafico Ordenes Barra */
        Function_GetGraficoOrdenBarra(EntryModel, function (response) {
            var Data = new Array();
            var maxValue1 = 0, maxValue2 = 0, maxValue3 = 0, maxValue = 0;
            $.each(response.Response.Grafico, function (index, object) {
                var Tipo = "";
                switch (object.Tipo) {
                    case "N":
                        Tipo = "Nuevas";
                        break;
                    case "P":
                        Tipo = "Por Entregar";
                        break;
                    case "O":
                        Tipo = "Observadas";
                        break;
                }
                var json_object =
                {
                    category: Tipo,
                    first: parseInt(object.ValorNuevas),
                    second: parseInt(object.ValorObservadas),
                    third: parseInt(object.ValorPorEntregar)
                };
                Data.push(json_object);
                maxValue1 = maxValue1 > parseInt(object.ValorNuevas) ? maxValue1 : parseInt(object.ValorNuevas);
                maxValue2 = maxValue2 > parseInt(object.ValorObservadas) ? maxValue2 : parseInt(object.ValorObservadas);
                maxValue3 = maxValue3 > parseInt(object.ValorPorEntregar) ? maxValue3 : parseInt(object.ValorPorEntregar);
            });

            if (maxValue1 > maxValue2) {
                if (maxValue1 > maxValue3) {
                    maxValue = maxValue1;
                } else {
                    maxValue = maxValue3;
                }
            } else {
                if (maxValue2 > maxValue3) {
                    maxValue = maxValue2;
                } else {
                    maxValue = maxValue3;
                }
            }
            maxValue = maxValue + parseInt(maxValue * 0.25);

            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create('grafico-barra', am4charts.XYChart);
                chart.colors.step = 2;

                chart.legend = new am4charts.Legend();
                chart.legend.position = 'top';
                chart.legend.paddingBottom = 20;
                chart.legend.labels.template.maxWidth = 95;

                chart.colors.list = [
                    am4core.color("#ff0000"),
                    am4core.color("#ffbb6e"),
                    am4core.color("#ffbb6e"),
                    am4core.color("#84e0be")
                ];

                var xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                xAxis.dataFields.category = 'category';
                xAxis.renderer.cellStartLocation = 0.1;
                xAxis.renderer.cellEndLocation = 0.9;
                xAxis.renderer.grid.template.location = 0;

                var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
                yAxis.min = 0;
                yAxis.max = maxValue <= 50 ? 100 : maxValue;
                console.log(maxValue);

                function createSeries(value, name) {
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueY = value;
                    series.dataFields.categoryX = 'category';
                    series.name = name;                    

                    series.events.on("hidden", arrangeColumns);
                    series.events.on("shown", arrangeColumns);
                    series.columns.template.events.on("hit", function (ev) {
                        if (ev.target.dataItem.categoryX == "Nuevas") {
                            g_app.fn_loadAppPage("OrdenesNuevas/List", "Ordenes Nuevas", undefined, undefined, true);
                        } else if (ev.target.dataItem.categoryX == "Observadas") {
                            g_app.fn_loadAppPage("OrdenesObservadas/List", "Ordenes Observadas", undefined, undefined, true);
                        } else if (ev.target.dataItem.categoryX == "Por Entregar") {
                            g_app.fn_loadAppPage("OrdenesPorEntregar/List", "Ordenes Por Entregar", undefined, undefined, true);
                        }                        
                    }, this);

                    var bullet = series.bullets.push(new am4charts.LabelBullet());
                    bullet.interactionsEnabled = true;
                    bullet.dy = -10;
                    bullet.label.text = '{valueY}';
                    //bullet.label.fill = am4core.color('#ffffff');
                    bullet.label.fill = am4core.color('#000000');

                    return series;
                }

                chart.data = Data;

                createSeries('first', "Vencido");
                createSeries('second', "Por Vencer");
                createSeries('third', "Vigente");

                function arrangeColumns() {

                    var series = chart.series.getIndex(0);

                    var w = 1 - xAxis.renderer.cellStartLocation - (1 - xAxis.renderer.cellEndLocation);
                    if (series.dataItems.length > 1) {
                        var x0 = xAxis.getX(series.dataItems.getIndex(0), "categoryX");
                        var x1 = xAxis.getX(series.dataItems.getIndex(1), "categoryX");
                        var delta = ((x1 - x0) / chart.series.length) * w;
                        if (am4core.isNumber(delta)) {
                            var middle = chart.series.length / 2;

                            var newIndex = 0;
                            chart.series.each(function (series) {
                                if (!series.isHidden && !series.isHiding) {
                                    series.dummyData = newIndex;
                                    newIndex++;
                                }
                                else {
                                    series.dummyData = chart.series.indexOf(series);
                                }
                            });
                            var visibleCount = newIndex;
                            var newMiddle = visibleCount / 2;

                            chart.series.each(function (series) {
                                var trueIndex = chart.series.indexOf(series);
                                var newIndex = series.dummyData;

                                var dx = (newIndex - trueIndex + middle - newMiddle) * delta;

                                series.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                                series.bulletsContainer.animate({ property: "dx", to: dx }, series.interpolationDuration, series.interpolationEasing);
                            });
                        }
                    }
                }
            });
            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $("#grafico-barra").closest(".card"));
        
        /* Grafico Ordenes Vigentes */
        Function_GetGraficoOrdenesVigentes(EntryModel, function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vigente", am4charts.XYChart);

                chart.data = Data;

                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 },
                    { timeUnit: "month", count: 2 },
                    { timeUnit: "month", count: 4 },
                    { timeUnit: "month", count: 6 },
                    { timeUnit: "month", count: 12 },
                    { timeUnit: "month", count: 24 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}"; 

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $("#grafico-vigente").closest(".card"));

        /* Grafico Ordenes Vencidas */
        Function_GetGraficoOrdenesVencidas(EntryModel, function (response) {
            var Data = new Array();
            $.each(response.Response.Grafico, function (index, object) {
                var json_object =
                {
                    date: DateToFormatDate(object.Fecha),
                    value: object.Valor
                };
                Data.push(json_object);
            });
            am4core.ready(function () {
                am4core.useTheme(am4themes_animated);
                var chart = am4core.create("grafico-vencido", am4charts.XYChart);

                chart.data = Data;
                chart.paddingRight = 0;
                chart.paddingLeft = -8;
                chart.paddingBottom = -10;
                chart.hiddenState.properties.opacity = 1;

                chart.zoomOutButton.disabled = true;

                chart.colors.list = [
                    am4core.color("red"),
                    am4core.color("#FF6F91"),
                    am4core.color("#FF9671"),
                    am4core.color("#FFC75F"),
                    am4core.color("#F9F871")
                ];

                // Create axes
                var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
                dateAxis.renderer.grid.template.location = 0;
                dateAxis.renderer.minGridDistance = 50;
                dateAxis.gridIntervals.setAll([
                    //{ timeUnit: "day", count: 1 },
                    { timeUnit: "month", count: 1 },
                    { timeUnit: "month", count: 2 },
                    { timeUnit: "month", count: 4 },
                    { timeUnit: "month", count: 6 },
                    { timeUnit: "month", count: 12 },
                    { timeUnit: "month", count: 24 }
                ]);
                dateAxis.groupData = true;
                dateAxis.groupCount = 500;

                var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                // Create series
                var series = chart.series.push(new am4charts.LineSeries());
                series.dataFields.valueY = "value";
                series.dataFields.dateX = "date";
                series.strokeWidth = 3;
                series.fillOpacity = 0.5;
                series.tooltipText = "{valueY}";
                series.tooltip.pointerOrientation = "vertical";

                var bullet = series.bullets.push(new am4charts.CircleBullet());
                bullet.tooltipText = "{valueY}"; 

                // Add vertical scrollbar
                //chart.scrollbarY = new am4core.Scrollbar();
                //chart.scrollbarY.marginLeft = 0;

                // Add cursor
                //chart.cursor = new am4charts.XYCursor();
                //chart.cursor.behavior = "zoomY";
                //chart.cursor.lineX.disabled = true;

            });
            //CountFunctionsReady++;
            //ValidateLoadPageAsync();
        }, $("#grafico-vencido").closest(".card"));

        ValidateLoadPageAsync();
    }
    (function () {
        /* Inicializar Filtros para el Analista */
        if (Global.ProfileId != Global.ProveedorProfileId) {
            Function_ListAnalista(function (response) {
                var selectAnalista = $("#analista");
                $.each(response.Response.Analista, function (index, object) {
                    var option = $("<option value='" + object.CodiFana + "'>" + object.Nombre + "</option>");
                    selectAnalista.append(option);
                });
                selectAnalista.append("<option value='0'>Todos</option>");
                selectAnalista.val(Global.AnalistaId).trigger("click");

                Function_ListProceso(function (response) {
                    var selectProceso = $("#proceso");
                    $.each(response.Response, function (index, object) {
                        var option = $("<option value='" + object.CodiProc + "'>" + object.Descripcion + "</option>");
                        selectProceso.append(option);
                    });
                    selectProceso.append("<option value='0'>Todos</option>");
                    $(selectProceso.find("option")[0]).trigger("click");
                    Search();
                });
            });
        } else {
            /* Empezar la pagina con la Busqueda de las opciones */
            Search();
        }

    })();
});