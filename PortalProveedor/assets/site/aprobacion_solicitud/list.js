﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var EntryFiltroModel =
    {
        CodiFana: 0,
        Estado: null,
        FechaInicio: null,
        FechaFin: null
    };
    var EntrySaveModel =
    {
        Registro: new Array()
    };
    
    var Global = GetGlobalVar();

    /* Functions Globales */
    $("#estado-filtro").on("change", function () {
        var Estado = $(this).val();

        RestartTable();

        if (Estado == "P") {
            $("#guardar").removeClass("d-block-none");
            $(".fecha-filtro-section").addClass("d-block-none");
        } else {
            $("#guardar").addClass("d-block-none");
            $(".fecha-filtro-section").removeClass("d-block-none");
        }
    });
    function RestartTable() {
        var table = $("#aprobacion-solicitud-datatable");
        table.DataTable().clear().destroy();
        table.DataTable({
            "scrollX": true,
            "aaSorting": [[5, "asc"]],
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false },
                { "bSortable": false }
            ],
            "oLanguage": {
                "sLengthMenu": "Mostrar _MENU_ registros por página",
                "sZeroRecords": "No se encontró nada, lo sentimos",
                "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                "sSearch": "Buscar:",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                }
            }
        });
        $(".dataTables_length").addClass("d-block-none");
        $(".dataTables_filter").addClass("d-block-none");
    }

    /* Exportar a Excel */
    $("#ExportToExcel").on("click", function () {
        Function_ExportToExcel($(this), EntryFiltroModel);
    });

    /* Obtener Listado de Solicitudes */
    $("#buscar").on("click", function () {
        var Estado = $("#estado-filtro").val();
        var FechaInicio = $("#fecha-inicio-filtro").val();
        var FechaFin = $("#fecha-fin-filtro").val();

        if (!Estado == "P" && (FechaInicio == "" || FechaFin == "")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Fechas incorrectas",
                "Title": "Error"
            });
        }
        
        EntryFiltroModel.Estado = Estado;
        EntryFiltroModel.FechaInicio = FechaInicio;
        EntryFiltroModel.FechaFin = FechaFin;

        BuildDataTable();
    });
    
    function BuildDataTable() {
        Function_ListAprobacionSolicitudRegistro(EntryFiltroModel, function (response) {
            var table = $("#aprobacion-solicitud-datatable");
            table.DataTable().clear().destroy();
            $.each(response.Response, function (index, object) {
                var l_tr = $(`<tr>
                                <td>` + object.CodigoSapProveedor + `</td>
                                <td>` + object.RazonSocial + `</td>
                                <td>` + object.Nombres + `</td>
                                <td>` + object.Correo + `</td>
                                <td>` + object.Password + `</td>
                                <td>` + object.FechaRegistro + `</td>
                                <td>
                                    <select data-select="rol" class="select-datatable">
                                        <option value="0">--Seleccione--</option>
                                        <option value="1">Mantenimiento</option>
                                        <option value="2">Visualización</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="acciones-datatable-2">
                                        <label data-estado="" data-motivo="" data-ini="` + object.Estado + `"></label>
                                        <button class="bg-btn-light-blue bg-btn-white" data-estado="A">Aprobado</button>
                                        <button class="bg-btn-light-violete bg-btn-white" data-estado="P">Pendiente</button>
                                        <button class="bg-btn-danger bg-btn-white" data-estado="R">Rechazado</button>
                                    </div>
                                </td>
                            </tr>`);

                l_tr.data("Id", object.CodiPert);
                l_tr.find("select[data-select=rol]").val(object.CodiRol);
                l_tr.find("button[data-estado=A], button[data-estado=P], button[data-estado=R]").on("click", function () {
                    var DataEstado = $(this).attr("data-estado");
                    var Rol = l_tr.find("select[data-select=rol]").val();

                    switch (DataEstado) {
                        case "A":
                            var Btn = $(this);
                            if (Rol == 0) {
                                $("body").ModalFastAlert({
                                    "MasterColor": "#000000",
                                    "Color": "Danger",
                                    "Text": "Falta seleccionar un rol.",
                                    "Title": "Error"
                                });
                                return;
                            } else {
                                AccionSelected(Btn);
                            }
                            break;
                        case "P":
                            var Btn = $(this);
                            AccionSelected(Btn);
                            break;
                        case "R":
                            var Btn = $(this);
                            $("body").ModalAlertPlugin({
                                "Type": "Save",
                                "Color": "Danger",
                                "Content": `<div class="form-group-1">
                                                <label>Motivo</label>
                                                <textarea style="border: 1px solid silver;"></textarea>
                                            </div>`,
                                "Save": function () {
                                    var Element = $(this)[0].Element[0];
                                    var Motivo = $(Element).find("textarea").val();
                                    if (Motivo == "") {
                                        return false;
                                    } else {
                                        l_tr.find(".acciones-datatable-2 label").attr("data-motivo", Motivo);
                                        AccionSelected(Btn);
                                        return true;
                                    }                                    
                                }
                            });
                    }
                    
                    
                });
                //l_tr.find("button[data-estado=" + object.Estado + "]").trigger("click");
                AccionSelected(l_tr.find("button[data-estado=" + object.Estado + "]"));
                function AccionSelected(btn) {
                    var DataEstado = btn.attr("data-estado");
                    var Estado = btn.text();
                    
                    switch (DataEstado) {
                        case "A":
                            l_tr.find(".acciones-datatable-2 button").addClass("bg-btn-white");
                            btn.removeClass("bg-btn-white");                            
                            break;
                        case "P":
                            l_tr.find(".acciones-datatable-2 button").addClass("bg-btn-white");
                            btn.removeClass("bg-btn-white");
                            break;
                        case "R":
                            l_tr.find(".acciones-datatable-2 button").addClass("bg-btn-white");
                            btn.removeClass("bg-btn-white");
                            break;
                    }

                    l_tr.find(".acciones-datatable-2 label").attr("data-estado", DataEstado);
                    l_tr.find(".acciones-datatable-2 label").text(Estado);
                }

                table.find("tbody").append(l_tr);
            });
            table.DataTable({
                "scrollX": true,
                "aaSorting": [[5, "asc"]],
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false },
                    { "bSortable": false }
                ],
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "No se encontró nada, lo sentimos",
                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 de 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros totales)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    }
                }
            });
            $(".dataTables_length").addClass("d-block-none");
            $(".dataTables_filter").addClass("d-block-none");
        });
    }

    /* Responder Solicitudes */
    $("#guardar").on("click", function () {
        var filas = $("#aprobacion-solicitud-datatable tbody").find("tr");
        var ArrayRegistro = new Array();
        $.each(filas, function (index, object) {
            var InitialValue = $(object).find(".acciones-datatable-2 label").attr("data-ini");
            var FinalValue = $(object).find(".acciones-datatable-2 label").attr("data-estado");
            var Id = $(object).data("Id");
            var Motivo = $(object).find(".acciones-datatable-2 label").attr("data-motivo");
            var Rol = $(object).find("select[data-select=rol]").val();

            if (InitialValue != FinalValue && FinalValue != "P") {
                var AprobacionRegistroModel =
                {
                    CodiPert: Id,
                    Estado: FinalValue,
                    Motivo: Motivo,
                    CodiRol: Rol
                };
                ArrayRegistro.push(AprobacionRegistroModel);
            }
        });
        
        if (ArrayRegistro.length > 0) {
            EntrySaveModel.Registro = ArrayRegistro;

            $("body").ModalAlert({
                "Type": "Confirm",
                "Color": "Info",
                "Text": "¿Esta seguro que desea guardar?",
                "Confirm": function () {
                    Function_SetAprobacionSolicitudRegistro(EntrySaveModel, function (response) {
                        if (response.Value == "1000") {
                            $("body").ModalFastAlert({
                                "Color": "Success",
                                "Text": response.Message,
                                "Title": "Correcto!"
                            });
                            $("#buscar").trigger("click");
                        } else {
                            $("body").ModalFastAlert({
                                "MasterColor": "#000000",
                                "Color": "Danger",
                                "Text": response.Message,
                                "Title": "Error"
                            });
                            HideLoading();
                        }
                    });
                    return true;
                }
            });            
        }
    });

    (function () {
        /* Inicializar Datepicker */
        $("#fecha-inicio-filtro").datetimepicker({
            format: 'DD/MM/YYYY',
            date: new Date()
        });
        $("#fecha-fin-filtro").datetimepicker({
            format: 'DD/MM/YYYY',
            date: new Date()
        });

        /* Inicializar Filtros para el Analista */
        $("#buscar").trigger("click");

    })();
});