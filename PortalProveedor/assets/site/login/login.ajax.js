﻿"use strict";

function Function_ListAnalistas() {
    var l_Response = null;
    $.ajax({
        data: null,
        url: '/Login/ListAnalistas',
        type: 'post',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        //g_app.fn_showLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            //g_app.fn_hideLoading();
        }
    }
    function _error(xhr, status) { }
    return l_Response;
}

function Function_ValidateRuc(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Login/ValidateRuc',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_ValidateEmail(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Login/ValidateEmail',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}

function Function_LogInValidate(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Account/GetLoginValidate',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        console.log(r);
        if (r.Status == 1 || r.Status == 0) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined' && l_Response != null) {
            f_Callback(l_Response);
            //HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) { }
}
function Function_LogIn(p_Parameters) {
    var param = new FormData(),
        go = false;

    param["ProfileId"] = p_Parameters.Id;

    $.ajax({
        data: JSON.stringify(param),
        url: '/Account/Login',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.status === 1) {
            go = true;
        }
    }
    function _complete() {
        if (go) {
            sessionStorage.setItem('window-profile', param.ProfileId);
            window.location = '/';
        } else {
            HideLoading();
        }
    }
}
function Function_SignUp(p_Parameters, f_Callback) {
    var l_Response = null;
    $.ajax({
        data: JSON.stringify(p_Parameters),
        url: '/Login/SignUp',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete,
        error: _error
    });
    function _beforeSend() {
        ShowLoading();
    }
    function _success(r) {
        if (r.Status == 1) {
            l_Response = r;
        } else {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": r.Exception,
                "Title": "Error"
            });
        }
    }
    function _complete(xhr, status) {
        if (typeof f_Callback != 'undefined') {
            f_Callback(l_Response);
            HideLoading();
        } else {
            HideLoading();
        }
    }
    function _error(xhr, status) {}
}