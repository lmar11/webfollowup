﻿"use strict";
$(document).ready(function () {
    /* Parametros Globales */
    var ModelParametro =
    {
        Ruc: null,
        RazonSocial: null,
        Analista: null,
        Password: null,
        Solicitante: null,
        ContactoFacturacion: null,
        ContactoGerencia: null,
        ContactoTecnico: null
    };    
    var ModelParametroSolicitante =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ModelParametroContactoGerencia =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ModelParametroContactoFacturacion =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ModelParametroContactoTecnico =
    {
        Nombre: null,
        ApellidoPaterno: null,
        ApellidoMaterno: null,
        Cargo: null,
        Correo: null,
        Celular: null
    };
    var ModelLogIn =
    {
        Correo: null,
        Password: null,
        Type: null
    };
    var PassRuc = false;
    $(document).on("keyup", function (event) { console.log(event.which); });   

    /* Acceso */
    $(".acceso-button").on("click", function () {
        $(".acceso-button").removeClass("acceso-button-selected");
        $(this).addClass("acceso-button-selected");
        $("#next-acceso").removeClass("next-button-disabled");
    });
    $("#next-acceso").on("click", function () {
        if (!$("#next-acceso").hasClass("next-button-disabled")) {
            var selected = $(".acceso-button-selected").attr("data-id");
            if (selected == "P") {
                $(".acceso").addClass("d-block-none");
                $(".inicio-session-proveedor").removeClass("d-block-none");
            } else if (selected == "A") {
                $(".acceso").addClass("d-block-none");
                $(".inicio-session-analista").removeClass("d-block-none");
            }
        }
    });

    /* Inicio Sesión Proveedor */
    $("#return-inicio-session-proveedor").on("click", function () {
        $(".acceso").removeClass("d-block-none");
        $(".inicio-session-proveedor").addClass("d-block-none");
    });
    $("#correo-proveedor-login, #password-proveedor-login").on("keyup", function (event) {
        var correo = $("#correo-proveedor-login").val().trim();
        var password = $("#password-proveedor-login").val().trim();
                
        if (correo == "" || password == "") {
            $("#proveedor-login").addClass("next-button-disabled");               
            return;
        }
        if (!correo.includes("@") || !correo.includes(".")) {           
            $("#proveedor-login").addClass("next-button-disabled");
            return;
        }

        $("#proveedor-login").removeClass("next-button-disabled");

        if (event.which == 13) {
            $("#proveedor-login").trigger("click");
        }
    });
    $("#proveedor-login").on("click", function () {
        ModelLogIn.Correo = $("#correo-proveedor-login").val().trim();
        ModelLogIn.Password = $("#password-proveedor-login").val().trim();
        ModelLogIn.Type = "P";

        if (!ModelLogIn.Correo.includes("@") || !ModelLogIn.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return;
        }

        GetLoginProcess(ModelLogIn);
    });
    $("#sign-up").on("click", function () {        
        ModelParametro.Solicitante = null;
        $(".inicio-session-proveedor").addClass("d-block-none");
        $(".crear-cuenta-1").removeClass("d-block-none");
    });   
    function GetLoginProcess(ModelLogIn) {
        Function_LogInValidate(ModelLogIn, function (response) {
            if (response.Value == "1000") {
                var perfiles = response.Response.Profile;
                if (perfiles.length == 1) {
                    $.each(perfiles, function (index, object) {
                        Function_LogIn({ "Id": object.CodiPerf });
                    });                    
                } else {
                    $("#perfil-proveedor-login").empty();
                    $("#perfil-analista-login").empty();
                    HideLoading();
                    $.each(perfiles, function (index, object) {
                        var box_perfil = $("<button>" + object.Descripcion + "</button>");
                        if (ModelLogIn.Type == "P") {
                            $("#perfil-proveedor-login").append(box_perfil);
                        } else if (ModelLogIn.Type == "A") {
                            $("#perfil-analista-login").append(box_perfil);
                        }
                        box_perfil.on("click", function (event) {
                            event.preventDefault();
                            Function_LogIn({ "Id": object.CodiPerf });
                        });
                    }); 
                }
            } else if (response.Value == "3000") {
                $("body").ModalFastAlert({
                    "MasterColor": "#000000",
                    "Color": "Danger",
                    "Text": response.Message,
                    "Title": "Error"
                });
                HideLoading();
            }
        });
    }
    (function () {
        setInterval(function () {
            $.ajax({
                data: null,
                url: 'Account/KeepActive',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8"
            });
        }, 300000);//active session every 5 minutes
    }());

    /* Inicio Sesión Analista */
    $("#return-inicio-session-analista").on("click", function () {
        $(".acceso").removeClass("d-block-none");
        $(".inicio-session-analista").addClass("d-block-none");
    });
    $("#correo-analista-login, #password-analista-login").on("keyup", function () {
        var correo = $("#correo-analista-login").val().trim();
        var password = $("#password-analista-login").val().trim();

        if (correo == "" || password == "") {
            $("#analista-login").addClass("next-button-disabled");
            return;
        }
        if (!correo.includes("@") || !correo.includes(".")) {
            $("#proveedor-login").addClass("next-button-disabled");
            return;
        }

        $("#analista-login").removeClass("next-button-disabled");

        if (event.which == 13) {
            $("#analista-login").trigger("click");
        }
    });
    $("#analista-login").on("click", function () {
        ModelLogIn.Correo = $("#correo-analista-login").val().trim();
        ModelLogIn.Password = $("#password-analista-login").val().trim();
        ModelLogIn.Type = "A";

        if (!ModelLogIn.Correo.includes("@") || !ModelLogIn.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return;
        }

        GetLoginProcess(ModelLogIn);
    });

    /* Crear Cuenta 1 */
    $("#return-crear-cuenta-1").on("click", function () {
        $(".inicio-session-proveedor").removeClass("d-block-none");
        $(".crear-cuenta-1").addClass("d-block-none");
    });
    $("#ruc-login, #razon-social-login, #nombre-login, #ap-paterno-login, #ap-materno-login," +
        "#cargo-login, #correo-login, #celular-login, #password-1-login, #password-2-login").on("keyup", function () {

            var ruc = $("#ruc-login").val().trim();
            var razon_social = $("#razon-social-login").val().trim();
            var nombre = $("#nombre-login").val().trim();
            var ap_paterno = $("#ap-paterno-login").val().trim();
            var ap_materno = $("#ap-materno-login").val().trim();
            var cargo = $("#cargo-login").val().trim();
            var correo = $("#correo-login").val().trim();
            var celular = $("#celular-login").val().trim();
            var password_1 = $("#password-1-login").val().trim();
            var password_2 = $("#password-2-login").val().trim();

            if (ruc.length != 11) {
                PassRuc = false;
                $("#razon-social-login").val("");
            }    
            
            if (ruc == "" || razon_social == "" || nombre == "" || ap_paterno == "" || ap_materno == "" ||
                cargo == "" || correo == "" || celular == "" || password_1 == "" || password_2 == "") {
                ModelParametro.Solicitante = null;
                $("#crear-cuenta-1-login").addClass("next-button-disabled");
                return;
            }           

            ModelParametro.Solicitante = ModelParametroSolicitante;

            ModelParametro.Ruc = ruc;
            ModelParametro.RazonSocial = razon_social;
            ModelParametro.Password = password_1;

            ModelParametro.Solicitante.Nombre = nombre;
            ModelParametro.Solicitante.ApellidoPaterno = ap_paterno;
            ModelParametro.Solicitante.ApellidoMaterno = ap_materno;
            ModelParametro.Solicitante.Cargo = cargo;
            ModelParametro.Solicitante.Correo = correo;
            ModelParametro.Solicitante.Celular = celular;

            $("#crear-cuenta-1-login").removeClass("next-button-disabled");

            if (event.which == 13) {
                $("#proveedor-login").trigger("click");
            }
        });    
    $("#ruc-login").on("keyup", function (event) {
        var ruc = $("#ruc-login").val().trim();
        var len = $("#ruc-login").val().trim().length;

        if (event.which == 13 && len == 11) {
            Function_ValidateRuc({ "Ruc": ruc }, function (response) {
                if (response.Value == "1000") {
                    PassRuc = true;
                    $("#razon-social-login").val(response.Response.RazonSocial);
                    $("#nombre-login").prop("disabled", false);
                    $("#ap-paterno-login").prop("disabled", false);
                    $("#ap-materno-login").prop("disabled", false);
                    $("#cargo-login").prop("disabled", false);
                    $("#correo-login").prop("disabled", false);
                    $("#celular-login").prop("disabled", false);
                    $("#password-1-login").prop("disabled", false);
                    $("#password-2-login").prop("disabled", false);

                    $.each(response.Response.Contacto, function (index, object) {
                        switch (object.Tipo) {
                            case "GV":
                                $("#nombre-gerencia-login").val(object.Nombre);
                                $("#ap-paterno-gerencia-login").val(object.ApellidoPaterno);
                                $("#ap-materno-gerencia-login").val(object.ApellidoMaterno);
                                $("#cargo-gerencia-login").val(object.Cargo);
                                $("#correo-gerencia-login").val(object.Correo);
                                $("#celular-gerencia-login").val(object.Celular);

                                $("#nombre-gerencia-login").prop("disabled", true);
                                $("#ap-paterno-gerencia-login").prop("disabled", true);
                                $("#ap-materno-gerencia-login").prop("disabled", true);
                                $("#cargo-gerencia-login").prop("disabled", true);
                                $("#correo-gerencia-login").prop("disabled", true);
                                $("#celular-gerencia-login").prop("disabled", true);

                                var e = jQuery.Event('keyup', { which: $.ui.keyCode.ENTER });
                                $("#nombre-gerencia-login").trigger(e);
                                break;
                            case "CF":
                                $("#nombre-facturacion-login").val(object.Nombre);
                                $("#ap-paterno-facturacion-login").val(object.ApellidoPaterno);
                                $("#ap-materno-facturacion-login").val(object.ApellidoMaterno);
                                $("#cargo-facturacion-login").val(object.Cargo);
                                $("#correo-facturacion-login").val(object.Correo);
                                $("#celular-facturacion-login").val(object.Celular);

                                $("#nombre-facturacion-login").prop("disabled", true);
                                $("#ap-paterno-facturacion-login").prop("disabled", true);
                                $("#ap-materno-facturacion-login").prop("disabled", true);
                                $("#cargo-facturacion-login").prop("disabled", true);
                                $("#correo-facturacion-login").prop("disabled", true);
                                $("#celular-facturacion-login").prop("disabled", true);

                                var e = jQuery.Event('keyup', { which: $.ui.keyCode.ENTER });
                                $("#nombre-facturacion-login").trigger(e);
                                break;
                            case "CT":
                                $("#nombre-tecnico-login").val(object.Nombre);
                                $("#ap-paterno-tecnico-login").val(object.ApellidoPaterno);
                                $("#ap-materno-tecnico-login").val(object.ApellidoMaterno);
                                $("#cargo-tecnico-login").val(object.Cargo);
                                $("#correo-tecnico-login").val(object.Correo);
                                $("#celular-tecnico-login").val(object.Celular);

                                $("#nombre-tecnico-login").prop("disabled", true);
                                $("#ap-paterno-tecnico-login").prop("disabled", true);
                                $("#ap-materno-tecnico-login").prop("disabled", true);
                                $("#cargo-tecnico-login").prop("disabled", true);
                                $("#correo-tecnico-login").prop("disabled", true);
                                $("#celular-tecnico-login").prop("disabled", true);

                                var e = jQuery.Event('keyup', { which: $.ui.keyCode.ENTER });
                                $("#nombre-tecnico-login").trigger(e);
                                break;
                        }
                    });                    
                } else if (response.Value == "3000") {
                    $("body").ModalFastAlert({
                        "MasterColor": "#000000",
                        "Color": "Danger",
                        "Text": response.Message,
                        "Title": "Error"
                    });
                    PassRuc = false;

                    $("#nombre-gerencia-login").val("");
                    $("#ap-paterno-gerencia-login").val("");
                    $("#ap-materno-gerencia-login").val("");
                    $("#cargo-gerencia-login").val("");
                    $("#correo-gerencia-login").val("");
                    $("#celular-gerencia-login").val("");

                    $("#nombre-gerencia-login").prop("disabled", false);
                    $("#ap-paterno-gerencia-login").prop("disabled", false);
                    $("#ap-materno-gerencia-login").prop("disabled", false);
                    $("#cargo-gerencia-login").prop("disabled", false);
                    $("#correo-gerencia-login").prop("disabled", false);
                    $("#celular-gerencia-login").prop("disabled", false);

                    var a = jQuery.Event('keyup', { which: $.ui.keyCode.ENTER });
                    $("#nombre-gerencia-login").trigger(a);

                    $("#nombre-facturacion-login").val("");
                    $("#ap-paterno-facturacion-login").val("");
                    $("#ap-materno-facturacion-login").val("");
                    $("#cargo-facturacion-login").val("");
                    $("#correo-facturacion-login").val("");
                    $("#celular-facturacion-login").val("");

                    $("#nombre-facturacion-login").prop("disabled", false);
                    $("#ap-paterno-facturacion-login").prop("disabled", false);
                    $("#ap-materno-facturacion-login").prop("disabled", false);
                    $("#cargo-facturacion-login").prop("disabled", false);
                    $("#correo-facturacion-login").prop("disabled", false);
                    $("#celular-facturacion-login").prop("disabled", false);

                    var e = jQuery.Event('keyup', { which: $.ui.keyCode.ENTER });
                    $("#nombre-facturacion-login").trigger(e);

                    $("#nombre-tecnico-login").val("");
                    $("#ap-paterno-tecnico-login").val("");
                    $("#ap-materno-tecnico-login").val("");
                    $("#cargo-tecnico-login").val("");
                    $("#correo-tecnico-login").val("");
                    $("#celular-tecnico-login").val("");

                    $("#nombre-tecnico-login").prop("disabled", false);
                    $("#ap-paterno-tecnico-login").prop("disabled", false);
                    $("#ap-materno-tecnico-login").prop("disabled", false);
                    $("#cargo-tecnico-login").prop("disabled", false);
                    $("#correo-tecnico-login").prop("disabled", false);
                    $("#celular-tecnico-login").prop("disabled", false);

                    var i = jQuery.Event('keyup', { which: $.ui.keyCode.ENTER });
                    $("#nombre-tecnico-login").trigger(i);
                }
            });
        }
    });
    $("#crear-cuenta-1-login").on("click", function () {
        if ($(this).hasClass("next-button-disabled")) {
            return;
        }

        var password_1 = $("#password-1-login").val().trim();
        var password_2 = $("#password-2-login").val().trim();
        if (ModelParametro.Solicitante == null) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos.",
                "Title": "Error"
            });
            return;
        }        
        if (!ModelParametro.Solicitante.Correo.includes("@") || !ModelParametro.Solicitante.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return;
        }       
        if (ModelParametro.Solicitante.Celular.length < 9) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El celular no tiene los digitos necesarios.",
                "Title": "Error"
            });
            return;
        }
        if (password_1 != password_2) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Las contraseñas no coinciden.",
                "Title": "Error"
            });
            return;
        }
        if ($("#ruc-login").val().trim().length != 11) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Ingrese un Ruc válido.",
                "Title": "Error"
            });
            return;
        }
        if (!PassRuc) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El Ruc ingresado no se encuentra en SAP.",
                "Title": "Error"
            });
            return;
        }

        Function_ValidateEmail({ "Correo": ModelParametro.Solicitante.Correo }, function (response) {
            if (response.Value == "1000") {
                $(".crear-cuenta-1").addClass("d-block-none");
                $(".crear-cuenta-2").removeClass("d-block-none");
            } else if (response.Value == "1500") {
                $("body").ModalAlert({
                    "Color": "Info",
                    "Icon": "fa fa-info",
                    "Text": response.Message
                });
            } else if (response.Value == "2000") {
                $("body").ModalAlert({
                    "Color": "Info",
                    "Icon": "fa fa-info",
                    "Text": response.Message
                });
            } else if (response.Value == "3000") {
                $("body").ModalFastAlert({
                    "MasterColor": "#000000",
                    "Color": "Danger",
                    "Text": response.Message,
                    "Title": "Error"
                });
            }
        });
    });
    (function () {
        jQuerySize($("#ruc-login"), 11);
        jQueryNumeric($("#ruc-login"));
        jQueryLetter($("#nombre-login"));
        jQueryLetter($("#ap-paterno-login"));
        jQueryLetter($("#ap-materno-login"));
        jQueryLetterNumeric($("#cargo-login"));
        jQuerySize($("#celular-login"), 12);
        jQueryPhoneNumeric($("#celular-login"));
    })();

    /* Crear Cuenta 2 */
    $("#return-crear-cuenta-2").on("click", function () {
        $(".crear-cuenta-1").removeClass("d-block-none");
        $(".crear-cuenta-2").addClass("d-block-none");
    });
    $("#nombre-gerencia-login, #ap-paterno-gerencia-login, #ap-materno-gerencia-login, #cargo-gerencia-login," +
        "#correo-gerencia-login, #celular-gerencia-login, #nombre-facturacion-login, #ap-paterno-facturacion-login," +
        "#ap-materno-facturacion-login, #cargo-facturacion-login, #correo-facturacion-login," +
        "#celular-facturacion-login").on("keyup", function () {
            var nombre_gerencia = $("#nombre-gerencia-login").val().trim();
            var ap_paterno_gerencia = $("#ap-paterno-gerencia-login").val().trim();
            var ap_materno_gerencia = $("#ap-materno-gerencia-login").val().trim();
            var cargo_gerencia = $("#cargo-gerencia-login").val().trim();
            var correo_gerencia = $("#correo-gerencia-login").val().trim();
            var celular_gerencia = $("#celular-gerencia-login").val().trim();
            var nombre_facturacion = $("#nombre-facturacion-login").val().trim();
            var ap_paterno_facturacion = $("#ap-paterno-facturacion-login").val().trim();
            var ap_materno_facturacion = $("#ap-materno-facturacion-login").val().trim();
            var cargo_facturacion = $("#cargo-facturacion-login").val().trim();
            var correo_facturacion = $("#correo-facturacion-login").val().trim();
            var celular_facturacion = $("#celular-facturacion-login").val().trim();

            if (nombre_gerencia == "" || ap_paterno_gerencia == "" || ap_materno_gerencia == "" || cargo_gerencia == "" ||
                correo_gerencia == "" || celular_gerencia == "" || nombre_facturacion == "" || ap_paterno_facturacion == "" ||
                ap_materno_facturacion == "" || cargo_facturacion == "" || correo_facturacion == "" ||
                celular_facturacion == "") {
                ModelParametro.ContactoFacturacion = null;
                ModelParametro.ContactoGerencia = null;
                $("#crear-cuenta-2-login").addClass("next-button-disabled");
                return;
            }

            ModelParametro.ContactoGerencia = ModelParametroContactoGerencia;
            ModelParametro.ContactoFacturacion = ModelParametroContactoFacturacion;

            ModelParametro.ContactoGerencia.Nombre = nombre_gerencia;
            ModelParametro.ContactoGerencia.ApellidoPaterno = ap_paterno_gerencia;
            ModelParametro.ContactoGerencia.ApellidoMaterno = ap_materno_gerencia;
            ModelParametro.ContactoGerencia.Cargo = cargo_gerencia;
            ModelParametro.ContactoGerencia.Correo = correo_gerencia;
            ModelParametro.ContactoGerencia.Celular = celular_gerencia;

            ModelParametro.ContactoFacturacion.Nombre = nombre_facturacion;
            ModelParametro.ContactoFacturacion.ApellidoPaterno = ap_paterno_facturacion;
            ModelParametro.ContactoFacturacion.ApellidoMaterno = ap_materno_facturacion;
            ModelParametro.ContactoFacturacion.Cargo = cargo_facturacion;
            ModelParametro.ContactoFacturacion.Correo = correo_facturacion;
            ModelParametro.ContactoFacturacion.Celular = celular_facturacion;

            $("#crear-cuenta-2-login").removeClass("next-button-disabled");

            if (event.which == 13) {
                $("#proveedor-login").trigger("click");
            }
        });
    $("#crear-cuenta-2-login").on("click", function () {
        if ($(this).hasClass("next-button-disabled")) {
            return;
        }

        if (!ModelParametro.ContactoGerencia.Correo.includes("@") || !ModelParametro.ContactoGerencia.Correo.includes(".") ||
            !ModelParametro.ContactoFacturacion.Correo.includes("@") || !ModelParametro.ContactoFacturacion.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return;
        }
        if (ModelParametro.ContactoGerencia.Celular.length < 9 || ModelParametro.ContactoFacturacion.Celular.length < 9) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El celular no tiene los digitos necesarios.",
                "Title": "Error"
            });
            return;
        }

        //ModelParametro.ContactoTecnico = null;

        $(".crear-cuenta-2").addClass("d-block-none");
        $(".crear-cuenta-3").removeClass("d-block-none");
    });
    (function () {
        jQueryLetter($("#nombre-gerencia-login"));
        jQueryLetter($("#ap-paterno-gerencia-login"));
        jQueryLetter($("#ap-materno-gerencia-login"));
        jQueryLetterNumeric($("#cargo-gerencia-login"));
        jQuerySize($("#celular-gerencia-login"), 12);
        jQueryPhoneNumeric($("#celular-gerencia-login"));
        
        jQueryLetter($("#nombre-facturacion-login"));
        jQueryLetter($("#ap-paterno-facturacion-login"));
        jQueryLetter($("#ap-materno-facturacion-login"));
        jQueryLetterNumeric($("#cargo-facturacion-login"));
        jQuerySize($("#celular-facturacion-login"), 12);
        jQueryPhoneNumeric($("#celular-facturacion-login"));
    })();

    /* Crear Cuenta 3 */
    $("#return-crear-cuenta-3").on("click", function () {
        $(".crear-cuenta-2").removeClass("d-block-none");
        $(".crear-cuenta-3").addClass("d-block-none");
    });
    $("#nombre-tecnico-login, #ap-paterno-tecnico-login, #ap-materno-tecnico-login, #cargo-tecnico-login," +
        "#correo-tecnico-login, #celular-tecnico-login").on("keyup", function () {
            var nombre_tecnico = $("#nombre-tecnico-login").val().trim();
            var ap_paterno_tecnico = $("#ap-paterno-tecnico-login").val().trim();
            var ap_materno_tecnico = $("#ap-materno-tecnico-login").val().trim();
            var cargo_tecnico = $("#cargo-tecnico-login").val().trim();
            var correo_tecnico = $("#correo-tecnico-login").val().trim();
            var celular_tecnico = $("#celular-tecnico-login").val().trim();            

            if (nombre_tecnico == "" || ap_paterno_tecnico == "" || ap_materno_tecnico == "" || cargo_tecnico == "" ||
                correo_tecnico == "" || celular_tecnico == "") {
                ModelParametro.ContactoTecnico = null;
                $("#crear-cuenta-3-login").addClass("next-button-disabled");
                return;
            }

            ModelParametro.ContactoTecnico = ModelParametroContactoTecnico;

            ModelParametro.ContactoTecnico.Nombre = nombre_tecnico;
            ModelParametro.ContactoTecnico.ApellidoPaterno = ap_paterno_tecnico;
            ModelParametro.ContactoTecnico.ApellidoMaterno = ap_materno_tecnico;
            ModelParametro.ContactoTecnico.Cargo = cargo_tecnico;
            ModelParametro.ContactoTecnico.Correo = correo_tecnico;
            ModelParametro.ContactoTecnico.Celular = celular_tecnico;

            $("#crear-cuenta-3-login").removeClass("next-button-disabled");

            if (event.which == 13) {
                $("#proveedor-login").trigger("click");
            }
        });
    $("#analista-select-login").on("change", function () {
        var analista = $("#analista-select-login").val().trim();
        ModelParametro.Analista = analista;
        if (ModelParametro.ContactoTecnico != null) {
            $("#crear-cuenta-3-login").removeClass("next-button-disabled");
        }
    });
    $("#crear-cuenta-3-login").on("click", function () {
        var isChecked = $("#terminos-condiciones").is(":checked");        
        var isCheckedConfirmacion = $("#confirmacion").is(":checked");  
        
        if (!ModelParametro.ContactoTecnico.Correo.includes("@") || !ModelParametro.ContactoTecnico.Correo.includes(".")) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El correo no tiene el formato correcto.",
                "Title": "Error"
            });
            return;
        }
        if (ModelParametro.ContactoTecnico.Celular.length < 9) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El celular no tiene los digitos necesarios.",
                "Title": "Error"
            });
            return;
        }
        if (ModelParametro.Analista == null || ModelParametro.Analista == "0") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Seleccione un analista.",
                "Title": "Error"
            });
            return;
        }
        if (!isChecked) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Se require aceptar los Términos y Condiciones.",
                "Title": "Error"
            });
            return;
        }
        if (!isCheckedConfirmacion) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Se require aceptar la Confirmación de Veracidad de Información.",
                "Title": "Error"
            });
            return;
        }

        if (!SignUpValidation()) {
            return;
        }

        /* Enviar Data */
        Function_SignUp(ModelParametro, function (response) {
            if (response.Value == "1000") {
                $(".crear-cuenta-3").addClass("d-block-none");
                $(".acceso").removeClass("d-block-none");
                location.reload();
            } else if (response.Value == "3000") {
                $("body").ModalFastAlert({
                    "MasterColor": "#000000",
                    "Color": "Danger",
                    "Text": response.Message,
                    "Title": "Error"
                });
            }            
        });
    });
    function SignUpValidation() {
        console.log(ModelParametro);
        /* Validacion General */
        if (ModelParametro.Ruc == null ||
            ModelParametro.RazonSocial == null ||
            ModelParametro.Analista == null ||
            ModelParametro.Analista == 0 ||
            ModelParametro.Password == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar campos",
                "Title": "Error"
            });
            return false;
        }
        if (!PassRuc) {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "El Ruc ingresado no se encuentra en SAP.",
                "Title": "Error"
            });
            return false;
        }

        /* Validacion Solicitante */
        if (ModelParametro.Solicitante.Nombre == "" ||
            ModelParametro.Solicitante.ApellidoPaterno == "" ||
            ModelParametro.Solicitante.ApellidoMaterno == "" ||
            ModelParametro.Solicitante.Cargo == "" ||
            ModelParametro.Solicitante.Correo == "" ||
            ModelParametro.Solicitante.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Solicitante.",
                "Title": "Error"
            });
            return false;
        }

        /* Validacion Contacto Gerencia */
        if (ModelParametro.ContactoGerencia.Nombre == "" ||
            ModelParametro.ContactoGerencia.ApellidoPaterno == "" ||
            ModelParametro.ContactoGerencia.ApellidoMaterno == "" ||
            ModelParametro.ContactoGerencia.Cargo == "" ||
            ModelParametro.ContactoGerencia.Correo == "" ||
            ModelParametro.ContactoGerencia.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Contacto de Gerencia.",
                "Title": "Error"
            });
            return false;
        }

        /* Validacion Contacto Facturacion */
        if (ModelParametro.ContactoFacturacion.Nombre == "" ||
            ModelParametro.ContactoFacturacion.ApellidoPaterno == "" ||
            ModelParametro.ContactoFacturacion.ApellidoMaterno == "" ||
            ModelParametro.ContactoFacturacion.Cargo == "" ||
            ModelParametro.ContactoFacturacion.Correo == "" ||
            ModelParametro.ContactoFacturacion.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Contacto de Facturación.",
                "Title": "Error"
            });
            return false;
        }

        /* Validacion Contacto Tecnico */
        if (ModelParametro.ContactoTecnico.Nombre == "" ||
            ModelParametro.ContactoTecnico.ApellidoPaterno == "" ||
            ModelParametro.ContactoTecnico.ApellidoMaterno == "" ||
            ModelParametro.ContactoTecnico.Cargo == "" ||
            ModelParametro.ContactoTecnico.Correo == "" ||
            ModelParametro.ContactoTecnico.Celular == "") {
            $("body").ModalFastAlert({
                "MasterColor": "#000000",
                "Color": "Danger",
                "Text": "Falta completar los campos del Contacto Técnico.",
                "Title": "Error"
            });
            return false;
        }

        return true;
    }
    (function () {
        jQueryLetter($("#nombre-tecnico-login"));
        jQueryLetter($("#ap-paterno-tecnico-login"));
        jQueryLetter($("#ap-materno-tecnico-login"));
        jQueryLetterNumeric($("#cargo-tecnico-login"));
        jQuerySize($("#celular-tecnico-login"), 12);
        jQueryPhoneNumeric($("#celular-tecnico-login"));
    })();

    /* Cargar Elementos */
    (function () {
        var select = $("#analista-select-login");
        var analistasResponse = Function_ListAnalistas();
        select.append("<option value='0'>-- Seleccione --</option>");
        $.each(analistasResponse.Response.Analista, function (index, object) {
            var option = $("<option value='" + object.CodiFana + "'>" + object.Nombre + "</option>");
            select.append(option);
        });
        HideLoading();
    })();

});