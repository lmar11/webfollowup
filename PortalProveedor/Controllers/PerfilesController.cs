﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using PortalProveedor.Filters;
using PortalProveedor.Models;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class PerfilesController : Controller
    {
        [HttpPost]
        public ActionResult List()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            ViewBag.Rol = SessionHelpers.GetRolId();
            ViewBag.ProfileId = SessionHelpers.GetProfileId();
            ViewBag.AnalistaId = SessionHelpers.GetAnalistaId();
            ViewBag.AnalistaProfileId = Helpers.ANALISTA_PROFILE_ID;
            ViewBag.AdministadorProfileId = Helpers.ADMINISTRADOR_PROFILE_ID;
            ViewBag.ProveedorProfileId = Helpers.PROVEEDOR_PROFILE_ID;
            return View("List");
        }
        [HttpPost]
        public JsonResult List_PerfilesMantenimiento(Entry_PerfilesMantenimiento entry)
        {
            entry.AppId = SessionHelpers.GetAppId();

            var my_json_data = entry;
            string url_api = "/api/Crud/CrudListAccesoSistema";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_PerfilesMantenimiento>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult List_AnalistasAutocompleteMantenimiento(Entry_AnalistasAutocompleteMantenimiento entry)
        {
            var my_json_data = entry;
            string url_api = "/api/Service/ListTrabajadores";

            var json_data = Helpers.ExecuteApiFollowUp(entry, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_AnalistasAutocompleteMantenimiento>>(json_data.Response.ToString());
            }

            return Json(json_data.Response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SetPerfilesMantenimiento(Entry_PerfilesMantenimiento entry)
        {
            entry.AppId = SessionHelpers.GetAppId();

            var my_json_data = entry;
            string url_api = "/api/Crud/CrudInsertUpdateAccesoSistema";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ExportToExcel(Entry_PerfilesMantenimiento entry)
        {
            XLWorkbook l_wb = null;
            IXLWorksheet l_ws = null;

            string l_name = "Excel-" + SessionHelpers.GetUsername() + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            string l_path = Path.Combine(Server.MapPath("~/temp/"), l_name);

            try
            {
                l_wb = new XLWorkbook();

                entry.AppId = SessionHelpers.GetAppId();

                var my_json_data = entry;
                string url_api = "/api/Crud/CrudListAccesoSistema";

                var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
                var Response = JsonConvert.DeserializeObject<List<Response_PerfilesMantenimiento>>(json_data.Response.ToString());

                DataTable Dt_Export = new DataTable();
                Dt_Export.Columns.Add("NOMBRES");
                Dt_Export.Columns.Add("USUARIO");
                Dt_Export.Columns.Add("PERFIL");
                Dt_Export.Columns.Add("ESTADO");

                DataRow row;

                foreach (Response_PerfilesMantenimiento obj in Response)
                {
                    row = Dt_Export.NewRow();
                    row["NOMBRES"] = obj.Nombres;
                    row["USUARIO"] = obj.Username;
                    row["PERFIL"] = obj.Perfil;
                    if (obj.Estado == 1)
                    {
                        row["ESTADO"] = "Activo";
                    }
                    else
                    {
                        row["ESTADO"] = "Inactivo";
                    }
                    Dt_Export.Rows.Add(row);
                    row = null;
                }

                l_ws = l_wb.Worksheets.Add(Dt_Export, "Perfiles");
                l_wb.SaveAs(l_path);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = new { name = l_name }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                if (l_ws != null)
                {
                    l_ws.Dispose();
                }
                if (l_wb != null)
                {
                    l_wb.Dispose();
                }
            }
        }
        [AppDeleteFileAttribute]
        public ActionResult DownloadExcel(Download_Entity p_Entity)
        {
            string l_name = p_Entity.DownloadName == "" ? p_Entity.FileName : p_Entity.DownloadName;
            string l_path = Path.Combine(Server.MapPath("~/temp/"), p_Entity.FileName);
            return File(l_path, "application/vnd.ms-excel", l_name);
        }
    }
}