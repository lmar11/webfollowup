﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using PortalProveedor.Filters;
using PortalProveedor.Models;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class OrdenesNuevasController : Controller
    {
        [HttpPost]
        public ActionResult List()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            ViewBag.Rol = SessionHelpers.GetRolId();
            ViewBag.ProfileId = SessionHelpers.GetProfileId();
            ViewBag.AnalistaId = SessionHelpers.GetAnalistaId();
            ViewBag.AnalistaProfileId = Helpers.ANALISTA_PROFILE_ID;
            ViewBag.AdministadorProfileId = Helpers.ADMINISTRADOR_PROFILE_ID;
            ViewBag.ProveedorProfileId = Helpers.PROVEEDOR_PROFILE_ID;
            return View("List");
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenesVigentes(Entry_GraficoOrdenesVigentes entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenesVigentes";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenesVigentes>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenesVencidas(Entry_GraficoOrdenesVencidas entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenesVencidas";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenesVencidas>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult List_OrdenesCompra(Entry_OrdenCompra entry)
        {
            entry.CodiProv = SessionHelpers.GetProveedorId();
            entry.Consulta = entry.Consulta ?? "";
            entry.Tipo = entry.Tipo ?? "";

            var my_json_data = entry;
            string url_api = "/api/Service/ListOrdenesCompra";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_OrdenCompra>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult List_OrdenCompraItem(Entry_OrdenCompraItem entry)
        {
            var my_json_data = entry;
            string url_api = "/api/Service/ListOrdenCompraItems";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_OrdenCompraItem>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult List_TipoMotivo()
        {
            string url_api = "/api/Service/ListTiposMotivo";

            var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_OrdenCompraTipoMotivo>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Insert_Observaciones(Entry_OrdenCompraObservacionMotivo entry)
        {
            entry.Username = SessionHelpers.GetUsername();
            entry.IpAddress = SessionHelpers.GetIPAddress();

            var my_json_data = entry;
            string url_api = "/api/Service/InsertObservaciones";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Insert_LogOrdenCompra(Entry_LogOrdenCompra entry)
        {
            entry.Username = SessionHelpers.GetUsername();
            entry.Email = "";

            var my_json_data = entry;
            string url_api = "/api/Service/InsertLogOrdenCompra";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SetAcceptOrdenesNuevas(Entry_AcceptOrdenesNuevas entry)
        {
            entry.Username = SessionHelpers.GetUsername();
            entry.IpAddress = SessionHelpers.GetIPAddress();

            var my_json_data = entry;
            string url_api = "/api/Service/UpdateOrdenNueva";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ExportToExcel(Entry_OrdenCompra entry)
        {
            XLWorkbook l_wb = null;
            IXLWorksheet l_ws = null;
            
            string l_name = "Excel-" + SessionHelpers.GetUsername() + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            string l_path = Path.Combine(Server.MapPath("~/temp/"), l_name);

            try
            {
                l_wb = new XLWorkbook();

                entry.CodiProv = SessionHelpers.GetProveedorId();
                entry.Consulta = entry.Consulta ?? "";
                entry.Tipo = entry.Tipo ?? "";

                var my_json_data = entry;
                string url_api = "/api/Service/ListOrdenesCompra";

                var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
                var Response = JsonConvert.DeserializeObject<List<Response_OrdenCompra>>(json_data.Response.ToString());

                DataTable Dt_Export = new DataTable();
                if (entry.CodiProv != 0)
                {
                    Dt_Export.Columns.Add("ANALISTA");
                }
                else
                {
                    Dt_Export.Columns.Add("PROVEEDOR");                    
                }
                Dt_Export.Columns.Add("ORDEN_COMPRA");
                Dt_Export.Columns.Add("IMPORTE");
                Dt_Export.Columns.Add("MONEDA");
                Dt_Export.Columns.Add("FECHA_DOC");
                //Dt_Export.Columns.Add("ESTADO");

                DataRow row;

                foreach (Response_OrdenCompra obj in Response)
                {
                    row = Dt_Export.NewRow();
                    if (entry.CodiProv != 0)
                    {                        
                        row["ANALISTA"] = obj.Analista;
                    }
                    else
                    {
                        row["PROVEEDOR"] = obj.Proveedor;
                    }
                    row["ORDEN_COMPRA"] = obj.DocumentoCompra;
                    row["IMPORTE"] = obj.ImporteTotal;
                    row["MONEDA"] = obj.TipoMoneda;
                    row["FECHA_DOC"] = obj.FechaDocumento;
                    //if (obj.Estatus == "PD")
                    //{
                    //    row["ESTADO"] = "Pend. Descarga";
                    //}
                    //else if (obj.Estatus == "PA")
                    //{
                    //    row["ESTADO"] = "Pend. Respuesta";
                    //}
                    //else if (obj.Estatus == "PR")
                    //{
                    //    row["ESTADO"] = "Pend. Respuesta Reciente";
                    //}
                    Dt_Export.Rows.Add(row);
                    row = null;
                }

                l_ws = l_wb.Worksheets.Add(Dt_Export, "Orden Compra");
                l_wb.SaveAs(l_path);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = new { name = l_name }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                if (l_ws != null)
                {
                    l_ws.Dispose();
                }
                if (l_wb != null)
                {
                    l_wb.Dispose();
                }
            }
        }
        [AppDeleteFileAttribute]
        public ActionResult DownloadExcel(Download_Entity p_Entity)
        {
            string l_name = p_Entity.DownloadName == "" ? p_Entity.FileName : p_Entity.DownloadName;
            string l_path = Path.Combine(Server.MapPath("~/temp/"), p_Entity.FileName);
            return File(l_path, "application/vnd.ms-excel", l_name);
        }
    }
}