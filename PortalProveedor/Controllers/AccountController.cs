﻿using Newtonsoft.Json;
using PortalProveedor.Models;
using PortalProveedor.Models.Business;
using PortalProveedor.Models.Dao;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Configuration;
using System.Web.Mvc;


namespace PortalProveedor.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        //[AppIEBrowser]
        public ActionResult Login()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            return View();
        }
        public ActionResult Logout()
        {
            SessionHelpers.DestroySession();
            return RedirectToAction("Login");
            //return RedirectToAction("Account");
        }
        [HttpPost]
        public JsonResult Login(Login_Model p_Model)
        {
            p_Model.Information = RequestHelpers.GetBrowserInformation(HttpContext.Request);
            p_Model.IPAddress = RequestHelpers.GetClientIpAddress(HttpContext.Request);
            p_Model.AppId = Convert.ToDecimal(Helpers.GetAppSettings("App_AppId"));

            SessionHelpers.InitializeSession(p_Model);
            SessionHelpers.SetAuthenticated();
            SessionHelpers.RemoveVirtualData();
            return new JsonResult()
            {
                Data = new
                {
                    status = 1
                },
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpPost]
        public JsonResult GetLoginValidate(Entry_Login p_Model)
        {
            string url_api = null;
            if (p_Model.Type == "A")
            {
                url_api = "/api/login/LoginAnalista";
            }
            else if (p_Model.Type == "P")
            {
                url_api = "/api/login/LoginEmail";
            }
            p_Model.AppId = Convert.ToInt32(WebConfigurationManager.AppSettings["App_AppId"].ToString());

            var json_data = Helpers.ExecuteApiFollowUp(p_Model, url_api);
            if (json_data.Response != null)
            {
                var l_result = JsonConvert.DeserializeObject<Response_Login>(json_data.Response.ToString());
                json_data.Response = JsonConvert.DeserializeObject<Response_Login>(json_data.Response.ToString());

                if (json_data.Value == "1000")
                {
                    SessionHelpers.SetVirtualUser(l_result);
                }
            }


            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult KeepActive()
        {
            return new JsonResult()
            {
                Data = null,
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpPost]
        public JsonResult GetStatus()
        {
            return new JsonResult()
            {
                Data = new
                {
                    status = SessionHelpers.IsAuthenticated(),
                    session = SessionHelpers.GetSessionID()
                },
                MaxJsonLength = int.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpPost]
        public JsonResult List_Notificaciones(Entry_Notificacion entry)
        {
            if (SessionHelpers.GetAnalistaId() != 0)
            {
                entry.Tipo = "A";
                entry.Codigo = SessionHelpers.GetAnalistaId();
            }
            else
            {
                entry.Tipo = "P";
                entry.Codigo = SessionHelpers.GetProveedorId();
            }

            string url_api = "/api/Service/ListNotificaciones";

            var json_data = Helpers.ExecuteApiFollowUp(entry, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_Notificacion>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
    }
}