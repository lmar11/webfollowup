﻿using PortalProveedor.Filters;
using PortalProveedor.Models;
using PortalProveedor.Models.Dao;
using PortalProveedor.Models.Entity;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;

namespace PortalProveedor.Controllers
{
    public class HomeController : Controller
    {
        
        [AppAuthenticate]
        [AppNoCache]
        public ActionResult Index()
        {
            ViewBag.FullName = SessionHelpers.GetNombre() ?? SessionHelpers.GetRazonSocial();
            ViewBag.PhotoUrl = SessionHelpers.GetUrlFoto() == "https://storage2018general.blob.core.windows.net/elearning/capacitacion/fotos_trabajador/.jpg" ?
                "assets/image/proveedor-image.jpg" : SessionHelpers.GetUrlFoto();
            ViewBag.Profile = SessionHelpers.GetProfileDescription();
            ViewBag.ProfileId = SessionHelpers.GetProfileId();
            ViewBag.AnalistaId = SessionHelpers.GetAnalistaId();
            ViewBag.AnalistaProfileId = Helpers.ANALISTA_PROFILE_ID;
            ViewBag.AdministadorProfileId = Helpers.ADMINISTRADOR_PROFILE_ID;
            ViewBag.ProveedorProfileId = Helpers.PROVEEDOR_PROFILE_ID;
            ViewBag.RolId = SessionHelpers.GetRolId();
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            return View();
        }
        [HttpPost]
        public JsonResult GetModuleObject(Entry_RubroObjectSecurity p_Model)
        {
            p_Model.AppId = SessionHelpers.GetAppId();
            p_Model.CodiPerf = SessionHelpers.GetProfileId();
            p_Model.Username = SessionHelpers.GetUsername();

            string url_api = "/api/Service/ListRubrosSecurity";

            var json_data = Helpers.ExecuteApiFollowUp(p_Model, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_LoginRubro>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetSecurityObject(Entry_ObjectSecurity p_Model)
        {
            p_Model.AppId = SessionHelpers.GetAppId();
            p_Model.ProfileId = SessionHelpers.GetProfileId();
            p_Model.UserId = SessionHelpers.GetUsername();

            string url_api = "/api/Service/ListObjectSecurity";

            var json_data = Helpers.ExecuteApiFollowUp(p_Model, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_ObjectSecurity>>(json_data.Response.ToString());
            }            

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
    }
}