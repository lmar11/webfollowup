﻿using Newtonsoft.Json;
using PortalProveedor.Models;
using PortalProveedor.Models.Entity;
using System;
using System.Globalization;
using System.Web.Configuration;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            return View();
        }        
        public JsonResult LogIn(Entry_Login entry)
        {
            var my_json_data = entry;
            string url_api = "/api/login/LoginEmail";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ListAnalistas()
        {
            string url_api = "/api/Service/ListAnalistas";

            var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_LoginAnalista>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidateRuc(Entry_LoginRuc entry)
        {
            var my_json_data = entry;
            string url_api = "/api/Service/GetProveedorByRuc";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_LoginRuc>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ValidateEmail(Entry_LoginValidacionCorreo entry)
        {
            entry.AppId = Convert.ToInt32(WebConfigurationManager.AppSettings["App_AppId"].ToString());
            var my_json_data = entry;
            string url_api = "/api/Service/ValidarCorreo";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SignUp(Response_LoginSignUp entry)
        {
            entry.AppId = Convert.ToInt32(WebConfigurationManager.AppSettings["App_AppId"].ToString());
            var my_json_data = entry;
            string url_api = "/api/Service/RegistrarUsuario";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
    }
}