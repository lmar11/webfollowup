﻿using Newtonsoft.Json;
using PortalProveedor.Models;
using PortalProveedor.Models.Entity;
using System;
using System.Globalization;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class DashboardController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            ViewBag.ProfileId = SessionHelpers.GetProfileId();
            ViewBag.AnalistaId = SessionHelpers.GetAnalistaId();
            ViewBag.AnalistaProfileId = Helpers.ANALISTA_PROFILE_ID;
            ViewBag.AdministadorProfileId = Helpers.ADMINISTRADOR_PROFILE_ID;
            ViewBag.ProveedorProfileId = Helpers.PROVEEDOR_PROFILE_ID;
            return View("Inicio");
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenItem(Entry_GraficoOrdenItem entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenItem";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenItem>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenBarra(Entry_GraficoOrdenBarra entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenBarra";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenBarra>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenesVigentes(Entry_GraficoOrdenesVigentes entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenesVigentes";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenesVigentes>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenesVencidas(Entry_GraficoOrdenesVencidas entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenesVencidas";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenesVencidas>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
    }
}