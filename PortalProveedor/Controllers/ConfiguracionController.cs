﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using PortalProveedor.Filters;
using PortalProveedor.Models;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class ConfiguracionController : Controller
    {
        [HttpPost]
        public ActionResult List()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            ViewBag.Rol = SessionHelpers.GetRolId();
            ViewBag.ProfileId = SessionHelpers.GetProfileId();
            ViewBag.AnalistaId = SessionHelpers.GetAnalistaId();
            ViewBag.AnalistaProfileId = Helpers.ANALISTA_PROFILE_ID;
            ViewBag.AdministadorProfileId = Helpers.ADMINISTRADOR_PROFILE_ID;
            ViewBag.ProveedorProfileId = Helpers.PROVEEDOR_PROFILE_ID;
            return View("List");
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenesVigentes(Entry_GraficoOrdenesVigentes entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();
            entry.CodiFana = SessionHelpers.GetAnalistaId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenesVigentes";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenesVigentes>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetDataGraficoOrdenesVencidas(Entry_GraficoOrdenesVencidas entry)
        {
            entry.CodigoSapProveedor = SessionHelpers.GetSapId();
            entry.CodiFana = SessionHelpers.GetAnalistaId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetGraficoOrdenesVencidas";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_GraficoOrdenesVencidas>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetDatosProveedor(Entry_Persona_Proveedor entry)
        {
            entry.CodiProv = SessionHelpers.GetProveedorId();
            entry.CodiPers = SessionHelpers.GetPersonaId();

            var my_json_data = entry;
            string url_api = "/api/Service/GetConfiguracionPersona";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_Persona_Proveedor>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateConfiguracion(Entry_Configuracion entry)
        {
            entry.AppId = SessionHelpers.GetAppId();
            entry.CodiPers = SessionHelpers.GetPersonaId();
            entry.Username = SessionHelpers.GetUsername();
            entry.IpAddress = SessionHelpers.GetIPAddress();

            var my_json_data = entry;
            string url_api = "/api/Service/ActualizarUsuarioProveedor";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            //if (json_data.Response != null)
            //{
            //    json_data.Response = JsonConvert.DeserializeObject<List<Response_Configuracion>>(json_data.Response.ToString());
            //}

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
    }
}