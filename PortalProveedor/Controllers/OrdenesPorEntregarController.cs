﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using PortalProveedor.Filters;
using PortalProveedor.Models;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class OrdenesPorEntregarController : Controller
    {
        [HttpPost]
        public ActionResult List()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            ViewBag.Rol = SessionHelpers.GetRolId();
            ViewBag.ProfileId = SessionHelpers.GetProfileId();
            ViewBag.AnalistaId = SessionHelpers.GetAnalistaId();
            ViewBag.AnalistaProfileId = Helpers.ANALISTA_PROFILE_ID;
            ViewBag.AdministadorProfileId = Helpers.ADMINISTRADOR_PROFILE_ID;
            ViewBag.ProveedorProfileId = Helpers.PROVEEDOR_PROFILE_ID;
            return View("List");
        }
        [HttpPost]
        public JsonResult List_OrdenesCompra(Entry_OrdenCompraPorEntregar entry)
        {
            entry.CodiProv = SessionHelpers.GetProveedorId();
            entry.Consulta = entry.Consulta ?? "";
            entry.Tipo = entry.Tipo ?? "";

            var my_json_data = entry;
            string url_api = "/api/Service/ListOrdenesPorEntregar";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_OrdenCompraPorEntregar>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult List_OrdenCompraDetalle(Entry_OrdenCompraPorEntregarDetalle entry)
        {
            var my_json_data = entry;
            string url_api = "/api/Service/ListItemFechasConfirmacion";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_OrdenCompraPorEntregarDetalle>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult List_Motivo()
        {
            string url_api = "/api/Service/ListMotivos";

            var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_OrdenCompraPorEntregarMotivo>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SetOrdenesPorEntregarProveedor(Entry_OrdenCompraPorEntregarConfirmacion entry)
        {
            entry.Username = SessionHelpers.GetUsername();
            entry.IpAddress = SessionHelpers.GetIPAddress();

            foreach (OrdenCompraPorEntregarConfirmacion orden in entry.OrdenCompra)
            {
                orden.Estado = orden.Estado ?? "";
            }

            var my_json_data = entry;
            string url_api = "/api/Service/ConfirmacionOrdenPorEntregar";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SetOrdenesPorEntregarAnalista(Entry_OrdenCompraPorEntregarConfirmacion entry)
        {
            entry.Username = SessionHelpers.GetUsername();
            entry.IpAddress = SessionHelpers.GetIPAddress();

            foreach (OrdenCompraPorEntregarConfirmacion orden in entry.OrdenCompra)
            {
                orden.Estado = orden.Estado ?? "";
            }

            var my_json_data = entry;
            string url_api = "/api/Service/AprobacionOrdenPorEntregar";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveOrdenesPorEntregar(Entry_OrdenCompraPorEntregarRemove entry)
        {
            entry.Username = SessionHelpers.GetUsername();
            entry.IpAddress = SessionHelpers.GetIPAddress();

            var my_json_data = entry;
            string url_api = "/api/Service/AnularItem";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }


        public JsonResult ExportToExcel(Entry_OrdenCompraPorEntregar entry)
        {
            XLWorkbook l_wb = null;
            IXLWorksheet l_ws = null;

            string l_name = "Excel-" + SessionHelpers.GetUsername() + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            string l_path = Path.Combine(Server.MapPath("~/temp/"), l_name);

            try
            {
                l_wb = new XLWorkbook();

                entry.CodiProv = SessionHelpers.GetProveedorId();
                entry.Consulta = entry.Consulta ?? "";
                entry.Tipo = entry.Tipo ?? "";

                var my_json_data = entry;
                string url_api = "/api/Service/ListOrdenesPorEntregar";

                var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
                var Response = JsonConvert.DeserializeObject<List<Response_OrdenCompraPorEntregar>>(json_data.Response.ToString());

                DataTable Dt_Export = new DataTable();
                if (entry.CodiProv == 0)
                {
                    Dt_Export.Columns.Add("PROVEEDOR");
                }
                Dt_Export.Columns.Add("ORDEN_COMPRA");
                Dt_Export.Columns.Add("ITEM");                
                Dt_Export.Columns.Add("DESCRIPCION");
                Dt_Export.Columns.Add("CANT. PEND.");
                Dt_Export.Columns.Add("UM");
                if (entry.CodiProv == 0 && entry.CodiFana == 0)
                {
                    Dt_Export.Columns.Add("ANALISTA");
                }
                if (entry.CodiProv != 0)
                {
                    Dt_Export.Columns.Add("ANALISTA");
                }
                Dt_Export.Columns.Add("IMPORTE_TOTAL");
                Dt_Export.Columns.Add("MONEDA");
                Dt_Export.Columns.Add("FECHA_D");
                Dt_Export.Columns.Add("FECHA_E");
                Dt_Export.Columns.Add("FECHA_C");
                if (entry.CodiProv == 0)
                {
                    Dt_Export.Columns.Add("MOTIVO");
                }

                DataRow row;

                foreach (Response_OrdenCompraPorEntregar obj in Response)
                {
                    row = Dt_Export.NewRow();
                    if (entry.CodiProv == 0)
                    {
                        row["PROVEEDOR"] = obj.Proveedor;
                    }
                    row["ORDEN_COMPRA"] = obj.DocumentoCompra;
                    row["ITEM"] = obj.Posicion;                    
                    row["DESCRIPCION"] = obj.Descripcion;
                    row["CANT. PEND."] = obj.CantidadPendiente;
                    row["UM"] = obj.UM;
                    if (entry.CodiProv == 0 && entry.CodiFana == 0)
                    {
                        row["ANALISTA"] = obj.Analista;
                    }
                    if (entry.CodiProv != 0)
                    {
                        row["ANALISTA"] = obj.Analista;
                    }
                    row["IMPORTE_TOTAL"] = obj.ImporteTotal;
                    row["MONEDA"] = obj.TipoMoneda;
                    row["FECHA_D"] = (obj.FechaDocumento == "01/01/1901" ? "" : obj.FechaDocumento);
                    row["FECHA_E"] = (obj.FechaEntrega == "01/01/1901" ? "" : obj.FechaEntrega);
                    row["FECHA_C"] = (obj.FechaConfirmacion == "01/01/1901" ? "" : obj.FechaConfirmacion);
                    if (entry.CodiProv == 0)
                    {
                        row["MOTIVO"] = obj.Motivo;
                    }

                    Dt_Export.Rows.Add(row);
                    row = null;
                }

                l_ws = l_wb.Worksheets.Add(Dt_Export, "Orden Compra");
                l_wb.SaveAs(l_path);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = new { name = l_name }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                if (l_ws != null)
                {
                    l_ws.Dispose();
                }
                if (l_wb != null)
                {
                    l_wb.Dispose();
                }
            }
        }
        [AppDeleteFileAttribute]
        public ActionResult DownloadExcel(Download_Entity p_Entity)
        {
            string l_name = p_Entity.DownloadName == "" ? p_Entity.FileName : p_Entity.DownloadName;
            string l_path = Path.Combine(Server.MapPath("~/temp/"), p_Entity.FileName);
            return File(l_path, "application/vnd.ms-excel", l_name);
        }
    }
}