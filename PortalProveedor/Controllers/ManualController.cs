﻿using ClosedXML.Excel;
using Newtonsoft.Json;
using PortalProveedor.Filters;
using PortalProveedor.Models;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class ManualController : Controller
    {
        [HttpPost]
        public ActionResult List()
        {
            ViewBag.Time = DateTime.UtcNow.ToString("HHmmssfff", CultureInfo.InvariantCulture);
            ViewBag.Rol = SessionHelpers.GetRolId();
            ViewBag.ProfileId = SessionHelpers.GetProfileId();
            ViewBag.AnalistaId = SessionHelpers.GetAnalistaId();
            ViewBag.AnalistaProfileId = Helpers.ANALISTA_PROFILE_ID;
            ViewBag.AdministadorProfileId = Helpers.ADMINISTRADOR_PROFILE_ID;
            ViewBag.ProveedorProfileId = Helpers.PROVEEDOR_PROFILE_ID;
            return View("List");
        }
        [HttpPost]
        public JsonResult List_ManualMantenimiento()
        {
            string url_api = "/api/Crud/CrudGetManualActivo";

            var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<Response_ManualMantenimiento>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Get_ManualActivoMantenimiento()
        {
            string url_api = "/api/Crud/CrudGetManualActivo";

            var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_ManualMantenimiento>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult SetManualMantenimiento(Entry_ManualMantenimiento entry)
        {
            if (SaveFileToServer(entry.File))
            {
                var _entry = new
                {
                    NombreArchivo = entry.File.FileName,
                    Username = SessionHelpers.GetUsername(),
                    IpAddress = SessionHelpers.GetIPAddress(),
                };

                var my_json_data = _entry;
                string url_api = "/api/Crud/CrudInsertManual";

                var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);

                return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var my_json_response = new
                {
                    Response = "",
                    Value = "",
                    Message = "",
                    Status = 0,
                    Exception = "Ocurrió un error al guardar el archivo"
                };
                return Json(my_json_response, JsonRequestBehavior.AllowGet);
            }

            
        }
        private bool SaveFileToServer(HttpPostedFileBase file)
        {
            try
            {
                string strPath = Server.MapPath("~");
                strPath = strPath + "\\assets\\files";
                Directory.CreateDirectory(strPath);
                strPath = strPath + "\\";
                file.SaveAs(strPath + file.FileName);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        public JsonResult ExportToExcel()
        {
            XLWorkbook l_wb = null;
            IXLWorksheet l_ws = null;

            string l_name = "Excel-" + SessionHelpers.GetUsername() + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            string l_path = Path.Combine(Server.MapPath("~/temp/"), l_name);

            try
            {
                l_wb = new XLWorkbook();
                
                string url_api = "/api/Crud/CrudListAnalistas";

                var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
                var Response = JsonConvert.DeserializeObject<List<Response_ManualMantenimiento>>(json_data.Response.ToString());

                DataTable Dt_Export = new DataTable();
                Dt_Export.Columns.Add("NOMBRE_ARCHVO");
                Dt_Export.Columns.Add("FECHA_CREACION");

                DataRow row;

                foreach (Response_ManualMantenimiento obj in Response)
                {
                    row = Dt_Export.NewRow();
                    row["NOMBRE_ARCHVO"] = obj.NombreArchivo;
                    row["FECHA_CREACION"] = obj.FechaCreacion;
                    Dt_Export.Rows.Add(row);
                    row = null;
                }

                l_ws = l_wb.Worksheets.Add(Dt_Export, "Manual");
                l_wb.SaveAs(l_path);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = new { name = l_name }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                if (l_ws != null)
                {
                    l_ws.Dispose();
                }
                if (l_wb != null)
                {
                    l_wb.Dispose();
                }
            }
        }
        [AppDeleteFileAttribute]
        public ActionResult DownloadExcel(Download_Entity p_Entity)
        {
            string l_name = p_Entity.DownloadName == "" ? p_Entity.FileName : p_Entity.DownloadName;
            string l_path = Path.Combine(Server.MapPath("~/temp/"), p_Entity.FileName);
            return File(l_path, "application/vnd.ms-excel", l_name);
        }
    }
}