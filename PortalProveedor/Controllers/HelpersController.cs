﻿using Newtonsoft.Json;
using PortalProveedor.Models;
using PortalProveedor.Models.Dao;
using PortalProveedor.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace PortalProveedor.Controllers
{
    public class HelpersController : Controller
    {
        public JsonResult List_Analista()
        {
            string url_api = "/api/Service/ListAnalistas";

            var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<Response_LoginAnalista>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult List_Proceso()
        {
            string url_api = "/api/Service/ListProcesos";

            var json_data = Helpers.ExecuteApiFollowUp(null, url_api);
            if (json_data.Response != null)
            {
                json_data.Response = JsonConvert.DeserializeObject<List<ProcesoModel>>(json_data.Response.ToString());
            }

            return Json(json_data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Exist_Url(string Url)
        {
            ApiResponse Response = new ApiResponse();
            try
            {
                HttpWebRequest request = HttpWebRequest.Create(Url) as HttpWebRequest;
                request.Timeout = 5000;
                request.Method = "HEAD";

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                int statusCode = (int)response.StatusCode;
                if (statusCode >= 100 && statusCode < 400)
                {
                    Response.Status = 1;
                    Response.Value = "1000";
                    Response.Response = true;
                }
                else if (statusCode >= 500 && statusCode <= 510)
                {
                    Response.Status = 1;
                    Response.Value = "1000";
                    Response.Response = false;
                }
            }
            catch (Exception e)
            {
                Response.Status = -1;
                Response.Value = "3000";
                Response.Message = e.Message;
                Response.Response = false;
            }

            return Json(Response, JsonRequestBehavior.AllowGet);
        }
        public string GetFile(string Ruta)
        {
            try
            {
                StringBuilder _sb = new StringBuilder();

                Byte[] _byte = this.GetImage(Ruta);

                _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

                return _sb.ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private byte[] GetImage(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }
    }
}