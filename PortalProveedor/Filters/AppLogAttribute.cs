﻿using PortalProveedor.Models;
using PortalProveedor.Models.Dao;
using PortalProveedor.Models.Entity;
using System;
using System.Web.Mvc;

namespace PortalProveedor.Filters
{
    public class AppLogAttribute : ActionFilterAttribute
    {
        private string Controller = null;
        private string Action = null;
        /* This method is called before a controller action is executed */
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Action = filterContext.ActionDescriptor.ActionName;
        }
        /* This method is called after a controller action is executed */
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
        }
        /* This method is called before a controller action result is executed */
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
        }
        /* This method is called after a controller action result is executed */
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            try
            {
                SaveLog(new Log_Model
                {
                    ControllerName = Controller,
                    ActionName = Action,
                    SessionId = SessionHelpers.GetSessionLog()
                });
            }
            catch (Exception e)
            {
                
            }
        }
        private void SaveLog(Log_Model entry)
        {
            var my_json_data = entry;
            string url_api = "/api/login/SaveLog";

            var json_data = Helpers.ExecuteApiFollowUp(my_json_data, url_api);
        }
    }
}