﻿using PortalProveedor.Models;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PortalProveedor.Filters
{
    public class AppAuthenticateAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (!AuthorizeCore(filterContext.HttpContext))
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }
            return SessionHelpers.IsAuthenticated();
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //User isn't logged in
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(new { controller = "Account", action = "Login" }));
            //new RouteValueDictionary(new { controller = "Login", action = "Login" }));
        }
    }
}